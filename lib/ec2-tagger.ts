import { createHash } from 'crypto';

import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import {
  AwsCustomResource,
  AwsCustomResourcePolicy,
  AwsSdkCall,
  PhysicalResourceId,
} from '@aws-cdk/custom-resources';
import * as iam from '@aws-cdk/aws-iam';

import { BaseResource } from './base-resource';

interface TaggerProps {
  arns: string[];
  tags: { [key: string]: string };
  physicalResourceId?: PhysicalResourceId;
}

/**
 * This construct is to work around the limitation with CDK where it's not able
 * to add tags to external existing resources, so we'll do it using a lambda instead.
 */
export class EC2Tagger extends BaseResource {
  constructor(scope: cdk.Construct, id: string, props: TaggerProps) {
    super(scope, id);

    const tags = Object.entries(props.tags).map(([key, value]) => ({
      Key: key,
      Value: value,
    }));

    const parameters = {
      Resources: props.arns.map((arn) => cdk.Arn.parse(arn).resourceName).filter(Boolean),
      Tags: tags,
    };

    const hash = createHash('sha256').update(JSON.stringify(parameters)).digest('hex');

    const sdkCall: AwsSdkCall = {
      service: 'EC2',
      action: 'createTags',
      parameters,
      physicalResourceId: PhysicalResourceId.of(hash),
    };

    const func = new AwsCustomResource(this, 'CustomResource', {
      onCreate: sdkCall,
      onUpdate: sdkCall,
      onDelete: {
        ...sdkCall,
        action: 'deleteTags',
      },
      policy: AwsCustomResourcePolicy.fromSdkCalls({
        resources: props.arns,
      }),
    });
  }
}
