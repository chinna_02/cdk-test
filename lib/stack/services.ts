import * as cdk from '@aws-cdk/core';
import * as cf from '@aws-cdk/aws-cloudfront';
import * as route53 from '@aws-cdk/aws-route53';
import * as wafv2 from '@aws-cdk/aws-wafv2';
import * as s3 from '@aws-cdk/aws-s3';
import * as lambda from '@aws-cdk/aws-lambda';
import r53targets = require('@aws-cdk/aws-route53-targets/lib');

import * as waf from '../waf';
import { GetApiKey } from '../get-apikey';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { servicesCertificateArn } from '../certs';
import {
  CloudFrontStage,
  EdgeLambda,
  lambdaAssociations,
  disableCache,
  CloudFrontBaseStack,
  CloudFrontBaseStackProps,
} from '../cloudfront';
import { RemovalPolicy } from '@aws-cdk/core';

const { ORIGIN_REQUEST } = cf.LambdaEdgeEventType;

const fluentApiGatewayIds = {
  dev: 'cb57mkt3pa',
  stage: 'a10z422ge4',
  hfe: 'n9jz1h9yl2',
  ppd: '5a81vzkk3a',
  prod: 'og4wadipp8',
};

const orderStatusApiGatewayIds = {
  dev: 'hno09udj77',
  stage: 'hnh63pw8ec',
  hfe: 'a1n7y98zja',
  ppd: '8eb77r7ie0',
  prod: 'eu524io4jh',
};

const emailApiGatewayIds = {
  dev: 'b673mic1od',
  stage: '49jhivuioc',
  hfe: 'b673mic1od',
  ppd: '08xdtwz841',
  prod: 'xyexhxh5tb',
};

const inventorySyncApiGatewayIds = {
  dev: 'j701vjjzv6',
  stage: 'rqtxv8luq0',
  hfe: '',
  ppd: 'kj0r9yqvdj',
  prod: 'd75r6md4r4',
};

export class ServicesCloudFrontStack extends CloudFrontBaseStack {
  constructor(scope: cdk.Construct, id: string, props: CloudFrontBaseStackProps) {
    super(scope, id, props);

    const servicesDomain =
      this.stage === 'prod'
        ? 'services.pokemoncenter.com'
        : `services.${this.stage}.pokemoncenter.com`;

    const originRequestLambda = new EdgeLambda(this, `apiOriginRequest`).currentVersion;

    const getApiKey = new GetApiKey(this, 'GetApiKey', {
      apiKeyName: `tpci-aws-cdk-api-wafs-cloudfront-key-${this.stage}`,
      region: 'us-west-2',
    });

    const acl = new wafv2.CfnWebACL(this, 'ACL', {
      name: this.stackName,
      description: 'CloudFront WAF for services.pokemoncenter.com',
      scope: 'CLOUDFRONT',
      defaultAction: this.stage === 'prod' ? { allow: {} } : { block: {} },
      visibilityConfig: {
        metricName: this.stackName,
        cloudWatchMetricsEnabled: true,
        sampledRequestsEnabled: true,
      },
      rules: waf.rules(
        this,
        waf.healthRule,
        ...waf.orderStatusRules(this),
        waf.internalIpWhitelistRule(this),
        waf.externalIpWhitelistRule(this),
      ),
    });

    const originConfig = ({
      pathPattern,
      apiGatewayId,
      forwardedValues = {},
    }: {
      pathPattern: string;
      apiGatewayId: string;
      forwardedValues?: Omit<cf.CfnDistribution.ForwardedValuesProperty, 'queryString'> & {
        queryString?: boolean;
      };
    }) => ({
      behaviors: [
        {
          pathPattern,
          lambdaFunctionAssociations: lambdaAssociations({
            [ORIGIN_REQUEST]: originRequestLambda,
          }),
          ...disableCache,
          allowedMethods: cf.CloudFrontAllowedMethods.ALL,
          forwardedValues: {
            queryString: true,
            ...forwardedValues,
          },
        },
      ],
      customOriginSource: {
        originProtocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
        domainName: `${apiGatewayId}.execute-api.us-west-2.amazonaws.com`,
      },
      originPath: `/${this.stage}`,
      originHeaders: {
        'x-api-key': getApiKey.value,
      },
    });
    const apiGatewayOriginConfig = [
      {
        pathPattern: '/tpci-fluent-webhook/*',
        apiGatewayId: fluentApiGatewayIds[this.stage],
        forwardedValues: {
          headers: ['flex.signature'],
        },
      },
      {
        pathPattern: '/tpci-order-status/*',
        apiGatewayId: orderStatusApiGatewayIds[this.stage],
      },
      {
        pathPattern: '/tpci-transaction-email-api/*',
        apiGatewayId: emailApiGatewayIds[this.stage],
      },
      {
        pathPattern: '/tpci-inventory-sync/*',
        apiGatewayId: inventorySyncApiGatewayIds[this.stage],
      },
    ]
      .filter((config) => config.apiGatewayId)
      .map((config) => originConfig(config));
    const distribution = new cf.CloudFrontWebDistribution(this, `CloudFrontServicesDistribution`, {
      defaultRootObject: '',
      loggingConfig: {
        bucket: new s3.Bucket(this, 'servicesLogBucket', {
          bucketName: this.stackName,
          lifecycleRules: [
            {
              enabled: true,
              expiration: cdk.Duration.days(30),
            },
          ],
        }),
        includeCookies: false,
      },
      aliasConfiguration: {
        names: [servicesDomain],
        acmCertRef: servicesCertificateArn(this.stage),
        securityPolicy: cf.SecurityPolicyProtocol.TLS_V1_2_2018,
      },
      errorConfigurations:
        this.stage === 'prod'
          ? [
              {
                errorCode: 403,
                errorCachingMinTtl: 0,
              },
            ]
          : undefined,

      originConfigs: [
        ...apiGatewayOriginConfig,
        {
          behaviors: [
            {
              isDefaultBehavior: true,
              allowedMethods: cf.CloudFrontAllowedMethods.ALL,
              forwardedValues: {
                queryString: true,
              },
            },
          ],
          customOriginSource: {
            originProtocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
            domainName: 'www.pokemoncenter.com',
          },
          originPath: '/404',
        },
      ],
      webACLId: acl.attrArn,
    });

    new route53.ARecord(this, `AliasRecord`, {
      recordName: '',
      target: route53.RecordTarget.fromAlias(new r53targets.CloudFrontTarget(distribution)),
      zone: route53.HostedZone.fromLookup(this, `Zone`, {
        domainName: servicesDomain,
      }),
    });
  }
}

export default ServicesCloudFrontStack;
