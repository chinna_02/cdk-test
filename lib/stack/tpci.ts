import * as cdk from '@aws-cdk/core';
import { dev, prod } from '../accounts';
import { toTitleCase } from '../utils';

export type Stage = 'dev' | 'stage' | 'ppd' | 'prod' | 'hfe' | 'stage-eu' | 'ppd-eu' | 'prod-eu';
export type AccountName = 'dev' | 'prod';

export interface TPCIStackProps extends cdk.StackProps {
  stage?: Stage;
}

export class TPCIStack extends cdk.Stack {
  public readonly shortStackName: string;
  public readonly stage?: Stage;
  public readonly stageTitle?: string;
  public readonly accountName: AccountName;
  public readonly isProdAccount: boolean;
  public readonly isDevAccount: boolean;

  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    let shortStackName = id.replace(/^tpci-aws-cdk-/, '');

    let fullId: string;
    if (props && props.stage) {
      shortStackName = shortStackName.replace(new RegExp(`-${props.stage}$`), '');
      fullId = `tpci-aws-cdk-${shortStackName}-${props.stage}`;
    } else {
      fullId = `tpci-aws-cdk-${shortStackName}`;
    }

    super(scope, fullId, props);

    this.shortStackName = shortStackName;

    if (props && props.stage) {
      this.stage = props.stage;
      cdk.Tags.of(this).add('stage', props.stage);
    }
    this.stageTitle = this.stage && toTitleCase(this.stage);

    if (this.account === dev) {
      this.accountName = 'dev';
      this.isDevAccount = true;
      this.isProdAccount = false;
      if (['ppd', 'prod'].some((name) => this.stage && this.stage.startsWith(name))) {
        throw new Error(`${this.stage} cannot be deployed to the dev AWS account`);
      }
    } else if (this.account === prod) {
      this.accountName = 'prod';
      this.isDevAccount = false;
      this.isProdAccount = true;
      if (['dev', 'stage', 'hfe'].some((name) => this.stage && this.stage.startsWith(name))) {
        throw new Error(`${this.stage} cannot be deployed to the prod AWS account`);
      }
    } else if (!cdk.Token.isUnresolved(this.account)) {
      throw new Error(`Unknown AWS Account: ${this.account}`);
    }
  }

  public allocateLogicalId(element: cdk.CfnElement): string {
    const orig = super.allocateLogicalId(element);
    return 'tpci' + orig + (this.stageTitle || '');
  }

  /**
   * Output the resourceName using TPCI's standard naming conventions:
   *
   *   <repo>-<stack>-<resource>-<stage>
   *
   * If there is no stage, as in there must be only one instance of this stack
   * per AWS account, then the stage is left out:
   *
   *    <repo>-<stack>-<resource>
   *
   * Since this repo is "tpci-aws-cdk", resource names always start with "tpci-aws-cdk".
   *
   * For example, if the stack is called "vpc", the resource name is "subnet-a",
   * and stack is "dev", then the output is:
   *
   *   tpci-aws-cdk-vpc-subnet-a-dev
   */
  public resourceName(name: string): string {
    const res = `tpci-aws-cdk-${this.shortStackName}-${name}`;
    if (!this.stage) {
      return res;
    }
    return `${res}-${this.stage}`;
  }
}
