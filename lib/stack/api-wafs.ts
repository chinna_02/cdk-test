import * as cdk from '@aws-cdk/core';
import * as apigw from '@aws-cdk/aws-apigateway';
import * as wafv2 from '@aws-cdk/aws-wafv2';

import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { GetApiKey } from '../get-apikey';
import {
  allow,
  matchUrl,
  matchQueryArg,
  matchHeader,
  textTransformations,
  rules,
  healthRule,
} from '../waf';

interface ApiWafsStackProps extends TPCIStackProps {
  stage: Stage;
}

const scope = 'REGIONAL';

export class ApiWafsStack extends TPCIStack {
  public readonly stage: Stage;

  constructor(cdkScope: cdk.Construct, id: string, props: ApiWafsStackProps) {
    super(cdkScope, id, props);

    const apiKeyName = this.resourceName('cloudfront-key').replace(/-eu$/, '');

    const getApiKey = new GetApiKey(this, 'GetApiKey', { apiKeyName, region: 'us-west-2' });

    if (this.region === 'us-west-2') {
      const apiKey = new apigw.ApiKey(this, 'ApiKey', {
        apiKeyName,
        description: 'For CloudFront to have exclusive access to API Gateways',
        generateDistinctId: false,
        enabled: true,
      });
      getApiKey.node.addDependency(apiKey);
    }

    const allowHealth = {
      name: 'health',
      ...allow,
      statement: {
        byteMatchStatement: {
          ...matchUrl('/health'),
          positionalConstraint: 'ENDS_WITH',
        },
      },
    };

    const hasQueryArg = (name: string) => ({
      sizeConstraintStatement: {
        ...matchQueryArg(name),
        comparisonOperator: 'GT',
        size: 1,
        ...textTransformations('NONE'),
      },
    });

    const hasHeader = (name: string) => ({
      sizeConstraintStatement: {
        ...matchHeader(name),
        comparisonOperator: 'GT',
        size: 1,
        ...textTransformations('NONE'),
      },
    });

    const and = (
      ...statements: wafv2.CfnWebACL.StatementProperty[]
    ): wafv2.CfnWebACL.StatementProperty => {
      if (statements.length === 1) {
        return statements[0];
      } else if (statements.length > 1) {
        return {
          andStatement: {
            statements,
          },
        };
      } else {
        throw new Error('No statements');
      }
    };

    const or = (
      ...statements: wafv2.CfnWebACL.StatementProperty[]
    ): wafv2.CfnWebACL.StatementProperty => {
      if (statements.length === 1) {
        return statements[0];
      } else if (statements.length > 1) {
        return {
          orStatement: {
            statements,
          },
        };
      } else {
        throw new Error('No statements');
      }
    };

    const mkAcl = ({
      name,
      statements = [],
    }: {
      name: string;
      statements?: wafv2.CfnWebACL.StatementProperty[];
    }) => {
      const acl = new wafv2.CfnWebACL(this, `${name}ACL`, {
        name: this.resourceName(name),
        description: `Only CloudFront can access tpci-${name}`,
        scope,
        defaultAction: { block: {} },
        rules: rules(
          this,
          healthRule, //
          {
            name: 'allow',
            ...allow,
            statement: and(
              ...statements, //
              {
                byteMatchStatement: {
                  ...matchHeader('x-api-key'),
                  positionalConstraint: 'EXACTLY',
                  searchString: getApiKey.value,
                  ...textTransformations('NONE'),
                },
              },
            ),
          },
        ),
        visibilityConfig: {
          metricName: this.resourceName(name),
          cloudWatchMetricsEnabled: false,
          sampledRequestsEnabled: false,
        },
      });

      new cdk.CfnOutput(this, `${name}-output`, {
        value: acl.attrArn,
        exportName: this.resourceName(name),
      });
    };

    mkAcl({
      name: 'ecommweb-lambda',
    });

    mkAcl({
      name: 'ecommweb-api',
    });

    if (this.region !== 'eu-west-1') {
      mkAcl({
        name: 'aws-sam-ref-api',
        statements: [hasQueryArg('x-api-key')],
      });

      mkAcl({
        name: 'fluent-webhook',
        statements: [
          hasQueryArg('x-api-key'), //
          hasHeader('flex.signature'),
        ],
      });

      mkAcl({
        name: 'transaction-email-api',
        statements: [hasHeader('authorization')],
      });

      mkAcl({
        name: 'order-status',
        statements: [hasHeader('authorization')],
      });

      mkAcl({
        name: 'inventory-sync',
        statements: [hasHeader('authorization')],
      });
    }
  }
}
