import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as iam from '@aws-cdk/aws-iam';
import * as apigw from '@aws-cdk/aws-apigateway';
import * as transfer from '@aws-cdk/aws-transfer';
import * as s3 from '@aws-cdk/aws-s3';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { EndpointType, JsonSchemaType, JsonSchemaVersion } from '@aws-cdk/aws-apigateway';
export class S3SFTP extends TPCIStack {
  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);
    if (this.isDevAccount) {
      const lambdaRole = new iam.Role(this, 'S3SftpLambdaRole', {
        assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
        managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName('AWSLambdaExecute')],
        inlinePolicies: {
          LambdaSMPolicy: new iam.PolicyDocument({
            statements: [
              new iam.PolicyStatement({
                actions: ['secretsmanager:GetSecretValue'],
                resources: [`arn:aws:secretsmanager:${this.region}:${this.account}:secret:SFTP/*`],
              }),
            ],
          }),
        },
      });

      const s3sftLambda = new lambda.Function(this, 'S3SftpLambda', {
        functionName: this.resourceName('lambda'),
        runtime: lambda.Runtime.PYTHON_3_8,
        code: new lambda.AssetCode('./lambda/s3sftp'),
        handler: 'lambda_function.lambda_handler',
        timeout: cdk.Duration.seconds(45),
        role: lambdaRole,
        environment: {
          SecretsManagerRegion: this.region,
        },
      });
      const apiGateway = new apigw.RestApi(this, 'S3SftpApiGateway', {
        endpointTypes: [EndpointType.REGIONAL],
      });
      apiGateway.addModel('S3SftpMethodResponseModel', {
        contentType: 'application/json',
        modelName: 'S3SftpMethodResponseModel',
        schema: {
          schema: JsonSchemaVersion.DRAFT4,
          title: 'S3SftpMethodResponseModel',
          type: JsonSchemaType.OBJECT,
          properties: {
            Role: { type: JsonSchemaType.STRING },
            Policy: { type: JsonSchemaType.STRING },
            HomeDirectory: { type: JsonSchemaType.STRING },
            PublicKeys: {
              type: JsonSchemaType.ARRAY,
              items: {
                type: JsonSchemaType.STRING,
              },
            },
          },
        },
      });
      const lambdaIntegration = new apigw.LambdaIntegration(s3sftLambda, {
        proxy: false,
        allowTestInvoke: false,
        passthroughBehavior: apigw.PassthroughBehavior.WHEN_NO_TEMPLATES,
        integrationResponses: [
          {
            statusCode: '200',
          },
        ],
        requestTemplates: {
          'application/json': `
            {"username": "$util.urlDecode($input.params('username'))",
            "password": "$util.escapeJavaScript($input.params('Password')).replaceAll("\\'","'")",
            "protocol": "$input.params('protocol')",
            "serverId": "$input.params('serverId')",
            "sourceIp": "$input.params('sourceIp')"}
          `,
        },
      });

      apiGateway.root
        .addResource('servers')
        .addResource('{serverId}')
        .addResource('users')
        .addResource('{username}')
        .addResource('config')
        .addMethod('GET', lambdaIntegration, {
          methodResponses: [
            {
              statusCode: '200',
              responseModels: {
                'application/json': {
                  modelId: 'S3SftpMethodResponseModel',
                },
              },
            },
          ],
        });
      const bucketName = `tpci-s3sftp`;
      const bucket = new s3.Bucket(this, 'S3SftpBucket', { bucketName }); //More config and name of the bucket
      new transfer.CfnServer(this, 'Sftp', {
        identityProviderType: 'API_GATEWAY',
        identityProviderDetails: {
          invocationRole: new iam.Role(this, 'S3SftpInvocationRole', {
            roleName: `${cdk.Aws.STACK_NAME}-s3-sftp-invocation`,
            assumedBy: new iam.ServicePrincipal('transfer.amazonaws.com'),
            inlinePolicies: {
              S3SftpInvocationPolicy: new iam.PolicyDocument({
                statements: [
                  new iam.PolicyStatement({
                    actions: ['execute-api:Invoke'],
                    resources: [
                      `arn:aws:execute-api:${props?.env?.region}:${props?.env?.account}:${apiGateway.restApiId}/prod/GET/*`,
                    ],
                  }),
                  new iam.PolicyStatement({
                    actions: ['apigateway:GET'],
                    resources: ['*'],
                  }),
                ],
              }),
            },
          }).roleArn,
          url: apiGateway.url,
        },
        loggingRole: new iam.Role(this, 'S3SftpCloudWatchRole', {
          roleName: `${cdk.Aws.STACK_NAME}-s3-sftp-cloudwatch`,
          assumedBy: new iam.ServicePrincipal('transfer.amazonaws.com'),
          inlinePolicies: {
            CloudWatch: new iam.PolicyDocument({
              statements: [
                new iam.PolicyStatement({
                  actions: [
                    'logs:CreateLogGroup',
                    'logs:CreateLogStream',
                    'logs:DescribeLogStreams',
                    'logs:PutLogEvents',
                  ],
                  resources: ['*'],
                }),
                new iam.PolicyStatement({
                  actions: ['s3:ListBucket', 's3:GetBucketLocation'],
                  resources: [`arn:aws:s3:::${bucket}`, `arn:aws:s3:::${bucket}/*`],
                }),
              ],
            }),
          },
        }).roleArn,
      });
    }
  }
}
