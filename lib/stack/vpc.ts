import * as cdk from '@aws-cdk/core';
import * as ec2 from '@aws-cdk/aws-ec2';
import * as ips from '../ips';
import { TPCIStack, TPCIStackProps } from './tpci';
import {
  AwsCustomResource,
  AwsCustomResourcePolicy,
  AwsSdkCall,
  PhysicalResourceId,
} from '@aws-cdk/custom-resources';
import { EC2Tagger } from '../ec2-tagger';

export class VpcStack extends TPCIStack {
  public readonly vpc: ec2.Vpc | ec2.IVpc;

  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);

    const pcenterVpcName = `pcenter-${this.accountName}-vpc`;

    // pcenter VPC is ONLY deployed in us-west-2 on both accounts
    const pcenter = this.region === 'us-west-2';

    if (pcenter) {
      this.vpc = ec2.Vpc.fromLookup(this, 'pcenterVpc', {
        vpcName: pcenterVpcName,
      });
    } else {
      this.vpc = new ec2.Vpc(this, 'Vpc', {
        // To avoid cross AZ traffic costs. Put a NatGateway/EIP in each AZ
        natGateways: 2,
        maxAzs: 2,
        subnetConfiguration: [
          {
            name: 'Public',
            subnetType: ec2.SubnetType.PUBLIC,
          },
          {
            name: 'Private',
            subnetType: ec2.SubnetType.PRIVATE,
          },
        ],
      });
    }
    const outputPaths: string[] = this.vpc.publicSubnets.map(
      (_, idx) => `NatGateways.${idx}.NatGatewayAddresses.0.PublicIp`,
    );
    const describeNatGatewaysSdkCall: AwsSdkCall = {
      service: 'EC2',
      action: 'describeNatGateways',
      parameters: {
        Filter: [
          {
            Name: 'vpc-id',
            Values: [this.vpc.vpcId],
          },
        ],
      },
      outputPaths,
      physicalResourceId: PhysicalResourceId.of(this.vpc.vpcId),
      region: cdk.Aws.REGION,
    };
    const describeNatGateways = new AwsCustomResource(this, 'DescribeNatGateways', {
      onCreate: describeNatGatewaysSdkCall,
      onUpdate: describeNatGatewaysSdkCall,
      policy: AwsCustomResourcePolicy.fromSdkCalls({
        resources: AwsCustomResourcePolicy.ANY_RESOURCE,
      }),
    });
    describeNatGateways.node.addDependency(this.vpc);

    const privateSubnets = (() => {
      if (pcenter) {
        // Because pcenter has both private subnets for Data and Private
        return ['a', 'b', 'c'].map((zone, n) =>
          ec2.Subnet.fromSubnetAttributes(this, `PrivateSubnet${n}`, {
            availabilityZone: `${this.region}${zone}`,
            subnetId: cdk.Fn.importValue(`${pcenterVpcName}-PrivateSubnetAZ${n}`),
          }),
        );
      } else {
        return this.vpc.privateSubnets;
      }
    })();

    const svc = ec2.InterfaceVpcEndpointAwsService;

    (
      [
        ['ApiGateway', svc.APIGATEWAY],
        ['SecretsManager', svc.SECRETS_MANAGER],
        ['Sns', svc.SNS],
        ['Sqs', svc.SQS],
      ] as [string, ec2.IInterfaceVpcEndpointService][]
    ).forEach(([name, service]) => {
      this.vpc
        .addInterfaceEndpoint(`${name}Endpoint`, {
          service,
          subnets: {
            subnets: privateSubnets,
          },
        })
        .connections.allowDefaultPortFromAnyIpv4();
    });

    // Kubernetes Tagging for Load Balancing
    // https://wiki.pokemon.tools/display/PC/EKS+Load+balancer+tagging
    const subnetArn = (subnet: ec2.ISubnet) =>
      cdk.Arn.format({ service: 'ec2', resource: 'subnet', resourceName: subnet.subnetId }, this);
    const subnets = [...this.vpc.publicSubnets, ...this.vpc.privateSubnets];
    subnets.forEach((subnet) => {
      // XXX: Unfortunately, this doesn't work as CDK is not able to tag an existing resource.
      //      It's a known limitation of CDK.
      //      Leaving this here just in case it works some day.
      cdk.Tags.of(subnet).add(
        `kubernetes.io/cluster/tpci-aws-cdk-eks-cluster-${this.isProdAccount ? 'ppd' : 'dev'}`,
        'shared',
      );
      cdk.Tags.of(subnet).add(
        `kubernetes.io/cluster/tpci-aws-cdk-eks-cluster-${this.isProdAccount ? 'prod' : 'stage'}`,
        'shared',
      );
    });

    // XXX: As a hack, we'll use CDK to create a Custom Resource that adds the tags via the AWS APIs
    new EC2Tagger(this, 'SubnetK8sClusterTags', {
      arns: subnets.map(subnetArn),
      tags: {
        [`kubernetes.io/cluster/tpci-aws-cdk-eks-cluster-${this.isProdAccount ? 'ppd' : 'dev'}`]:
          'shared',
        [`kubernetes.io/cluster/tpci-aws-cdk-eks-cluster-${this.isProdAccount ? 'prod' : 'stage'}`]:
          'shared',
      },
    });

    // XXX: does not work, see above
    this.vpc.privateSubnets.forEach((subnet) => {
      cdk.Tags.of(subnet).add('kubernetes.io/role/internal-elb', '1');
    });

    // XXX: hack, see above
    new EC2Tagger(this, 'PrivateSubnetK8sRoleTags', {
      arns: this.vpc.privateSubnets.map(subnetArn),
      tags: {
        'kubernetes.io/role/internal-elb': '1',
      },
    });

    // XXX: does not work, see above
    this.vpc.publicSubnets.forEach((subnet) => {
      cdk.Tags.of(subnet).add('kubernetes.io/role/elb', '1');
    });

    // XXX: hack, see above
    new EC2Tagger(this, 'PublicSubnetK8sRoleTags', {
      arns: this.vpc.publicSubnets.map(subnetArn),
      tags: {
        'kubernetes.io/role/elb': '1',
      },
    });

    if (this.isDevAccount && this.region === 'us-west-2') {
      // Import tpci-aws-cdk-eks-cluster-dev-elb-SG security ID
      const importedEksDevSecurityGroup = ec2.SecurityGroup.fromSecurityGroupId(
        this,
        'imported-security-group-dev',
        'sg-05efd207a992b72eb',
        { allowAllOutbound: true, mutable: true },
      );

      // Adding inbound rules for tpci-aws-cdk-eks-cluster-dev-elb-SG
      importedEksDevSecurityGroup.addIngressRule(
        ec2.Peer.ipv4('10.82.0.0/16'),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 within vpc',
      );
      importedEksDevSecurityGroup.addIngressRule(
        ec2.Peer.ipv4(`${ips.pokemonVPN[0]}/32`),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 from Bellevue vpn',
      );
      importedEksDevSecurityGroup.addIngressRule(
        ec2.Peer.ipv4(`${ips.pokemonVPN[1]}/32`),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 from london vpn',
      );

      // Import tpci-aws-cdk-eks-cluster-Stage-elb-SG security ID
      const importedEksStageSecurityGroup = ec2.SecurityGroup.fromSecurityGroupId(
        this,
        'imported-security-group-stage',
        'sg-0381fe61fe12acca3',
        { allowAllOutbound: true, mutable: true },
      );
      // Adding inbound rules for tpci-aws-cdk-eks-cluster-dev-elb-SG
      importedEksStageSecurityGroup.addIngressRule(
        ec2.Peer.ipv4('10.82.0.0/16'),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 within vpc',
      );
      importedEksStageSecurityGroup.addIngressRule(
        ec2.Peer.ipv4(`${ips.pokemonVPN[0]}/32`),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 from Bellevue vpn',
      );
      importedEksStageSecurityGroup.addIngressRule(
        ec2.Peer.ipv4(`${ips.pokemonVPN[1]}/32`),
        ec2.Port.tcp(80),
        'allow outgoing traffic on port 80 from london vpn',
      );
    }
    // Outputs
    if (pcenter) {
      new cdk.CfnOutput(this, 'DefaultSecurityGroup', {
        value: cdk.Fn.importValue(`${pcenterVpcName}-VPCDefaultSG`),
        exportName: `${this.stackName}-DefaultSecurityGroup`,
      });

      new cdk.CfnOutput(this, 'PrivateSubnets', {
        value: privateSubnets.map((sn) => sn.subnetId).join(','),
        exportName: `${this.stackName}-PrivateSubnets`,
      });
    } else {
      // configure S3 endpoint
      this.vpc.addGatewayEndpoint('S3Endpoint', { service: ec2.GatewayVpcEndpointAwsService.S3 });

      new cdk.CfnOutput(this, 'DefaultSecurityGroup', {
        value: (this.vpc as ec2.Vpc).vpcDefaultSecurityGroup,
        exportName: `${this.stackName}-DefaultSecurityGroup`,
      });

      new cdk.CfnOutput(this, 'PrivateSubnets', {
        value: this.vpc.privateSubnets.map((sn) => sn.subnetId).join(','),
        exportName: `${this.stackName}-PrivateSubnets`,
      });
    }

    new cdk.CfnOutput(this, 'VpcId', {
      value: this.vpc.vpcId,
      exportName: `${this.stackName}-VpcId`,
    });

    new cdk.CfnOutput(this, 'PublicSubnets', {
      value: this.vpc.publicSubnets.map((sn) => sn.subnetId).join(','),
      exportName: `${this.stackName}-PublicSubnets`,
    });

    new cdk.CfnOutput(this, 'PublicIps', {
      value: outputPaths.map((path) => describeNatGateways.getResponseField(path)).join(','),
      exportName: `${this.stackName}-PublicIps`,
    });
  }
}
