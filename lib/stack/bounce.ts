import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';
import * as sns from '@aws-cdk/aws-sns';
import * as lambda from '@aws-cdk/aws-lambda';
import * as snsSubscription from '@aws-cdk/aws-sns-subscriptions';
import * as sqs from '@aws-cdk/aws-sqs';
import * as targets from '@aws-cdk/aws-events-targets';
import * as events from '@aws-cdk/aws-events';
import * as cloudwatch from '@aws-cdk/aws-cloudwatch';
import { SnsAction } from '@aws-cdk/aws-cloudwatch-actions';

import { TPCIStack, TPCIStackProps } from './tpci';

// This stack is for the SNS, SQS, and Lambda for sending daily bounce alerts

interface SesBounceStackProps extends TPCIStackProps {
  recipients?: string[];
}

export class SESBounceStack extends TPCIStack {
  constructor(scope: cdk.Construct, id: string, props: SesBounceStackProps) {
    super(scope, id, props);

    const { recipients = [] } = props;

    const topic = new sns.Topic(this, `BounceTopic`, {
      topicName: this.stackName,
      displayName: 'SES Bounce Notifications',
    });

    const queue = new sqs.Queue(this, 'BounceQueue', {
      queueName: this.stackName,
    });

    topic.addSubscription(new snsSubscription.SqsSubscription(queue));

    const alarm = queue
      .metricApproximateNumberOfMessagesVisible({
        period: cdk.Duration.minutes(1),
        statistic: 'Sum',
        dimensions: {
          QueueName: queue.queueName,
        },
      })
      .createAlarm(this, 'Alarm', {
        alarmName: `${this.stackName}`,
        alarmDescription: 'SES Bounces/Complaints',
        comparisonOperator: cloudwatch.ComparisonOperator.GREATER_THAN_THRESHOLD,
        threshold: 0,
        evaluationPeriods: 1,
        datapointsToAlarm: 1,
      });

    const alarmTopic = new sns.Topic(this, `AlarmNotifications`, {
      topicName: `${this.stackName}-alarm-notifications`,
      displayName: `${this.stackName}-alarm-notifications`,
    });

    alarm.addAlarmAction(new SnsAction(alarmTopic));

    const dailyTopic = new sns.Topic(this, `DailyAlertsTopic`, {
      topicName: `${this.stackName}-daily-alerts`,
      displayName: `${this.stackName}-daily-alerts`,
    });

    recipients.forEach((email) => {
      dailyTopic.addSubscription(new snsSubscription.EmailSubscription(email));
    });

    const lambdaRole = new iam.Role(this, 'LambdaRole', {
      assumedBy: new iam.CompositePrincipal(
        new iam.ServicePrincipal('lambda.amazonaws.com'),
        new iam.ServicePrincipal('events.amazonaws.com'),
      ),
      managedPolicies: [
        iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'),
        iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaSQSQueueExecutionRole'),
      ],
    });

    queue.grantConsumeMessages(lambdaRole);
    dailyTopic.grantPublish(lambdaRole);

    const alertFunc = new lambda.Function(this, 'DailyAlertsFunction', {
      functionName: this.stackName + '-daily-alerts',
      code: lambda.Code.asset('./dist/lambda/bounce'),
      runtime: lambda.Runtime.NODEJS_14_X,
      handler: 'index.handler',
      role: lambdaRole,
      environment: {
        QUEUE: queue.queueName,
        TOPIC: dailyTopic.topicArn,
      },
      timeout: cdk.Duration.minutes(1),
    });

    new events.Rule(this, 'DailyEvent', {
      // Everyday at 4PM UTC/8AM PST
      schedule: events.Schedule.cron({ minute: '0', hour: '16' }),
      targets: [new targets.LambdaFunction(alertFunc)],
    });
  }
}
