import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';

export class DAMS3 extends TPCIStack {
  public bucket: s3.Bucket;

  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);
    this.bucket = new s3.Bucket(this, 'Bucket', {
      bucketName: `tpci-damroot-${this.stage}`,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
    });
  }
}
