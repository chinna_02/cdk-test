import * as cdk from '@aws-cdk/core';
import * as s3 from '@aws-cdk/aws-s3';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import * as sns from '@aws-cdk/aws-sns';
import * as s3n from '@aws-cdk/aws-s3-notifications';
import * as iam from '@aws-cdk/aws-iam';
import { prod } from '../accounts';
import { getQueuePrincipal } from '../pim';

export class PIMS3 extends TPCIStack {
  public bucket: s3.Bucket;
  public topic: sns.Topic;

  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);

    this.bucket = new s3.Bucket(this, 'Bucket', {
      bucketName: `tpci-pim-${this.stage}`,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      lifecycleRules: [
        {
          expiration: cdk.Duration.days(14),
        },
      ],
    });

    const isProd = this.account === prod && this.stage === 'prod';
    const epAccountId = isProd
      ? 'arn:aws:iam::994670059680:root'
      : 'arn:aws:iam::406697379181:root';

    //create topic for S3 event notification
    this.topic = new sns.Topic(this, `topic`, {
      displayName: 'PIM user topic',
      topicName: `tpci-pim-topic-${this.stage}`,
    });

    const queuePrincipalName = getQueuePrincipal(this.stage);

    // set topic access policy
    const policy = new iam.PolicyStatement({
      resources: [this.topic.topicArn],
      actions: ['SNS:Subscribe'],
      principals: [new iam.ArnPrincipal(queuePrincipalName)],
      conditions: {
        Bool: {
          'aws:SecureTransport': 'true',
        },
      },
    });

    const policyeu = new iam.PolicyStatement({
      resources: [this.topic.topicArn],
      actions: ['SNS:Subscribe'],
      principals: [new iam.ArnPrincipal(epAccountId)],
      conditions: {
        Bool: {
          'aws:SecureTransport': 'true',
        },
      },
    });
    this.topic.addToResourcePolicy(policy);
    this.topic.addToResourcePolicy(policyeu);

    //add event notification for S3 bucket
    this.bucket.addEventNotification(
      s3.EventType.OBJECT_CREATED_PUT,
      new s3n.SnsDestination(this.topic),
    );

    // s3 bucket for AX PIM integration
    this.bucket = new s3.Bucket(this, 'AxPimBucket', {
      bucketName: `tpci-ax-pim-${this.stage}`,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      lifecycleRules: [
        {
          expiration: cdk.Duration.days(14),
        },
      ],
    });
  }
}
