import * as cdk from '@aws-cdk/core';
import * as cf from '@aws-cdk/aws-cloudfront';
import * as iam from '@aws-cdk/aws-iam';
import * as lambda from '@aws-cdk/aws-lambda';
import * as route53 from '@aws-cdk/aws-route53';
import * as s3 from '@aws-cdk/aws-s3';
import * as events from '@aws-cdk/aws-events';
import * as targets from '@aws-cdk/aws-events-targets';
import * as kinesisfirehose from '@aws-cdk/aws-kinesisfirehose';
import * as acm from '@aws-cdk/aws-certificatemanager';
import {
  AwsCustomResource,
  AwsCustomResourcePolicy,
  AwsSdkCall,
  PhysicalResourceId,
} from '@aws-cdk/custom-resources';
import {
  behaviors,
  cache,
  disableCache,
  Cache,
  CloudFrontBaseStack,
  CloudFrontBaseStackProps,
  CloudFrontStage,
} from '../cloudfront';

import fs from 'fs';
import crypto from 'crypto';
import glob from 'glob';
import r53targets = require('@aws-cdk/aws-route53-targets/lib');

import { GetApiKey } from '../get-apikey';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { ProductImages } from '../cloudfront/product-images';
import { CloudFrontWAF } from '../cloudfront/waf';
import { certificateArn } from '../certs';
import { toTitleCase } from '../utils';
import { HttpOrigin, S3Origin } from '@aws-cdk/aws-cloudfront-origins';
import { Duration } from '@aws-cdk/core';
import { EdgeLambda } from '../cloudfront/edge-lambda';
import { FeatureToggle } from '../feature-toggle';
import { MaintenanceToggle } from '../cloudfront/maintenance';
import { ModifyDocumentPermissionCommand } from '@aws-sdk/client-ssm';

// TODO these should be retrieved automatically during deployment
// using AwsSdkCall instead of hardcoding
const uiApiGatewayIds: { [key in CloudFrontStage]: string } = {
  dev: 'klp839918g',
  stage: 'a2b0a69f0b',
  ppd: 'a000a5hn5f',
  prod: '60zaax86n0',
  hfe: '7ritljrln8',
};

// TODO these should be retrieved automatically during deployment
// using AwsSdkCall instead of hardcoding
const apiApiGatewayIds: { [key in CloudFrontStage]: string } = {
  dev: '2pgyrh4ao1',
  stage: 'c609eitot6',
  ppd: 't2prwo07ra',
  prod: 'b5g6plcqx6',
  hfe: 'gkzg0j317j',
};

const { VIEWER_REQUEST, ORIGIN_REQUEST, ORIGIN_RESPONSE } = cf.LambdaEdgeEventType;

type EdgeLambdas = {
  [key in cf.LambdaEdgeEventType]?: lambda.IVersion;
};

function lambdaAssociations(lambdas: EdgeLambdas): cf.EdgeLambda[] {
  const res: cf.EdgeLambda[] = [];
  Object.entries(lambdas).forEach(
    ([eventType, EdgeFunction]: [string, lambda.IVersion | undefined]) => {
      if (EdgeFunction) {
        res.push({
          eventType: eventType as cf.LambdaEdgeEventType,
          functionVersion: EdgeFunction,
        });
      }
    },
  );
  return res;
}

interface CloudFrontStackProps extends CloudFrontBaseStackProps {
  allSitesMaintanceMode: boolean;
  caSiteMaintanceMode: boolean;
}

export class CloudFrontStack extends CloudFrontBaseStack {
  constructor(scope: cdk.Construct, id: string, props: CloudFrontStackProps) {
    super(scope, id, props);

    const uiDomain = `${this.stage}.ui.pokemoncenter.com`;
    const apiDomain = `${this.stage}.api.pokemoncenter.com`;
    const uiDomainCa = `${this.stage}.ui.pokemoncenter.ca`;
    const uiDomainUk = `${this.stage}.ui.pokemon-center.co.uk`;
    const uiDomainTest = 'test.pokemoncenter.com';
    const betaDomain = this.stage === 'prod' && 'beta.pokemoncenter.com';

    const uiApiGatewayId = uiApiGatewayIds[this.stage];
    const apiApiGatewayId = apiApiGatewayIds[this.stage];

    const domainNames = [uiDomain, apiDomain];
    if (['dev', 'stage', 'ppd', 'hfe'].includes(this.stage)) {
      domainNames.push(uiDomainCa);
    }
    if (['dev', 'stage'].includes(this.stage)) {
      domainNames.push(uiDomainUk);
    }
    if (this.stage === 'stage') {
      domainNames.push(uiDomainTest);
    }
    if (betaDomain) {
      domainNames.push(betaDomain);
    }

    if (this.stage === 'prod') {
      domainNames.push(
        'pokemoncenter.com',
        'www.pokemoncenter.com',
        'www.pokemoncenter.ca',
        'pokemoncenter.ca',
        'www.pokemon-center.co.uk',
        'pokemon-center.co.uk',
      );
    }

    // localize and old product redirects
    const uiOriginRequestLambda = new EdgeLambda(this, 'uiOriginRequest').currentVersion;

    // rewrites API Gateway URIs and EU
    const apiOriginRequestLambda = new EdgeLambda(this, 'apiOriginRequest').currentVersion;

    // injects Cache-Control and security headers
    const originResponseLambda = new EdgeLambda(this, 'originResponse').currentVersion;

    // Rewrites
    const s3OriginRequestLambda = new EdgeLambda(this, 's3OriginRequest', {
      runtime: lambda.Runtime.NODEJS_14_X,
    }).currentVersion;

    // Redirects non-www to www and DataDome Bot Protection
    const viewerRequestLambda = new EdgeLambda(this, 'viewerRequest', {
      runtime: lambda.Runtime.NODEJS_14_X,
    }).currentVersion;

    // Redirects all responses to maintenance/index.html to turn site into maintenance mode
    const maintenanceResponseLambda = new EdgeLambda(this, 'maintenanceRedirectResponse', {
      runtime: lambda.Runtime.NODEJS_14_X,
    }).currentVersion;

    const getApiKey = new GetApiKey(this, 'GetApiKey', {
      apiKeyName: `tpci-aws-cdk-api-wafs-cloudfront-key-${this.stage}`,
      region: 'us-west-2',
    });

    const uiBucketName = `${this.isProdAccount ? 'prod' : 'np'}-tpci-ecommweb-lambda-${
      this.isDevAccount ? `${this.stage === 'hfe' ? 'hfe' : 'dev'}` : 'ppd'
    }`;

    const uiBucket: s3.IBucket = s3.Bucket.fromBucketAttributes(this, 'UIBucket', {
      bucketName: uiBucketName,
      bucketRegionalDomainName: `${uiBucketName}.s3.us-west-2.amazonaws.com`,
    });

    const uiEuBucketName = `${this.isProdAccount ? 'prod' : 'np'}-tpci-ecommweb-lambda-${
      this.isDevAccount ? 'dev' : 'ppd'
    }-eu-west-1`;

    const uiEuBucket: s3.IBucket = s3.Bucket.fromBucketAttributes(this, 'UIEuBucket', {
      bucketName: uiEuBucketName,
      bucketRegionalDomainName: `${uiEuBucketName}.s3.us-west-2.amazonaws.com`,
    });

    const apiBucketName = `${this.isProdAccount ? 'prod' : 'np'}-tpci-ecommweb-api-${
      this.isDevAccount ? `${this.stage === 'hfe' ? 'hfe' : 'dev'}` : 'ppd'
    }`;

    const apiBucket: s3.IBucket = s3.Bucket.fromBucketAttributes(this, 'APIBucket', {
      bucketName: apiBucketName,
      bucketRegionalDomainName: `${apiBucketName}.s3.us-west-2.amazonaws.com`,
    });

    const apiEuBucketName = `${this.isProdAccount ? 'prod' : 'np'}-tpci-ecommweb-api-${
      this.isDevAccount ? 'dev' : 'ppd'
    }-eu-west-1`;
    const apiEuBucket: s3.IBucket = s3.Bucket.fromBucketAttributes(this, 'APIEuBucket', {
      bucketName: apiEuBucketName,
      bucketRegionalDomainName: `${apiEuBucketName}.s3.us-west-2.amazonaws.com`,
    });

    // There can only be one BucketPolicy defined per bucket.
    //
    // Multiple CloudFronts with unique OAIs would need to access
    // the bucket that is shared between dev/stage and ppd/prod.
    //
    // Instead we only define an OAI and bucket policy in dev and ppd
    // and share that OAI with stage and prod respectively.
    //
    // If static assets were hosted in a unique bucket per environment (dev, stage, ppd, prod),
    // then each CloudFront would use its own OAI
    let oai;

    // The other stage on the same AWS account
    const otherStage: Stage = (() => {
      switch (this.stage) {
        case 'dev':
          return 'stage';
        case 'stage':
          return 'dev';
        case 'ppd':
          return 'prod';
        case 'prod':
          return 'ppd';
        case 'hfe':
          return 'dev';
        default:
          throw new Error(`Unsupported stage: ${this.stage}`);
      }
    })();

    if (['dev', 'ppd', 'hfe'].includes(this.stage)) {
      oai = new cf.OriginAccessIdentity(this, 'OriginAccessIdentity', {
        comment: this.stackName,
      });

      new s3.BucketPolicy(this, 'StaticBucketPolicy', {
        bucket: uiBucket,
      }).document.addStatements(
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: ['s3:GetObject'],
          principals: [oai.grantPrincipal],
          resources: [
            `arn:aws:s3:::${uiBucketName}/favicon.ico`,
            `arn:aws:s3:::${uiBucketName}/robots.txt`,
            `arn:aws:s3:::${uiBucketName}/static/*`,
            `arn:aws:s3:::${uiBucketName}/_next/*`,
          ],
        }),
      );

      new s3.BucketPolicy(this, 'ApiBucketPolicy', {
        bucket: apiBucket,
      }).document.addStatements(
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: ['s3:GetObject'],
          principals: [oai.grantPrincipal],
          resources: [
            ...(['dev', 'ppd'].includes(this.stage)
              ? [
                  `arn:aws:s3:::${apiBucketName}/${otherStage}/sitemaps/*`,
                  `arn:aws:s3:::${apiBucketName}/${otherStage}/sitemap.xml`,
                ]
              : []),
            `arn:aws:s3:::${apiBucketName}/${this.stage}/sitemaps/*`,
            `arn:aws:s3:::${apiBucketName}/${this.stage}/sitemap.xml`,
          ],
        }),
      );
      new s3.BucketPolicy(this, 'StaticEuBucketPolicy', {
        bucket: uiEuBucket,
      }).document.addStatements(
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: ['s3:GetObject'],
          principals: [oai.grantPrincipal],
          resources: [
            `arn:aws:s3:::${uiEuBucketName}/favicon.ico`,
            `arn:aws:s3:::${uiEuBucketName}/robots.txt`,
            `arn:aws:s3:::${uiEuBucketName}/static/*`,
            `arn:aws:s3:::${uiEuBucketName}/_next/*`,
          ],
        }),
      );
      new s3.BucketPolicy(this, 'ApiEuBucketPolicy', {
        bucket: apiEuBucket,
      }).document.addStatements(
        new iam.PolicyStatement({
          effect: iam.Effect.ALLOW,
          actions: ['s3:GetObject'],
          principals: [oai.grantPrincipal],
          resources: [
            ...(['dev', 'ppd'].includes(this.stage)
              ? [
                  `arn:aws:s3:::${apiEuBucketName}/${otherStage}/sitemaps/*`,
                  `arn:aws:s3:::${apiEuBucketName}/${otherStage}/sitemap.xml`,
                ]
              : []),
            `arn:aws:s3:::${apiEuBucketName}/${this.stage}/sitemaps/*`,
            `arn:aws:s3:::${apiEuBucketName}/${this.stage}/sitemap.xml`,
          ],
        }),
      );

      new cdk.CfnOutput(this, 'OAIOuptut', {
        value: oai.originAccessIdentityName,
        exportName: `tpci-aws-cdk-cloudfront-originAccessIdentityName-${this.stage}`,
      });
    } else {
      const originAccessIdentityName = cdk.Fn.importValue(
        `tpci-aws-cdk-cloudfront-originAccessIdentityName-${otherStage}`,
      );
      oai = cf.OriginAccessIdentity.fromOriginAccessIdentityName(
        this,
        'OAI',
        originAccessIdentityName,
      );
    }

    // Feature toggle for maintenance page
    const maintenanceToggle = new MaintenanceToggle(this, 'maintenanceToggle', {
      uiBucket,
    });

    const productImages = new ProductImages(this, 'ProductImages', {
      stage: this.stage,
      domainName: uiDomain,
      oai,
    });

    const getBRCredsSdkCall: AwsSdkCall = {
      service: 'SecretsManager',
      action: 'getSecretValue',
      parameters: {
        SecretId: `${this.stage}/bloomreach/xm-basic-auth`,
      },
      outputPath: 'SecretString',
      physicalResourceId: PhysicalResourceId.of(Date.now().toString()), // Update physical id to always fetch the latest version
      region: 'us-west-2',
    };

    const getBRCreds = new AwsCustomResource(this, 'GetBRCreds', {
      onCreate: getBRCredsSdkCall,
      onUpdate: getBRCredsSdkCall,
      policy: AwsCustomResourcePolicy.fromSdkCalls({
        resources: AwsCustomResourcePolicy.ANY_RESOURCE,
      }),
    });

    const staticFilesCachePolicy = new cf.CachePolicy(this, 'StaticFilesCachePolicy', {
      cachePolicyName: this.resourceName('static-files'),
      comment: 'Cache static files for a day',
      enableAcceptEncodingGzip: true,
      enableAcceptEncodingBrotli: true,
    });

    const apiDomainHttpsOrigin = new HttpOrigin(`customdomain.${apiDomain}`, {
      customHeaders: {
        ['x-api-key']: getApiKey.value,
      },
      protocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
      connectionAttempts: 3,
      connectionTimeout: Duration.seconds(10),
      keepaliveTimeout: Duration.seconds(5),
      httpPort: 80,
      httpsPort: 443,
      readTimeout: Duration.seconds(30),
    });
    const apiApiGatewayIdHttpsOrigin = new HttpOrigin(
      `${apiApiGatewayId}.execute-api.us-west-2.amazonaws.com`,
      {
        originPath: `/${this.stage}`,
        customHeaders: {
          ['x-api-key']: getApiKey.value,
        },
        protocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
        connectionAttempts: 3,
        connectionTimeout: Duration.seconds(10),
        keepaliveTimeout: Duration.seconds(5),
        httpPort: 80,
        httpsPort: 443,
        readTimeout: Duration.seconds(30),
      },
    );

    const distribution = new cf.Distribution(this, `distribution`, {
      defaultRootObject: props.allSitesMaintanceMode ? '/index.html' : '',
      enableLogging: true,
      logBucket: new s3.Bucket(this, 'LogBucket', {
        bucketName: this.stackName,
        lifecycleRules: [
          {
            enabled: true,
            expiration: cdk.Duration.days(30),
          },
        ],
      }),
      logIncludesCookies: true,
      minimumProtocolVersion: cf.SecurityPolicyProtocol.TLS_V1_2_2019,
      domainNames: domainNames,
      certificate: acm.Certificate.fromCertificateArn(this, 'Cert', certificateArn(this.stage)),
      errorResponses:
        this.stage === 'prod'
          ? [
              {
                httpStatus: 403,
                ttl: cdk.Duration.seconds(0),
                ...(props.allSitesMaintanceMode
                  ? { responsePagePath: '/index.html', responseHttpStatus: 503 }
                  : {}),
              },
            ]
          : undefined,
      additionalBehaviors: {
        ...behaviors(
          [
            '/static/*', //
            '/_next/*',
            'favicon.ico',
            'robots.txt',
          ],
          {
            ...this.cache(cdk.Duration.days(1), ['stage', 'prod']),
            ...(['stage', 'ppd', 'prod'].includes(this.stage)
              ? {
                  lambdaFunctionAssociations: lambdaAssociations({
                    [ORIGIN_REQUEST]: s3OriginRequestLambda,
                  }),
                }
              : {}),
            compress: true,
            origin: new S3Origin(uiBucket),
            cachePolicy: staticFilesCachePolicy,
            allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
            viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
            cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
            originRequestPolicy: new cf.OriginRequestPolicy(
              this,
              'StaticFilesOriginRequestPolicy',
              {
                queryStringBehavior: cf.OriginRequestQueryStringBehavior.none(),
                ...(['stage', 'ppd', 'prod'].includes(this.stage)
                  ? {
                      headerBehavior: cf.OriginRequestHeaderBehavior.allowList('Referer'),
                      cookieBehavior: cf.OriginRequestCookieBehavior.allowList('NEXT_LOCALE'),
                    }
                  : {}),
              },
            ),
          },
        ),

        // tpci-ecommweb-lambda static files on S3 that must be at root

        ['/apple-touch-icon*.png']: {
          origin: new S3Origin(uiBucket, {
            originAccessIdentity: oai,
            originPath: '/static',
          }),
          compress: true,
          cachePolicy: new cf.CachePolicy(this, 'lambdaStaticFilesCachePolicy', {
            cachePolicyName: this.resourceName('lambda-static-files'),
            comment: 'A default policy',
            ...this.cache(cdk.Duration.days(1), ['dev', 'stage', 'ppd', 'prod']),
            enableAcceptEncodingGzip: true,
            enableAcceptEncodingBrotli: true,
          }),
          allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
        },

        // sitemap files in the S3 bucket from tpci-ecommweb-api
        ...behaviors(
          [
            '/sitemaps/*', //
            '/sitemap.xml',
          ],
          {
            cachePolicy: cf.CachePolicy.CACHING_DISABLED,
            compress: true,
            origin: new S3Origin(apiBucket, {
              originAccessIdentity: oai,
              originPath: `/${this.stage}`,
            }),
            allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
            cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
            viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          },
        ),

        // OMS APIs
        ['/tpci-fluent-webhook/*']: {
          ...disableCache,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'WebhookRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
            headerBehavior: cf.OriginRequestHeaderBehavior.allowList('flex.signature'),
          }),
          origin: apiDomainHttpsOrigin,
        },

        ['/tpci-order-status/*']: {
          ...disableCache,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'OrderStatusRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
          }),
          origin: apiDomainHttpsOrigin,
        },

        ['/tpci-inventory-sync/*']: {
          ...disableCache,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'InventorySyncRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
          }),
          origin: apiDomainHttpsOrigin,
        },

        ['/tpci-transaction-email-api/*']: {
          ...disableCache,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'EmailApiRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
          }),
          origin: apiDomainHttpsOrigin,
        },

        ['/tpci-aws-sam-ref-api/*']: {
          ...disableCache,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'SamRefRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
          }),
          origin: apiDomainHttpsOrigin,
        },

        // /products/images/*.jpg

        ['/products/images/*.jpg']: {
          ...this.cache(cdk.Duration.days(1), ['stage', 'prod']),
          origin: new S3Origin(productImages.s3BucketSource, {
            originAccessIdentity: productImages.originAccessIdentity,
          }),
          compress: true,
          allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
        },

        // Maintenance Page
        // If the feature toggle in AWS System Manager Parameter Store in us-east-1 region
        // named `/${this.stage}/feature-toggle/tpci-aws-cdk/maintenance` is set to "true",
        // then respond with the maintenance page for all requests
        ...(props.allSitesMaintanceMode
          ? {
              ['/*']: {
                ...disableCache,
                compress: true,
                origin: new S3Origin(uiBucket, {
                  originAccessIdentity: oai,
                  originPath: '/maintenance/',
                }),
              },
            }
          : {}),

        // Canada Site Maintenance Page
        // If the feature toggle in AWS System Manager Parameter Store in us-east-1 region
        // named `/${this.stage}/feature-toggle/tpci-aws-cdk/ca-maintenance` is set to "true",
        // then respond with the maintenance page for only CA Site requests
        ...(props.caSiteMaintanceMode
          ? {
              ['/en-ca*']: {
                cachePolicy: cf.CachePolicy.CACHING_DISABLED,
                compress: true,
                origin: new S3Origin(uiBucket, {
                  originAccessIdentity: oai,
                  originPath: '/maintenance/',
                }),
                originRequestPolicy: new cf.OriginRequestPolicy(this, 'maintenance-page', {
                  headerBehavior: cf.OriginRequestHeaderBehavior.allowList('HOST'),
                }),
                edgeLambdas: lambdaAssociations({
                  [ORIGIN_RESPONSE]: maintenanceResponseLambda,
                }),
              },

              ['/maintenance/*']: {
                cachePolicy: cf.CachePolicy.CACHING_DISABLED,
                compress: true,
                origin: new S3Origin(uiBucket, {
                  originAccessIdentity: oai,
                }),
              },
            }
          : {}),

        // Search APIs
        ['/tpci-ecommweb-api/search']: {
          ...this.cache(cdk.Duration.hours(1), ['stage', 'prod']),
          origin: apiApiGatewayIdHttpsOrigin,
          compress: true,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'SearchApi', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
            headerBehavior: cf.OriginRequestHeaderBehavior.allowList('x-store-scope'),
            cookieBehavior: cf.OriginRequestCookieBehavior.none(),
          }),
          allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          edgeLambdas: lambdaAssociations({
            [VIEWER_REQUEST]: viewerRequestLambda,
            [ORIGIN_REQUEST]: apiOriginRequestLambda,
            [ORIGIN_RESPONSE]: originResponseLambda,
          }),
        },

        ['/tpci-ecommweb-api/suggest']: {
          ...this.cache(cdk.Duration.hours(1), ['stage', 'prod']),
          origin: apiApiGatewayIdHttpsOrigin,
          compress: true,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'Suggest', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
            headerBehavior: cf.OriginRequestHeaderBehavior.allowList('x-store-scope'),
            cookieBehavior: cf.OriginRequestCookieBehavior.none(),
          }),
          allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          edgeLambdas: lambdaAssociations({
            [VIEWER_REQUEST]: viewerRequestLambda,
            [ORIGIN_REQUEST]: apiOriginRequestLambda,
            [ORIGIN_RESPONSE]: originResponseLambda,
          }),
        },

        // API's

        ['/tpci-ecommweb-api/product/*']: {
          allowedMethods: cf.AllowedMethods.ALLOW_GET_HEAD_OPTIONS,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD_OPTIONS,
          compress: true,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'ProductRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
            headerBehavior: cf.OriginRequestHeaderBehavior.allowList('x-store-scope'),
          }),
          edgeLambdas: lambdaAssociations({
            [VIEWER_REQUEST]: viewerRequestLambda,
            [ORIGIN_REQUEST]: apiOriginRequestLambda,
            [ORIGIN_RESPONSE]: originResponseLambda,
          }),
          origin: apiApiGatewayIdHttpsOrigin,
        },

        ['/tpci-ecommweb-api/*']: {
          compress: true,
          cachePolicy: new cf.CachePolicy(this, 'tpciecommwebapipolicy', {
            cachePolicyName: this.resourceName('ecommweb-api'),
            minTtl: cdk.Duration.seconds(0),
            maxTtl: this.stage === 'dev' ? cdk.Duration.seconds(1) : cdk.Duration.hours(1),
            //maxTtl: cdk.Duration.hours(1),
            defaultTtl: cdk.Duration.seconds(0),
            cookieBehavior: cf.CacheCookieBehavior.all(),
            queryStringBehavior: cf.CacheQueryStringBehavior.all(),
            headerBehavior: cf.CacheHeaderBehavior.allowList('Authorization', 'x-store-scope'),
          }),
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          edgeLambdas: lambdaAssociations({
            [VIEWER_REQUEST]: viewerRequestLambda,
            [ORIGIN_REQUEST]: apiOriginRequestLambda,
            [ORIGIN_RESPONSE]: originResponseLambda,
          }),
          origin: apiApiGatewayIdHttpsOrigin,
        },

        // Bloomreach Proxy
        ['/site/*']: {
          ...this.cache(cdk.Duration.hours(1), ['stage', 'prod']),
          edgeLambdas: lambdaAssociations({
            [VIEWER_REQUEST]: viewerRequestLambda,
          }),
          viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
          allowedMethods: cf.AllowedMethods.ALLOW_ALL,
          cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
          originRequestPolicy: new cf.OriginRequestPolicy(this, 'BloomreachRequestPolicy', {
            queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
            headerBehavior: cf.OriginRequestHeaderBehavior.allowList('Origin'),
            cookieBehavior: cf.OriginRequestCookieBehavior.none(),
          }),
          origin: new HttpOrigin(`${this.stage}.cms.pokemoncenter.com`, {
            protocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
            connectionAttempts: 3,
            connectionTimeout: Duration.seconds(10),
            keepaliveTimeout: Duration.seconds(5),
            httpPort: 80,
            httpsPort: 443,
            readTimeout: Duration.seconds(30),
            customHeaders: {
              ['Authorization']: `Basic ${getBRCreds.getResponseField('SecretString').toString()}`,
            },
          }),
        },
      },
      defaultBehavior: {
        edgeLambdas: lambdaAssociations({
          [VIEWER_REQUEST]: viewerRequestLambda,
          [ORIGIN_REQUEST]: uiOriginRequestLambda,
          [ORIGIN_RESPONSE]: originResponseLambda,
        }),
        allowedMethods: cf.AllowedMethods.ALLOW_ALL,
        cachedMethods: cf.CachedMethods.CACHE_GET_HEAD,
        originRequestPolicy: new cf.OriginRequestPolicy(this, 'apirequestpolicy', {
          cookieBehavior: cf.OriginRequestCookieBehavior.allowList(
            'auth',
            'complianceCookie',
            'correlationId',
            'NEXT_LOCALE',
          ),
          headerBehavior: cf.OriginRequestHeaderBehavior.allowList('SiteSpect'),
          queryStringBehavior: cf.OriginRequestQueryStringBehavior.all(),
        }),
        viewerProtocolPolicy: cf.ViewerProtocolPolicy.REDIRECT_TO_HTTPS,
        origin: new HttpOrigin(`${uiApiGatewayId}.execute-api.us-west-2.amazonaws.com`, {
          originPath: `/${this.stage}`,
          protocolPolicy: cf.OriginProtocolPolicy.HTTPS_ONLY,
          connectionAttempts: 3,
          connectionTimeout: Duration.seconds(10),
          keepaliveTimeout: Duration.seconds(5),
          httpPort: 80,
          httpsPort: 443,
          readTimeout: Duration.seconds(30),
          customHeaders: {
            ['x-api-key']: getApiKey.value,
          },
        }),
        compress: true,
      },
      webAclId: new CloudFrontWAF(this, 'CloudFrontWAF').acl.attrArn,
    });

    const distributionId: { [key in Stage]: string } = {
      dev: 'tpciCloudFrontWebDistributionCFDistribution9F34B88BDev',
      stage: 'tpciCloudFrontWebDistributionCFDistribution9F34B88BStage',
      ppd: 'tpciCloudFrontWebDistributionCFDistribution9F34B88BPpd',
      prod: 'tpciCloudFrontWebDistributionCFDistribution9F34B88BProd',
      hfe: 'tpciCloudFrontWebDistributionCFDistribution9F34B88BHfe',
      //EU distribution logical-id added to resolve issue later replace it with original logical id's
      'stage-eu': 'tpciCloudFrontWebDistributionCFDistribution9F34B88BStage-eu',
      'ppd-eu': 'tpciCloudFrontWebDistributionCFDistribution9F34B88BPpd-eu',
      'prod-eu': 'tpciCloudFrontWebDistributionCFDistribution9F34B88BProd-eu',
    };
    const cfnDistribution = distribution.node.defaultChild as cf.CfnDistribution;
    cfnDistribution.overrideLogicalId(distributionId[this.stage]);

    const invalidateFn = new lambda.Function(this, 'InvalidateFunction', {
      code: lambda.Code.asset('./dist/lambda/invalidate'),
      handler: 'index.handler',
      timeout: cdk.Duration.seconds(10),
      runtime: lambda.Runtime.NODEJS_14_X,
      environment: {
        CLOUDFRONT: distribution.distributionId,
      },
      role: new iam.Role(this, 'InvalidateRole', {
        assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
        managedPolicies: [
          iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'),
        ],
        inlinePolicies: {
          CloudFront: new iam.PolicyDocument({
            statements: [
              new iam.PolicyStatement({
                actions: [
                  'cloudfront:GetDistribution',
                  'cloudfront:GetDistributionConfig',
                  'cloudfront:CreateInvalidation',
                ],
                resources: [
                  cdk.Arn.format(
                    {
                      service: 'cloudfront',
                      resource: 'distribution',
                      region: '',
                      resourceName: distribution.distributionId,
                    },
                    this,
                  ),
                ],
              }),
            ],
          }),
        },
      }),
    });
    // Invalidate the cache every day at 5 AM EST / 6 AM EDT
    const rule = new events.Rule(this, 'InvalidateSchedule', {
      schedule: events.Schedule.cron({
        minute: '0',
        hour: '10',
      }),
    });

    rule.addTarget(new targets.LambdaFunction(invalidateFn));

    new route53.ARecord(this, `UiAliasRecord`, {
      recordName: '',
      target: route53.RecordTarget.fromAlias(new r53targets.CloudFrontTarget(distribution)),
      zone: route53.HostedZone.fromLookup(this, `UiZone`, {
        domainName: uiDomain,
      }),
    });

    new route53.ARecord(this, `ApiAliasRecord`, {
      recordName: '',
      target: route53.RecordTarget.fromAlias(new r53targets.CloudFrontTarget(distribution)),
      zone: route53.HostedZone.fromLookup(this, `ApiZone`, {
        domainName: apiDomain,
      }),
    });

    if (betaDomain) {
      const betaZone = route53.HostedZone.fromLookup(this, `BetaZone`, {
        domainName: betaDomain,
      });

      new route53.ARecord(this, `BetaAliasRecord`, {
        recordName: '',
        target: route53.RecordTarget.fromAlias(new r53targets.CloudFrontTarget(distribution)),
        zone: betaZone,
      });
    }
  }

  cache = (duration: cdk.Duration, environments: Stage[] = []): Cache => {
    return cache(this.stage, duration, environments);
  };
}

export default CloudFrontStack;
