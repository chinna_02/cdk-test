import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';
import * as eks from '@aws-cdk/aws-eks';
import * as ec2 from '@aws-cdk/aws-ec2';
import { VpcStack } from './vpc';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { Cluster } from '@aws-cdk/aws-eks';

export class EKS extends TPCIStack {
  public readonly vpc: ec2.Vpc | ec2.IVpc;
  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);
    if (this.isDevAccount) {
      const pcenterVpcName = `pcenter-${this.accountName}-vpc`;

      const DefaultVpc = ec2.Vpc.fromLookup(this, 'pcenterVpc', {
        vpcName: pcenterVpcName,
      });
      // Creating an IAM role for EKS
      const EksRole = new iam.Role(this, 'EksRole', {
        roleName: `tpci-aws-cdk-eks-${this.stage}`,
        assumedBy: new iam.ServicePrincipal('eks.amazonaws.com'),
        managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName('AmazonEKSClusterPolicy')],
      });

      // Creating EKS Cluster
      const Ekscluster = new eks.FargateCluster(this, 'Ekscluster', {
        vpc: DefaultVpc,
        vpcSubnets: [
          {
            subnetType: ec2.SubnetType.PRIVATE,
          },
        ],
        version: eks.KubernetesVersion.V1_18,
        clusterName: `tpci-aws-cdk-eks-cluster-${this.stage}`,
        role: EksRole,
        endpointAccess: eks.EndpointAccess.PUBLIC,
      });

      Ekscluster.addNodegroupCapacity('eks-node-group', {
        instanceTypes: [new ec2.InstanceType('t2.large')],
        minSize: 1,
        diskSize: 50,
      });
    }
  }
}
