import * as cdk from '@aws-cdk/core';
import * as topic from '@aws-cdk/aws-sns';
import * as s3 from '@aws-cdk/aws-s3';
import * as iam from '@aws-cdk/aws-iam';
import * as lambda from '@aws-cdk/aws-lambda';
import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { Effect } from '@aws-cdk/aws-iam';
import { prod } from '../accounts';
import { SnsEventSource } from '@aws-cdk/aws-lambda-event-sources';

export class PimImageSyncStack extends TPCIStack {
  snsTopic: topic.ITopic;
  public readonly stage: Stage;

  constructor(scope: cdk.Construct, id: string, props?: TPCIStackProps) {
    super(scope, id, props);

    // SNS tpoic and policy statement
    this.snsTopic = new topic.Topic(this, 'PimSyncSns', {
      topicName: 'tpci-aws-cdk-pim-image-sync',
    });

    const isProd = this.account === prod;
    const epAccountId = isProd
      ? 'arn:aws:iam::727927018030:root'
      : 'arn:aws:iam::406697379181:root';
    const sourceBucket = isProd ? 'tpci-product-images-ppd' : 'tpci-product-images-dev';
    const destinationBucket = isProd ? 'tpci-product-images-prod' : 'tpci-product-images-stage';

    const snsPolicy = new iam.PolicyStatement({
      effect: Effect.ALLOW,
      resources: [this.snsTopic.topicArn],
      actions: ['sns:Publish'],
      principals: [new iam.ArnPrincipal(epAccountId)],
      conditions: {
        Bool: {
          'aws:SecureTransport': 'true',
        },
      },
    });

    this.snsTopic.addToResourcePolicy(snsPolicy);

    // PIM lambda role
    const pimImageSyncRole = new iam.Role(this, 'LambdaRole', {
      assumedBy: new iam.ServicePrincipal('lambda.amazonaws.com'),
      managedPolicies: [iam.ManagedPolicy.fromAwsManagedPolicyName('AWSLambdaExecute')],
    });

    const pimS3LambdaPolicyDocument = new iam.PolicyDocument({
      statements: [
        new iam.PolicyStatement({
          effect: Effect.ALLOW,
          actions: ['lambda:GetFunction', 'lambda:GetFunctionConfiguration'],
          resources: ['*'],
        }),

        new iam.PolicyStatement({
          effect: Effect.ALLOW,
          actions: ['s3:ListBucket', 's3:CreateJob', 'iam:PassRole'],
          resources: ['*'],
        }),
      ],
    });

    const pimS3LambdaPolicy = new iam.Policy(this, 'pimS3LambdaPolicy', {
      document: pimS3LambdaPolicyDocument,
    });

    pimImageSyncRole.attachInlinePolicy(pimS3LambdaPolicy);

    // S3 batch operation inventory bucket
    const pimBatchInventoryBucket = new s3.Bucket(this, 'Bucket', {
      bucketName:
        this.account === prod
          ? this.resourceName('s3batch-inventory')
          : `np-${this.resourceName('s3batch-inventory')}`,
      publicReadAccess: false,
      blockPublicAccess: s3.BlockPublicAccess.BLOCK_ALL,
      lifecycleRules: [
        {
          expiration: cdk.Duration.days(30),
        },
      ],
    });

    // S3 batch operation IAM role and it's policies
    const pimS3BatchPolicyDocument = new iam.PolicyDocument({
      statements: [
        new iam.PolicyStatement({
          effect: Effect.ALLOW,
          actions: ['s3:PutObject', 's3:PutObjectAcl', 's3:PutObjectTagging'],
          resources: [`arn:aws:s3:::${destinationBucket}/*`],
        }),

        new iam.PolicyStatement({
          effect: Effect.ALLOW,
          actions: ['s3:ListBucket', 's3:GetObject', 's3:GetObjectAcl', 's3:GetObjectTagging'],
          resources: [`arn:aws:s3:::${sourceBucket}`, `arn:aws:s3:::${sourceBucket}/*`],
        }),

        new iam.PolicyStatement({
          effect: Effect.ALLOW,
          actions: ['s3:GetObject', 's3:GetObjectVersion', 's3:PutObject'],
          resources: [pimBatchInventoryBucket.bucketArn.concat('/*')],
        }),
      ],
    });

    const pimS3BatchPolicy = new iam.Policy(this, 'pimS3BatchPolicy', {
      document: pimS3BatchPolicyDocument,
    });

    const pimS3BatchRole = new iam.Role(this, 'PimS3BatchRole', {
      assumedBy: new iam.ServicePrincipal('batchoperations.s3.amazonaws.com'),
    });

    pimS3BatchRole.attachInlinePolicy(pimS3BatchPolicy);

    const pimImageSyncLambda = new lambda.Function(this, 'PimImageSyncLambda', {
      functionName: this.resourceName('lambda'),
      runtime: lambda.Runtime.PYTHON_3_8,
      code: new lambda.AssetCode('./lambda/pimImageSync'),
      handler: 'lambda_function.lambda_handler',
      timeout: cdk.Duration.seconds(45),
      role: pimImageSyncRole,
    });

    pimImageSyncLambda.addEventSource(new SnsEventSource(this.snsTopic));
    pimImageSyncLambda.addEnvironment('S3BATCH_ROLE', pimS3BatchRole.roleArn);
    pimImageSyncLambda.addEnvironment('INVENTORY_BUCKET_NAME', pimBatchInventoryBucket.bucketName);
    pimImageSyncLambda.addEnvironment('SOURCE_BUCKET_NAME', sourceBucket);
    pimImageSyncLambda.addEnvironment('DESTINATION_BUCKET_NAME', destinationBucket);
  }
}
