import * as cdk from '@aws-cdk/core';
import { cache, disableCache } from '../cloudfront';

test('CloudFrontStack.cache()', () => {
  expect(cache('dev', cdk.Duration.hours(1), [])).toBe(disableCache);
  expect(cache('dev', cdk.Duration.hours(1), ['stage', 'prod'])).toBe(disableCache);
  expect(cache('dev', cdk.Duration.hours(1), ['dev'])).toStrictEqual({
    minTtl: cdk.Duration.seconds(10),
    defaultTtl: cdk.Duration.hours(1),
    maxTtl: cdk.Duration.hours(1),
  });
  expect(cache('dev', cdk.Duration.minutes(10), ['dev', 'stage', 'ppd', 'prod'])).toStrictEqual({
    minTtl: cdk.Duration.seconds(10),
    defaultTtl: cdk.Duration.minutes(10),
    maxTtl: cdk.Duration.minutes(10),
  });
});
