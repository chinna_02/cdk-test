import * as cdk from '@aws-cdk/core';
import * as acm from '@aws-cdk/aws-certificatemanager';
import * as apigw from '@aws-cdk/aws-apigateway';
import * as route53 from '@aws-cdk/aws-route53';
import targets = require('@aws-cdk/aws-route53-targets/lib');

import { TPCIStack, TPCIStackProps, Stage } from './tpci';
import { certificateArn } from '../certs';

export class ApiDomainStack extends TPCIStack {
  public readonly stage: Stage;

  constructor(scope: cdk.Construct, id: string, props: TPCIStackProps) {
    super(scope, id, props);

    const domainName = `${this.stage}.api.pokemoncenter.com`;

    const customDomain = new apigw.DomainName(this, 'CustomDomain', {
      domainName: `customdomain.${domainName}`,
      certificate: acm.Certificate.fromCertificateArn(
        this,
        'CertificateARN',
        certificateArn(this.stage),
      ),
      endpointType: apigw.EndpointType.EDGE,
      securityPolicy: apigw.SecurityPolicy.TLS_1_2,
    });

    const zone = route53.HostedZone.fromLookup(this, `Zone`, {
      domainName,
    });

    new route53.ARecord(this, `ARecord`, {
      recordName: 'customdomain',
      zone,
      target: route53.RecordTarget.fromAlias(new targets.ApiGatewayDomain(customDomain)),
    });
  }
}
