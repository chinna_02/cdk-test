import * as wafv2 from '@aws-cdk/aws-wafv2';
import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';
import * as cf from '@aws-cdk/aws-cloudfront';
import * as lambda from '@aws-cdk/aws-lambda';

import * as ips from './ips';
import { TPCIStackProps, Stage, TPCIStack } from './stack/tpci';
export * from './cloudfront/edge-lambda';

export interface Cache {
  minTtl: cdk.Duration;
  defaultTtl: cdk.Duration;
  maxTtl?: cdk.Duration;
}

export const disableCache: Cache = {
  minTtl: cdk.Duration.seconds(0),
  maxTtl: cdk.Duration.seconds(0),
  defaultTtl: cdk.Duration.seconds(0),
};

export const cache = (stage: Stage, duration: cdk.Duration, environments: Stage[] = []): Cache => {
  if (environments.includes(stage)) {
    return {
      minTtl: cdk.Duration.seconds(10), // so that 4xx/5xx errors are not cached for too long
      maxTtl: duration,
      defaultTtl: duration,
    };
  } else {
    return disableCache;
  }
};

export type CloudFrontStage = 'dev' | 'stage' | 'hfe' | 'ppd' | 'prod';

export interface CloudFrontBaseStackProps extends TPCIStackProps {
  stage: CloudFrontStage;
}

export class CloudFrontBaseStack extends TPCIStack {
  public readonly stage: CloudFrontStage;

  constructor(scope: cdk.Construct, id: string, props: CloudFrontBaseStackProps) {
    super(scope, id, {
      ...props,
      env: {
        ...props.env,
        region: 'us-east-1',
      },
    });

    if (this.region !== 'us-east-1') {
      throw new Error('Lambda@Edge MUST be deployed to us-east-1');
    }

    if (['stage-eu', 'ppd-eu', 'prod-eu'].includes(this.stage)) {
      throw new Error(`The EU stages do not require their own Cloudfront`);
    }

    this.stage = props.stage;
  }
}

type EdgeLambdas = {
  [key in cf.LambdaEdgeEventType]?: lambda.IVersion;
};

export function lambdaAssociations(lambdas: EdgeLambdas): cf.LambdaFunctionAssociation[] {
  const res: cf.LambdaFunctionAssociation[] = [];
  Object.entries(lambdas).forEach(
    ([eventType, lambdaFunction]: [string, lambda.IVersion | undefined]) => {
      if (lambdaFunction) {
        res.push({
          eventType: eventType as cf.LambdaEdgeEventType,
          lambdaFunction,
        });
      }
    },
  );
  return res;
}

/**
 * This helper is to make it easier to define multiple paths with the same behavior options.
 */
export function behaviors(
  paths: string[],
  opts: cf.BehaviorOptions,
): { [path: string]: cf.BehaviorOptions } {
  return Object.assign(
    {},
    ...paths.map((path) => ({
      [path]: opts,
    })),
  );
}
