import * as cdk from '@aws-cdk/core';
import * as iam from '@aws-cdk/aws-iam';
import {
  AwsCustomResource,
  AwsSdkCall,
  PhysicalResourceId,
  AwsCustomResourcePolicy,
} from '@aws-cdk/custom-resources';

export interface GetApiKeyProps {
  apiKeyName: string;
  region?: string;
}

export class GetApiKey extends cdk.Construct {
  public readonly value: string;

  constructor(scope: cdk.Construct, id: string, props: GetApiKeyProps) {
    super(scope, id);

    const region = props.region || cdk.Aws.REGION;

    const getApiKeySdkCall: AwsSdkCall = {
      service: 'APIGateway',
      action: 'getApiKeys',
      parameters: {
        nameQuery: props.apiKeyName,
        includeValues: true,
      },
      outputPath: 'items.0',
      physicalResourceId: PhysicalResourceId.of(Date.now().toString()), // Update physical id to always fetch the latest version
      region,
    };
    const statement = new iam.PolicyStatement({
      actions: ['apigateway:GET'],
      resources: [`arn:aws:apigateway:${region}::/apikeys`],
    });
    const getApiKey = new AwsCustomResource(this, 'GetApiKeys', {
      onCreate: getApiKeySdkCall,
      onUpdate: getApiKeySdkCall,
      policy: AwsCustomResourcePolicy.fromStatements([statement]),
    });

    this.value = getApiKey.getResponseField('items.0.value').toString();
  }
}
