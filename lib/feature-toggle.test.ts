import * as cdk from '@aws-cdk/core';
import * as ssm from '@aws-sdk/client-ssm';
import { TPCIStack } from './stack/tpci';
import { isFeatureOn } from './feature-toggle';

const mockSSMGetParameter = jest.fn();
jest.mock('@aws-sdk/client-ssm', () => {
  return {
    SSM: jest.fn(() => ({
      getParameter: mockSSMGetParameter,
    })),
  };
});

test('parameter found returns true', async () => {
  mockSSMGetParameter.mockResolvedValue({
    Parameter: {
      Value: 'true',
    },
  });
  expect(await isFeatureOn('found-off')).toBe(true);
});

test('parameter found returns false', async () => {
  mockSSMGetParameter.mockResolvedValue({
    Parameter: {
      Value: 'false',
    },
  });
  expect(await isFeatureOn('found-on')).toBe(false);
});

test('parameter not found returns false', async () => {
  mockSSMGetParameter.mockImplementation(async () => {
    throw new Error('ParameterNotFound: ParameterNotFound');
  });
  expect(await isFeatureOn('notfound')).toBe(false);
});
