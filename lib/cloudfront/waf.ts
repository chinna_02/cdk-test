import * as cdk from '@aws-cdk/core';
import * as wafv2 from '@aws-cdk/aws-wafv2';

import * as ips from '../ips';
import { BaseResource } from '../base-resource';
import { TPCIStack } from '../stack/tpci';
import {
  block,
  matchQueryArgs,
  textTransformations,
  matchBody,
  matchHeader,
  allow,
  matchUrl,
  externalIpWhitelistRule,
  internalIpWhitelistRule,
  impervaWhitelistRule,
  orderStatusRules,
  rules,
  healthRule,
  xHttpMethodRule,
} from '../waf';

export class CloudFrontWAF extends BaseResource {
  public readonly acl: wafv2.CfnWebACL;

  constructor(cdkScope: cdk.Construct, id: string) {
    super(cdkScope, id);

    const scope = 'CLOUDFRONT';

    const stack = cdk.Stack.of(this) as TPCIStack;

    this.acl = new wafv2.CfnWebACL(this, 'ACLv2', {
      name: stack.stackName,
      description: 'WAF for pokemoncenter.com that are used to check against all requests',
      scope,
      defaultAction: { block: {} },
      visibilityConfig: {
        metricName: stack.stackName,
        cloudWatchMetricsEnabled: true,
        sampledRequestsEnabled: true,
      },
      rules: rules(
        stack,

        healthRule,

        xHttpMethodRule,
        // order-status needs to be before other rules
        // the xxs rule (and possibly others) blocks legit order-status requests
        ...orderStatusRules(stack),

        {
          name: 'SQLi',
          ...block,
          statement: {
            orStatement: {
              statements: [
                {
                  sqliMatchStatement: {
                    ...matchQueryArgs,
                    ...textTransformations('URL_DECODE'),
                  },
                },
                {
                  sqliMatchStatement: {
                    ...matchBody,
                    ...textTransformations('HTML_ENTITY_DECODE', 'CMD_LINE'),
                  },
                },
                {
                  sqliMatchStatement: {
                    ...matchHeader('cookie'),
                    ...textTransformations('HTML_ENTITY_DECODE'),
                  },
                },
              ],
            },
          },
        },

        {
          name: 'CyberSecurityCloud',
          overrideAction: {
            none: {},
          },
          statement: {
            managedRuleGroupStatement: {
              vendorName: 'Cyber Security Cloud Inc.',
              name: 'CyberSecurityCloud-APIGatewayServerless-',
            },
          },
        },

        {
          name: 'xss',
          ...block,
          statement: {
            orStatement: {
              statements: [
                {
                  xssMatchStatement: {
                    ...matchQueryArgs,
                    ...textTransformations('URL_DECODE'),
                  },
                },
                {
                  xssMatchStatement: {
                    ...matchBody,
                    ...textTransformations('HTML_ENTITY_DECODE'),
                  },
                },
                {
                  xssMatchStatement: {
                    ...matchHeader('cookie'),
                    ...textTransformations('HTML_ENTITY_DECODE'),
                  },
                },
              ],
            },
          },
        },

        {
          name: 'url',
          ...allow,
          statement: {
            orStatement: {
              statements: [
                {
                  byteMatchStatement: {
                    ...matchUrl('/tpci-transaction-email-api/'),
                    positionalConstraint: 'STARTS_WITH',
                  },
                },
                {
                  byteMatchStatement: {
                    ...matchUrl('/tpci-fluent-webhook/'),
                    positionalConstraint: 'STARTS_WITH',
                  },
                },
              ],
            },
          },
        },

        // Do not rate limit internal IPs
        internalIpWhitelistRule(stack),

        // Imperva IPs - Imperva has it's own IP Allowlists controlling access
        impervaWhitelistRule(stack),

        // Rate limit /tpci-ecommweb-api/
        {
          name: 'ratelimit',
          ...block,
          statement: {
            rateBasedStatement: {
              aggregateKeyType: 'IP',
              limit: 100, // per 5 minutes. Minimum value is 100
              scopeDownStatement: {
                byteMatchStatement: {
                  ...matchUrl('/tpci-ecommweb-api/'),
                  positionalConstraint: 'STARTS_WITH',
                },
              },
            },
          },
        },

        // Rate limit external testers
        externalIpWhitelistRule(stack),
      ),
    });
  }
}
