import * as cdk from '@aws-cdk/core';
import * as lambda from '@aws-cdk/aws-lambda';
import * as iam from '@aws-cdk/aws-iam';
import { BaseResource } from '../base-resource';

import fs from 'fs';
import crypto from 'crypto';
import glob from 'glob';
import { MetricOptions, Metric } from '@aws-cdk/aws-cloudwatch';
import { Connections } from '@aws-cdk/aws-ec2';

type EdgeLambdaProps = Partial<lambda.FunctionProps>;

export class EdgeLambda extends BaseResource {
  public readonly lambda: lambda.Function;
  public readonly currentVersion: lambda.IVersion;

  constructor(cdkScope: cdk.Construct, id: string, props?: EdgeLambdaProps) {
    super(cdkScope, id);

    const codeDir = `./dist/lambda/${id}`;

    const func = new lambda.Function(this, 'Fn', {
      runtime: lambda.Runtime.NODEJS_14_X,
      handler: 'index.handler',
      code: lambda.Code.fromAsset(codeDir),
      timeout: cdk.Duration.seconds(1),
      role: new iam.Role(this, 'Role', {
        assumedBy: new iam.CompositePrincipal(
          new iam.ServicePrincipal('lambda.amazonaws.com'),
          new iam.ServicePrincipal('edgelambda.amazonaws.com'),
        ),
        managedPolicies: [
          iam.ManagedPolicy.fromAwsManagedPolicyName('service-role/AWSLambdaBasicExecutionRole'),
        ],
      }),
      ...props,
    });

    func.applyRemovalPolicy(cdk.RemovalPolicy.RETAIN);

    const hash = crypto.createHash('sha256');
    glob.sync(codeDir + '/**/*', { nodir: true }).forEach((file) => {
      hash.update(fs.readFileSync(file));
    });

    this.currentVersion = func.addVersion(':sha256:' + hash.digest('hex'));
    this.lambda = func;
  }
}
