import path from 'path';
import { readFileSync } from 'fs';
import * as YAML from 'yaml';

import * as cdk from '@aws-cdk/core';
import * as events from '@aws-cdk/aws-events';
import * as targets from '@aws-cdk/aws-events-targets';
import * as lambda from '@aws-cdk/aws-lambda';
import * as codebuild from '@aws-cdk/aws-codebuild';
import * as s3 from '@aws-cdk/aws-s3';
import * as s3deploy from '@aws-cdk/aws-s3-deployment';

import { FeatureToggle } from '../feature-toggle';
import { BaseResource } from '../base-resource';

// TODO docs

interface MaintenanceToggleProps {
  uiBucket: s3.IBucket;
}

export class MaintenanceToggle extends BaseResource {
  public readonly mode: boolean;

  constructor(scope: cdk.Construct, id: string, props: MaintenanceToggleProps) {
    super(scope, id);
    const maintenanceToggle = new FeatureToggle(this, 'maintenance');
    this.mode = maintenanceToggle.mode;

    const rule = new events.Rule(this, 'rule', {
      description: 'Trigger CodeBuild when the maintenance feature toggle changes',
      eventPattern: {
        source: ['aws.ssm'],
        detailType: ['Parameter Store Change'],
        resources: [maintenanceToggle.parameter.parameterArn],
        detail: {
          name: [maintenanceToggle.parameter.parameterName],
          type: ['String'],
          operation: [
            'Update', // line-break
            'LabelParameterVersion',
          ],
        },
      },
    });

    const buildspec = YAML.parse(readFileSync('buildspec.yml', 'utf-8'));
    buildspec['phases']['build']['commands'] = [
      `ci/deploy.sh tpci-aws-cdk-cloudfront-${this.stage}`,
    ];
    delete buildspec['artifacts'];

    const project = codebuild.Project.fromProjectName(
      this,
      'project',
      `tpci-aws-cdk-build-${this.stage}`,
    );

    rule.addTarget(
      new targets.CodeBuildProject(project, {
        event: events.RuleTargetInput.fromObject({
          buildspecOverride: YAML.stringify(buildspec),
          artifactsOverride: {
            type: 'NO_ARTIFACTS',
          },
        }),
      }),
    );

    // TODO Maintenance Page HTML
    new s3deploy.BucketDeployment(this, 'page', {
      sources: [s3deploy.Source.asset(path.join(__dirname, 'maintenance/'))],
      destinationBucket: props.uiBucket,
      destinationKeyPrefix: 'maintenance/',
    });
  }
}
