import * as cdk from '@aws-cdk/core';
import * as cf from '@aws-cdk/aws-cloudfront';
import * as s3 from '@aws-cdk/aws-s3';
import * as transfer from '@aws-cdk/aws-transfer';
import * as iam from '@aws-cdk/aws-iam';
import * as route53 from '@aws-cdk/aws-route53';
import { Stage } from '../stack/tpci';

interface ProductImagesProps {
  stage: Stage;
  domainName: string;
  oai: cf.IOriginAccessIdentity;
}

export class ProductImages extends cdk.Construct implements cf.S3OriginConfig {
  public readonly s3BucketSource: s3.IBucket;
  public readonly originAccessIdentity: cf.IOriginAccessIdentity;

  public constructor(scope: cdk.Construct, id: string, props: ProductImagesProps) {
    super(scope, id);

    this.originAccessIdentity = props.oai;

    const bucket = new s3.Bucket(this, 'Bucket', {
      bucketName: `tpci-product-images-${props.stage}`,
      cors: [
        {
          id: '1',
          allowedHeaders: ['*'],
          allowedMethods: [s3.HttpMethods.GET],
          allowedOrigins:
            props.stage === 'prod'
              ? [
                  'https://prod.ui.pokemoncenter.com',
                  'https://prod.pokemoncenter.com',
                  'https://www.pokemoncenter.com',
                  'https://beta.pokemoncenter.com',
                  'https://pokemoncenter.com',
                  'https://prod.tpci1.bloomreach.cloud',
                  'https://prod.cms.pokemoncenter.com',
                ]
              : ['*'],
          exposedHeaders: ['Date'],
          maxAge: 86400,
        },
      ],
    });
    bucket.addToResourcePolicy(
      new iam.PolicyStatement({
        effect: iam.Effect.ALLOW,
        principals: [props.oai.grantPrincipal],
        actions: ['s3:GetObject'],
        resources: [bucket.arnForObjects('*')],
      }),
    );

    bucket.grantRead(
      {
        grantPrincipal: props.oai.grantPrincipal,
      },
      /* Filter out anything that doesn't end with .jpg in order to prevent access to files such as
       * Thumbs.db, .DS_store, and anything else that accidentally ends up in the bucket.
       */
      '*.jpg',
    );

    this.s3BucketSource = bucket;

    const sftp = new transfer.CfnServer(this, 'Sftp', {
      loggingRole: new iam.Role(this, 'ProductImagesSftpCloudWatchRole', {
        roleName: `${cdk.Aws.STACK_NAME}-product-images-sftp-cloudwatch`,
        assumedBy: new iam.ServicePrincipal('transfer.amazonaws.com'),
        inlinePolicies: {
          CloudWatch: new iam.PolicyDocument({
            statements: [
              new iam.PolicyStatement({
                actions: [
                  'logs:CreateLogGroup',
                  'logs:CreateLogStream',
                  'logs:DescribeLogStreams',
                  'logs:PutLogEvents',
                ],
                resources: ['*'],
              }),
            ],
          }),
        },
      }).roleArn,
    });

    const serverId = sftp.getAtt('ServerId').toString();

    new route53.CnameRecord(this, 'ProductImagesSftpRecord', {
      recordName: 'product-images',
      domainName: `${serverId}.server.transfer.${cdk.Aws.REGION}.amazonaws.com`,
      zone: route53.HostedZone.fromLookup(this, `Zone`, {
        domainName: props.domainName,
      }),
    });

    const adminRole = new iam.Role(this, 'AdminRole', {
      roleName: `${cdk.Aws.STACK_NAME}-product-images-admin`,
      assumedBy: new iam.ServicePrincipal('transfer.amazonaws.com'),
      inlinePolicies: {
        admin: new iam.PolicyDocument({
          statements: [
            new iam.PolicyStatement({
              actions: ['s3:ListBucket', 's3:GetBucketLocation'],
              resources: [bucket.bucketArn],
            }),
            new iam.PolicyStatement({
              actions: ['s3:*Object*'],
              resources: [bucket.arnForObjects('*')],
            }),
          ],
        }),
      },
    });

    [
      {
        name: 'Alphonse Giltus',
        userName: 'agiltus',
        email: 'a.giltus@pokemon.com',
        publicKey:
          'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC9PwzTm6B/hCwOX5GckLigGEkiANMKSqYw+8ICDqYN2CvOtUkGA2LupXFc0b9v9mFsnMnO8HGNsMBEsfjTDShkptQ0xrxqj5KOIJD28vnw/Bq1x3TybqBHxmA7crM2TwT71EAg0UoL+yev2MH6/mWO4FXw6Pq2CNdwYEDWzD/8sZPudpvsg7ImF7o4Ky1ZbVCGX5ETtnjn8pz7IgZ1gA1Ho8eq3b4IktPFL2c7GnTe7LRvPhBBucVrXjv3liuOcTLqStC4uXqsH/uZZA7ujAEA/yM/PCgLu6qq04mHlLH2wAPKfpTRX1IYVC2fFGvIDgZf0CvR2X7BEZa/FpGFB/h/',
      },
      {
        name: 'Mike Citro',
        userName: 'mcitro',
        email: 'mike.citro@slalom.com',
        publicKey:
          'ssh-rsa AAAAB3NzaC1yc2EAAAABJQAAAQEAi1dDo+TdZFGW3GmixKvTL9A6+2nMl+vGQzeeWuVqdwWvRQVmbwppyDtlg/ggOdQR44wovOOKcXXxVBPXMp7GTHy+NeBmnAGCQfd3ssgE0oZf+uLQsFFW4Wsl/OWc/34oKTYPLdWjLL501utY2Snuvm4vnhfMZGJ19lieCnCOvVZymmUvot1Q+IasP/Ibt2oq2my2OxPAQhwKllx5ujssdLQXeLtmJ7NuJnbjzrawegmb2lTB6pViCUr30/Uj1JOWG3WYRrrM4BDGL+kBsws+4RZ36d/nBI3cibv/LX77VBjzduhH0zf05Wona76DUb6aNZqQZqbF7hGvLfGHLiNIiQ==',
      },
    ].forEach(({ name, userName, email, publicKey }) => {
      new transfer.CfnUser(this, name.replace(' ', ''), {
        userName,
        homeDirectory: `/tpci-product-images-${props.stage}`,
        serverId: serverId,
        role: adminRole.roleArn,
        sshPublicKeys: [publicKey],
        tags: [
          {
            key: 'Name',
            value: name,
          },
          {
            key: 'Email',
            value: email,
          },
        ],
      });
    });
  }
}
