import { toTitleCase } from './utils';

test('toTitleCase', () => {
  expect(toTitleCase('stage')).toBe('Stage');
  expect(toTitleCase('stage-eu')).toBe('StageEu');
});
