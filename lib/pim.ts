import { Stage } from './stack/tpci';
export const getQueuePrincipal = (stage?: Stage): string => {
  switch (stage) {
    case 'dev':
    case 'stage':
      return 'arn:aws:iam::406697379181:root';
    case 'ppd':
    case 'prod':
      return 'arn:aws:iam::727927018030:root';
    default:
      return '';
  }
};
