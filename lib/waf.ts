import * as cdk from '@aws-cdk/core';
import * as wafv2 from '@aws-cdk/aws-wafv2';

import * as ips from './ips';
import { TPCIStack } from './stack/tpci';

export const subnetSuffix = (ip: string): string | undefined => /\/(\d\d?)$/.exec(ip)?.[1];

export const addSubnetSuffix = (ip: string): string => (subnetSuffix(ip) ? ip : `${ip}/32`);

export type TransformationType =
  | 'NONE'
  | 'CMD_LINE'
  | 'COMPRESS_WHITE_SPACE'
  | 'HTML_ENTITY_DECODE'
  | 'LOWERCASE'
  | 'URL_DECODE';

type TextTransformation = { type: TransformationType; priority: number };

type TextTransformations = { textTransformations: TextTransformation[] };

export const textTransformations = (...types: TransformationType[]): TextTransformations => ({
  textTransformations: types.map((type, idx) => ({ type, priority: idx + 1 })),
});

export const block = {
  action: { block: {} },
};

export const allow = {
  action: { allow: {} },
};

export const matchBody = {
  fieldToMatch: {
    body: {},
  },
};

export const matchQueryArgs = {
  fieldToMatch: {
    allQueryArguments: {},
  },
};

export const matchQueryArg = (
  Name: string,
): { fieldToMatch: { singleQueryArgument: { Name: string } } } => ({
  fieldToMatch: {
    singleQueryArgument: { Name },
  },
});

export const matchHeader = (
  Name: string,
): { fieldToMatch: { singleHeader: { Name: string } } } => ({
  fieldToMatch: {
    singleHeader: { Name },
  },
});

export const matchUrl = (
  url: string,
): {
  fieldToMatch: { uriPath: Record<string, never> };
  searchString: string;
} & TextTransformations => ({
  fieldToMatch: { uriPath: {} },
  searchString: url,
  ...textTransformations('NONE'),
});

export const ipSet = (stack: TPCIStack, name: string, ...ips: string[][]): string => {
  const ipSet = new wafv2.CfnIPSet(stack, name, {
    name: stack.resourceName(`${name}-1`),
    // TODO The '-1' suffic is to resolve the name conflict caused by the moving of
    // IPSet to a different file.
    // The suffix can be removed after the new prod release.
    scope: 'CLOUDFRONT',
    ipAddressVersion: 'IPV4',
    addresses: ips.flat().map(addSubnetSuffix),
  });
  return ipSet.attrArn;
};

export const ip6Set = (stack: TPCIStack, name: string, ...ips: string[][]): string => {
  const ipSet = new wafv2.CfnIPSet(stack, name, {
    name: stack.resourceName(`${name}-1`),
    // TODO The '-1' suffic is to resolve the name conflict caused by the moving of
    // IPSet to a different file.
    // The suffix can be removed after the new prod release.
    scope: 'CLOUDFRONT',
    ipAddressVersion: 'IPV6',
    addresses: ips.flat().map(addSubnetSuffix),
  });
  return ipSet.attrArn;
};

export const matchIPStatement = (arn: string): wafv2.CfnWebACL.StatementProperty => ({
  ipSetReferenceStatement: {
    arn,
  },
});
export const matchIPStatementXff = (arn: string): wafv2.CfnWebACL.StatementProperty => ({
  ipSetReferenceStatement: {
    arn,
    ipSetForwardedIpConfig: {
      headerName: 'X-Forwarded-For',
      position: 'FIRST',
      fallbackBehavior: 'MATCH',
    },
  },
});

type Rule = Omit<
  Omit<wafv2.CfnWebACL.RuleProperty, 'visibilityConfig'> & Partial<wafv2.CfnWebACL.RuleProperty>,
  'priority'
>;

export const rules = (stack: TPCIStack, ...args: Rule[]): wafv2.CfnWebACL.RuleProperty[] => {
  return args.map((props, idx) => ({
    ...props,
    priority: idx + 1,
    visibilityConfig: {
      metricName: stack.resourceName(props.name),
      cloudWatchMetricsEnabled: true,
      sampledRequestsEnabled: true,
    },
  }));
};

export const vpcIPs = (stack: TPCIStack): string[] =>
  stack.isProdAccount ? ips.prodVpc : ips.devVpc;

export const orderStatusRules = (stack: TPCIStack): Rule[] => {
  const name = 'order-status';
  const ipSetArn = ipSet(
    stack,
    name,
    ips.xpo,
    ips.narvar,
    ips.flexe,
    ips.pokemonVPN,
    vpcIPs(stack),
    stack.stage === 'stage' ? [...ips.xpoTesting] : [],
    stack.stage === 'dev' || stack.stage === 'stage' ? [...ips.ingrammicroTest] : [],
    stack.stage === 'prod' ? [...ips.ingrammicroProd] : [],
  );
  const rules: Rule[] = [
    {
      name,
      ...allow,
      statement: {
        andStatement: {
          statements: [
            {
              byteMatchStatement: {
                ...matchUrl('/tpci-order-status/'),
                positionalConstraint: 'STARTS_WITH',
              },
            },
            matchIPStatement(ipSetArn),
          ],
        },
      },
    },
    // If none of the IPs match for order-status but the request is still to /tpci-order-status/,
    // then block the request
    {
      name: 'order-status-wrong-ip',
      ...block,
      statement: {
        byteMatchStatement: {
          ...matchUrl('/tpci-order-status/'),
          positionalConstraint: 'STARTS_WITH',
        },
      },
    },
  ];
  // This statement is to support test cases for matching IP address using XFF header
  // in dev account
  if (stack.isDevAccount) {
    rules.splice(1, 0, {
      name: 'order-status-xff',
      ...allow,
      statement: {
        andStatement: {
          statements: [
            {
              byteMatchStatement: {
                ...matchUrl('/tpci-order-status/'),
                positionalConstraint: 'STARTS_WITH',
              },
            },
            matchIPStatementXff(ipSetArn),
          ],
        },
      },
    });
  }
  return rules;
};

// Add an OR statement in dev account to test IP Rules using XFF header
const ipWhitelistStatement = (stack: TPCIStack, arn: string) => {
  if (stack.isDevAccount) {
    return {
      orStatement: {
        statements: [matchIPStatement(arn), matchIPStatementXff(arn)],
      },
    };
  }
  return matchIPStatement(arn);
};

const ip4And6WhitelistStatement = (stack: TPCIStack, arn4: string, arn6: string) => {
  if (stack.isDevAccount) {
    return {
      orStatement: {
        statements: [
          matchIPStatement(arn4),
          matchIPStatementXff(arn4),
          matchIPStatement(arn6),
          matchIPStatementXff(arn6),
        ],
      },
    };
  }
  return {
    orStatement: {
      statements: [matchIPStatement(arn4), matchIPStatement(arn6)],
    },
  };
};

export const internalIpWhitelistRule = (stack: TPCIStack): Rule => {
  const name = 'internal-ip-whitelist';
  const ip4Arn = ipSet(
    stack,
    `${name}-ipv4`,
    vpcIPs(stack),
    ips.pokemonVPN,
    ips.elasticpath,
    ips.bloomreach,
    ips.mabl,
    ips.sitespect,
    stack.stage === 'stage'
      ? [
          ...ips.bitbucketPipeline, // for browser tests
          ...ips.netsparker,
          ...ips.siteImprove,
        ]
      : [],
    stack.stage === 'dev' || stack.stage === 'stage' ? ips.ingrammicroTest : [],
    stack.stage === 'prod' ? ips.ingrammicroProd : [],
  );

  return {
    name,
    ...allow,
    statement: ipWhitelistStatement(stack, ip4Arn),
  };
};

export const impervaWhitelistRule = (stack: TPCIStack): Rule => {
  const name = 'imperva-ip-whitelist';
  const ip4Arn = ipSet(stack, `${name}-ipv4`, ips.impervaIP4);
  const ip6Arn = ip6Set(stack, `${name}-ipv6`, ips.impervaIP6);

  return {
    name,
    ...allow,
    statement: ip4And6WhitelistStatement(stack, ip4Arn, ip6Arn),
  };
};

export const externalIpWhitelistRule = (stack: TPCIStack): Rule => {
  const name = 'external-ip-whitelist';
  const ipArn = ipSet(stack, name, ips.externalTesting);
  return {
    name,
    ...allow,
    statement: ipWhitelistStatement(stack, ipArn),
  };
};

export const healthRule: Rule = {
  name: 'health',
  ...allow,
  statement: {
    byteMatchStatement: {
      ...matchUrl('/health'),
      positionalConstraint: 'ENDS_WITH',
    },
  },
};
export const xHttpMethodRule: Rule = {
  name: 'block-x-http-method-override',
  ...block,
  statement: {
    sizeConstraintStatement: {
      ...matchHeader('x-http-method-override'),
      size: 0,
      ...textTransformations('NONE'),
      comparisonOperator: 'GT',
    },
  },
};
