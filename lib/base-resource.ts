import * as cdk from '@aws-cdk/core';
import { TPCIStack } from './stack/tpci';

export class BaseResource extends cdk.Resource {
  public readonly stackName: string;
  public readonly stage?: string;
  public readonly stageTitle?: string;

  constructor(scope: cdk.Construct, id: string) {
    super(scope, id);
    this.stackName = cdk.Stack.of(this).stackName;
    this.stage = (cdk.Stack.of(this) as TPCIStack).stage;
    this.stageTitle = (cdk.Stack.of(this) as TPCIStack).stageTitle;
  }
}
