import * as codepipeline from '@aws-cdk/aws-codepipeline';
import * as targets from '@aws-cdk/aws-events-targets';
import * as iam from '@aws-cdk/aws-iam';
import { Construct } from '@aws-cdk/core';
import { Action } from '@aws-cdk/aws-codepipeline-actions';

function sourceArtifactBounds(): codepipeline.ActionArtifactBounds {
  return {
    minInputs: 0,
    maxInputs: 0,
    minOutputs: 1,
    maxOutputs: 1,
  };
}

export enum OutputArtifactFormat {
  CODEBUILD_CLONE_REF = 'CODEBUILD_CLONE_REF',
  CODE_ZIP = 'CODE_ZIP',
}

/**
 * Construction properties of the {@link BitBucketSourceAction BitBucket source CodePipeline Action}.
 */
export interface BitBucketSourceActionProps extends codepipeline.CommonAwsActionProps {
  /** The connection ARN that is configured and authenticated for the BitBucket source provider. **/
  readonly connectionArn: string;

  /**
   *
   */
  readonly output: codepipeline.Artifact;

  /**
   * @default 'master'
   */
  readonly branch?: string;

  /**
   * The BitBucket repository. Example: "someowner/somerepo"
   */
  readonly repository: string;

  /**
   * Specifies the output artifact format. If unspecified, the default is CODE_ZIP.
   *
   * Important: The CODEBUILD_CLONE_REF option can only be used by CodeBuild downstream actions.
   **/
  readonly outputArtifactFormat?: OutputArtifactFormat;
}

/**
 * CodePipeline Source that is provided by an AWS BitBucket repository.
 */
export class BitBucketSourceAction extends Action {
  private readonly branch: string;
  private readonly props: BitBucketSourceActionProps;

  constructor(props: BitBucketSourceActionProps) {
    const branch = props.branch || 'master';

    super({
      ...props,
      category: codepipeline.ActionCategory.SOURCE,
      provider: 'CodeStarSourceConnection',
      artifactBounds: sourceArtifactBounds(),
      outputs: [props.output],
    });

    this.branch = branch;
    this.props = props;
  }

  protected bound(
    _scope: Construct,
    _stage: codepipeline.IStage,
    options: codepipeline.ActionBindOptions,
  ): codepipeline.ActionConfig {
    // the Action will write the contents of the Git repository to the Bucket,
    // so its Role needs write permissions to the Pipeline Bucket
    options.bucket.grantReadWrite(options.role);

    options.role.addToPolicy(
      new iam.PolicyStatement({
        resources: [this.props.connectionArn],
        actions: ['codestar-connections:UseConnection'],
      }),
    );

    return {
      configuration: {
        ConnectionArn: this.props.connectionArn,
        FullRepositoryId: this.props.repository,
        BranchName: this.branch,
        OutputArtifactFormat: this.props.outputArtifactFormat,
      },
    };
  }
}
