import * as cdk from '@aws-cdk/core';
import * as ssm from '@aws-cdk/aws-ssm';
import * as aws from '@aws-sdk/client-ssm';

import { BaseResource } from './base-resource';
import { TPCIStack } from './stack/tpci';

interface FeatureToggleProps {
  parameterName?: string;
  description?: string;
}

export class FeatureToggle extends BaseResource {
  public readonly mode: boolean;
  public readonly parameterName: string;
  public readonly parameter: ssm.StringParameter;

  constructor(scope: cdk.Construct, id: string, props?: FeatureToggleProps) {
    super(scope, id);
    this.parameterName = props?.parameterName || `/${this.stage}/feature-toggle/tpci-aws-cdk/${id}`;

    this.parameter = new ssm.StringParameter(this, id, {
      parameterName: this.parameterName,
      stringValue: ssm.StringParameter.valueForStringParameter(this, this.parameterName),
      description: props?.description,
      allowedPattern: '^(true|false)$',
    });
  }
}

export async function isFeatureOn(
  parameterName: string,
  config: aws.SSMClientConfig = {},
): Promise<boolean> {
  try {
    const awsSSM = new aws.SSM({ apiVersion: '2014-11-06', ...config });
    const resp = await awsSSM.getParameter({ Name: parameterName });

    if (resp.Parameter?.Value === 'true') {
      console.log(`Feature ON for ${parameterName}`);
      return true;
    }
  } catch (e) {
    console.error(`Can not retrieve feature toggle ${parameterName}`);
    console.error(`Error: ${e}`);
  }

  console.log(`Feature OFF for ${parameterName}`);
  return false;
}
