export function toTitleCase(str: string): string {
  let words = str.split('-');
  words = words.map((s) => s.charAt(0).toUpperCase() + s.slice(1));
  return words.join('');
}
