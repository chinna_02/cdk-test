import { subnetSuffix } from './waf';

test('matches subnets', () => {
  expect(subnetSuffix('8.8.8.8')).toBeFalsy();
  expect(subnetSuffix('8.8.8.8/32')).toBe('32');
  expect(subnetSuffix('10.0.0.0/8')).toBe('8');
});
