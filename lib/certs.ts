import { Stage } from './stack/tpci';

export const certificateArn = (stage: Stage): string => {
  switch (stage) {
    case 'dev':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/6aecdba4-a727-4f1c-b557-96d42a817551';
    case 'stage':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/6aecdba4-a727-4f1c-b557-96d42a817551';
    case 'ppd':
      return 'arn:aws:acm:us-east-1:322789178911:certificate/d301a2e4-53db-43f9-a9e7-b269280a28f8';
    case 'prod':
      return 'arn:aws:acm:us-east-1:322789178911:certificate/0a63f7ab-87c9-4708-9bf2-81159e26074a';
    case 'hfe':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/77d3a71e-414d-4ba8-a742-9f55d649a31e';
    default:
      throw new Error(`${stage} does not have a certificate`);
  }
};

export const servicesCertificateArn = (stage: Stage): string => {
  switch (stage) {
    case 'dev':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/e3e56e8f-c1a8-4a37-8387-b44881bbb30a';
    case 'stage':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/e3e56e8f-c1a8-4a37-8387-b44881bbb30a';
    case 'hfe':
      return 'arn:aws:acm:us-east-1:053655145506:certificate/e24e8fc7-c470-4309-90f7-efbe3bacffd3';
    case 'ppd':
      return 'arn:aws:acm:us-east-1:322789178911:certificate/7acbe48c-9ee7-4c9e-844c-c9ea7b9c15e0';
    case 'prod':
      return 'arn:aws:acm:us-east-1:322789178911:certificate/e8b7d6b8-d632-4ff6-805e-19a799edfac4';
    default:
      throw new Error(`${stage} does not have a certificate`);
  }
};
