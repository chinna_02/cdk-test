# tpci-aws-cdk

This repository contains AWS Cloud Development Kit (cdk) stacks for API Custom Domains, CloudFront, Simple Email Service (SES) Bounce/Complaint Handling, TPCI VPC, and Web Application Firewall (WAF) rules.

version: `tpci-aws-cdk:2.3.13`

[TOC]

# Usage

## Commands

- `npm run build` compile typescript to js
- `npm run watch` watch for changes and compile
- `npm run test` perform the jest unit tests on both lambda functions and cdk resources
- `npx cdk deploy` deploy this stack to your default AWS account/region
- `npx cdk diff` compare deployed stack with current state
- `npx cdk synth` synthesized CloudFormation
- `npx cdk synth --no-staging tpci-aws-cdk-cloudfront-dev > template.yaml` generate the template.yaml to use with SAM
- `sam local invoke --event events/event.json` run the lambda function locally

## Bootstrap cdk

If deploying in a new region or account, you need to bootstrap cdk:

```bash
cdk bootstrap aws://<account>/us-west-2
```

## Working with Stacks

### List

CDK apps are broken up into stacks. To get a list of stacks, run:

```
npx cdk list
```

To get a list of stacks for a different AWS profile than your default
such as one for the **prod** AWS account run:

```
npx cdk list --profile prod
```

### Deploy

Note: Automated deployments for this repository are configured in tpci-aws-non-prod and tpci-aws-prod repositories. Currently the `tpci-aws-cdk-pcenter-vpc` requires manual deployment because it alters security and requires review.

To deploy a stack, run:

```
npx cdk deploy tpci-aws-cdk-pcenter-vpc
```

To deploy a stack to a different AWS profile than your default
such as one for the **prod** AWS account run:

```
npx cdk deploy --profile prod tpci-aws-cdk-pcenter-vpc
```

# [Stacks](lib/stack/)

## [TPCIStack](lib/stack/tpci.ts)

CloudFormation stacks in this repo are based on the TPCIStack base class,
which ensures that stack names, resources, and CloudFormation follow TPCI's
naming conventions of:

    tpci-aws-cdk-<name>-<stage>

The following are the CloudFormation stacks in this repository:

## [API Custom Domains (tpci-aws-cdk-api-domain-\*)](lib/stack/api-domain.ts)

Creates the API Gateway Custom Domains with DNS for `customdomain.*.api.pokemoncenter.com`
that the APIs use for the base path mapping. This stack must be deployed before the APIs.

To enable DataDome's bot protection for the APIs,
they need to be served behind CloudFront.

The base path mapping is necessary because CloudFront is limited in how it can rewrite URLs.
CloudFront can only append request URLs to an origin.

For example, if not using custom-domain and base path mapping and sending directly to the API Gateway:

```
dev.api.pokemoncenter.com/tpci-ecommweb-api/
```

proxied to

```
2pgyrh4ao1.execute-api.us-west-2.amazonaws.com/dev/tpci-ecommweb-api/
```

With the custom domain and base path mapping, it is sent to:

```
customdomain.dev.api.pokemoncenter.com/tpci-ecommweb-api/
```

which mapped in each of the template.yml for the repos:

```
2pgyrh4ao1.execute-api.us-west-2.amazonaws.com/dev/
```

## [CloudFront (tpci-aws-cdk-cloudfront-\*)](lib/stack/cloudfront.ts)

This stack is where the CloudFront Distribution for the site and related resources are managed.

### [Maintenance Page](lib/cloudfront/maintenance.ts)

A feature toggle exists to take down the entire site(all regions) for scheduled maintenance.

This is done by injecting a route to the CloudFront distribution after the OMS
lambda routes (so that orders can still be processed) but before the website and
API so that customers can't browse the site and new orders can't be submitted.
Instead of service site content, a single HTML page is service instead,
that pages live in [lib/cloudfront/maintenance/index.html](lib/cloudfront/maintenance/index.html).

The feature toggle is controlled by AWS System Manager Parameter
`<STAGE>/feature-toggle/tpci-aws-cdk/maintenance` in **us-east-1**.

In order for changes to take effect, the tpci-aws-cdk-cloudfront-<STAGE> stack
needs to be redeployed using CDK.

#### [Canada Maintenance Page](lib/stack/cloudfront.ts)
A feature toggle exists to take down exclusively the Canada UI frontend for scheduled maintenance.

This is done by injecting a route to the CloudFront distribution which redirects all `<domain>/en-ca/` pathed
traffic to a Lambda@Edge which redirects to a fixed `/maintenance/index.html` path with a temp redirect,
HTTP Status Code 307. This means that customers can't browse the site and new orders can't be submitted for
the Canada Store front only. US store front remains functional.
Instead of service site content, a single HTML page is service instead,
that pages live in [lib/cloudfront/maintenance/index.html](lib/cloudfront/maintenance/index.html).

The feature toggle is controlled by AWS System Manager Parameter
`<STAGE>/feature-toggle/tpci-aws-cdk/ca-maintenance` in **us-east-1**.

In order for changes to take effect, the tpci-aws-cdk-cloudfront-<STAGE> stack
needs to be redeployed using CDK.

### [Lambda@Edge](lambda/)

![](https://docs.aws.amazon.com/lambda/latest/dg/images/cloudfront-events-that-trigger-lambda-functions.png)

The following are Lambda@Edge functions that run in CloudFront.
Because of Lambda@Edge, the CloudFront stack **MUST** be deployed to **us-east-1**.

The Lambda functions are written in TypeScript. Be sure to run `npm run build`
before `cdk` or else CloudFormation won't be able to find the Lambda code.

#### [viewerRequest](lambda/viewerRequest/index.ts)

The viewerRequest runs on the UI, Bloomreach, and `/tpci-ecommweb-api/` endpoints.

This lambda is responsible for removing redirecting from pokemoncenter.com to
www.pokemoncenter.com and bot protection using DataDome.

DataDome is a 3rd party Bot Protection tool. Because we need to be able to run
automated browser testings in dev, stage, and ppd,
DataDome is only enabled for the prod deployment stage.

CloudFront is configured using [this guide](https://docs.datadome.co/docs/cloudfront-integration-setup).
As per the guide, the code for the lambda is downloaded and added to the
repo with the file modified to include the license key.

#### [UI Origin Request](lambda/uiOriginRequest/index.ts)

This lambda function is used for redirected users from old BlueSphere URLs to the equivalent on the new site.

More details [here](https://wiki.pokemon.tools/display/PC/Redirects)

CloudFront caches responses from origin-request lambda functions.
Doing this reduces the number of times the function needs to execute
which may reduce cost and the number of users impacted by a Lambda _cold start_.

One drawback could be that this increases the time before users can
see changes to redirects, but this will probably not happen often.

The request url is checked against [urls.json](lambda/uiOriginRequest/redirect/urls.json)
that maps urls from the old BlueSphere site to new site.
A script to generate this file can be found [here](lambda/uiOriginRequest/redirect/generate/).

#### [API Origin Request](lambda/uiOriginRequest/index.ts)

This lambda function is used for stripping the `/tpci-*` prefix from URLs that go the various API Gateway services,
which removes the need for API Gateway Custom Domains.

This lambda also changes the Origin Request to go the EU region based on the `X-Store-Scope: pokemon-uk` header.

#### [Origin Response](lambda/originResponse/index.ts)

This lambda function is used for injecting HTTP headers into responses.

## [SES Bounce (tpci-aws-cdk-ses-bounce)](lib/stack/bounce.ts)

This stack is for resources related to handling and managing
SES bounces and/or complaints as documented [here](https://wiki.pokemon.tools/display/PC/Transaction+Emails#TransactionEmails-BouncedEmails).

There is only one instance of this stack per account; thus, there is no `stage` suffix on the stack name.

It creates an SNS topic named `tpci-aws-cdk-ses-bounce` with a subscribed queue of the same name.
Once per day, a lambda retrieves all the messages from the queue and generates a report.
The report is sent to the SNS topic `tpci-aws-cdk-ses-bounce-daily-alerts`.
To subscribe to the daily alerts topic, add your email to the `recipients` parameters for the stack.

## [pcenter VPC (tpci-aws-cdk-pcenter-vpc)](lib/stack/vpc.ts)

Provides CloudFormation output of the pcenter VPC resources that are needed for lambdas, CodeBuild, and other AWS resources that are configured to run inside the pcenter VPC.

There is only one instance of this stack per account; thus, there is no `stage` suffix on the stack name.

This stack can affect security and requires review upon deployment. Therefor it is currently manually deployed.

## [WAFS (tpci-aws-cdk-wafs-\*)](lib/stack/wafs.ts)

Configures Web Application Firewall (WAF) rules for an environment.

## [PIM image sync (tpci-aws-cdk-pim-image-sync)](lib/stack/pim-image-sync.ts)

This stack creates a SNS topic which triggers a Lambda function based on the messages received from EP batch job. The Lambda function creates a S3 batch job and moves the images from source bucket to destination bucket. ie
tpci-product-images-dev --> tpci-product-images-stage (in non-prod AWS account)
tpci-product-images-ppd --> tpci-product-images-prod (in prod AWS account)
SNS topic expects the EP messages in the below JSON format.
`{"paths":["path/to/image1", "path/to/image2", "path/to/image3",...]}`
