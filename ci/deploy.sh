#!/bin/bash -x

STACK=$1
npx cdk diff "$STACK"
exec npx cdk deploy --require-approval "never" "$STACK"
