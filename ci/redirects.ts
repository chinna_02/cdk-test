/**
 * Test that all redirects work and point to the correct page
 */
import urls from '../lambda/uiOriginRequest/redirect/urls.json';
import parse from 'csv-parse';
import fs from 'fs';
import fetch from 'node-fetch';

const concurrency = 20;

const checked: { [key: string]: boolean } = {};

async function check(oldUrl: string, newUrl: string) {
  if (checked[oldUrl]) {
    return;
  }

  const stage = process.env.STAGE;
  const domain =
    stage === 'prod' || !stage ? `www.pokemoncenter.com` : `${stage}.ui.pokemoncenter.com`;
  const res = await fetch(`https://${domain}${oldUrl}`, { redirect: 'manual' }).catch((err) => {
    throw new Error(`Network Error: ${err.message}`);
  });
  // console.log({ oldUrl, newUrl, status: res.status });
  try {
    if (res.status !== 302) {
      throw new Error(
        `Expected status code 302 but got ${res.status}: ${JSON.stringify({ oldUrl, newUrl })}`,
      );
    }
    const location = res.headers.get('Location');
    const expected = newUrl.startsWith('http') ? newUrl : `https://${domain}${newUrl}`;
    if (location !== expected) {
      throw new Error(
        `Wrong redirect destination for ${oldUrl}: expected ${expected} but got ${location}`,
      );
    }
    checked[oldUrl] = true;
  } catch (err) {
    throw new Error(`Check failed for ${oldUrl}: ${err.message}`);
  }
}

const encode = (url: string) =>
  url
    .split('/')
    .map((x) => encodeURIComponent(x))
    .join('/');

async function main() {
  let batch = [];
  try {
    // Test urls.json
    for (const [oldUrl, newUrl] of Object.entries(urls)) {
      batch.push(check(encode(oldUrl), newUrl));
      batch.push(check(encodeURI(oldUrl), newUrl));
      batch.push(check(encode(encode(oldUrl)), newUrl));
      batch.push(check(encode(encode(encode(oldUrl))), newUrl));
      batch.push(check(encodeURI(encodeURI(oldUrl)), newUrl));
      batch.push(check(encodeURI(encodeURI(encodeURI(oldUrl))), newUrl));
      if (batch.length > concurrency) {
        await Promise.all(batch);
        batch = [];
      }
    }

    // Test urls.csv
    const input = fs.readFileSync('./lambda/uiOriginRequest/redirect/generate/urls.csv');
    const parser = parse(input);

    for await (const row of parser) {
      const oldUrl: string = row[0];
      const newUrl: string = row[1];

      if (oldUrl.includes('%25')) {
        batch.push(check(oldUrl, newUrl));
      } else {
        batch.push(check(encode(oldUrl), newUrl));
        batch.push(check(encodeURI(oldUrl), newUrl));
      }

      if (batch.length > concurrency) {
        await Promise.all(batch);
        batch = [];
      }
    }
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
}

main();
