import fetch from 'node-fetch';

async function check() {
  for (const path of [
    'tpci-order-status',
    'tpci-fluent-webhook',
    'tpci-transaction-email-api',
    'tpci-inventory-sync',
  ]) {
    const stage = process.env.STAGE || 'dev';
    const url =
      stage === 'prod'
        ? `https://services.pokemoncenter.com/${path}/health`
        : `https://services.${stage}.pokemoncenter.com/${path}/health`;
    try {
      const res = await fetch(url);
      const resJson = await res.json();
      if (!resJson || resJson.status !== 'healthy' || resJson.environmentName !== stage) {
        throw new Error(
          `${url} is unhealthy. StatusCode: ${res.status} Status: ${resJson.status} Environment: ${resJson.environmentName}`,
        );
      }
    } catch (error) {
      throw new Error(`${url} is unhealthy. Error: ${error.message}`);
    }
  }
}

check().catch((err) => {
  console.error(err);
  process.exit(1);
});
