import aws, { S3 } from "aws-sdk";
import fs from "fs";

  async function main() {
    const s3Client = new aws.S3();
    const sourceBucket = "img-sync-1";
    const destinationBucket = "img-sync-2";
    const fileName = "test/image.jpg";
    const topicArn =
      "arn:aws:sns:us-east-1:814445629751:nu-eu-pim-topic-undefined";

    const sourceFile = await s3Client
      .upload({
        Bucket: sourceBucket,
        Key: fileName,
        Body: fs.createReadStream("ci/test-image.jpg"),
        ContentType: "image/jpg",
      })
      .promise();
    const sns = new aws.SNS({
      region: "us-east-1",
    });
    await sns
      .publish({
        TopicArn: topicArn,
        Message: JSON.stringify({ paths: [fileName] }),
      })
      .promise();
    await new Promise((resolve) => setTimeout(resolve, 30000));
    const destinationFile = await s3Client
      .getObject({
        Bucket: destinationBucket,

        Key: fileName,
      })
      .promise();
    if (sourceFile.ETag === destinationFile.ETag) {
      await s3Client
        .deleteObject({
          Bucket: sourceBucket,
          Key: fileName,
        })
        .promise();
      await s3Client
        .deleteObject({
          Bucket: destinationBucket,
          Key: fileName,
        })
        .promise();
    } else {
      throw new Error("Failed to copy the image");
    }
  }
 main().catch((error) => {
  console.error(error);
  process.exit(1);
});
