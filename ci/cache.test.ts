import fetch, { RequestInfo, RequestInit, Response as FetchResponse } from 'node-fetch';
import { CloudFront } from 'aws-sdk';

const stage = process.env.STAGE || 'stage';

const TIMEOUT = 10000; // 10 seconds
const DELAY = 1000; // 1 second. The amount of time between an uncached request and cached

const wait = () => new Promise((r) => setTimeout(r, DELAY));

jest.setTimeout(TIMEOUT);

const domain = (() => {
  if (stage === 'prod') {
    return 'www.pokemoncenter.com';
  } else {
    return `${stage}.ui.pokemoncenter.com`;
  }
})();

async function fetchError(url: RequestInfo, resp: FetchResponse) {
  let headersText = '';
  for (const [k, v] of resp.headers.entries()) {
    headersText += `${k}: ${v}\n`;
  }

  return new Error(
    `Request ${url} failed:\n${resp.status} ${
      resp.statusText
    }\n${headersText}\n${await resp.text()}`,
  );
}

/**
 * This is a test helper function makes an HTTP request wait a bit and then
 * re-sends the same HTTP request and returns the second response.
 *
 * It throws an exception if the first request fails.
 */
async function refetch(url: RequestInfo, opts: RequestInit = {}): Promise<FetchResponse> {
  const req = async () => {
    const resp = await fetch(`https://${domain}${url}`, {
      timeout: (TIMEOUT - DELAY) / 2, // default timeout
      ...opts,
    });

    if (!resp.ok) {
      throw await fetchError(url, resp);
    }

    return resp;
  };

  await req();

  // wait a second for cache to propagate
  await wait();

  await req();

  // wait a second for cache to propagate
  await wait();

  return req();
}

const hit = 'Hit from cloudfront';
const miss = 'Miss from cloudfront';

async function expectHit(url: RequestInfo, opts?: RequestInit) {
  const resp = await refetch(url, opts);
  expect(resp.headers.get('x-cache')).toBe(hit);
}

async function expectMiss(url: RequestInfo, opts?: RequestInit) {
  const resp = await refetch(url, opts);
  expect(resp.headers.get('x-cache')).toBe(miss);
}

const products = [
  {
    url: 'https://stage.ui.pokemoncenter.com/product/T170-81568',
    title: 'Pokémon TCG: Sun & Moon-Unified Minds Booster Display Box (36 Booster Packs)',
    sku: 'T170-81568',
    id: 'qgqvbksuge3taljyge2tmoa=',
  },
  {
    url: 'https://stage.ui.pokemoncenter.com/product/T741-01622',
    title: 'Team Magma Relaxed Fit Hoodie',
    sku: 'T741-01622',
    id: 'qgqvbksug42dcljqge3demq=',
  },
  {
    url: 'https://stage.ui.pokemoncenter.com/product/T710-01001',
    title: 'Pikachu Mug',
    sku: 'T710-01001',
    id: 'qgqvbksug4ytaljqgeydami=',
  },
];

describe('cache', () => {
  if (process.env.STAGE !== 'stage') {
    it('only enabled in "stage"', () => {
      return;
    });
    return;
  }

  describe('/tpci-ecommweb-api', () => {
    describe('/product/* should be cached', () => {
      test.each(products.map(({ sku }) => sku))('%s', async (sku) => {
        await expectHit(`/tpci-ecommweb-api/product/${sku}`, {
          headers: {
            'x-store-scope': 'pokemon',
          },
        });
      });
    });

    describe('/product/status/* should be cached', () => {
      test.each(products.map(({ id }) => id))('%s', async (id) => {
        await expectHit(`/tpci-ecommweb-api/product/status/${id}?format=zoom.nodatalinks`, {
          headers: {
            'x-store-scope': 'pokemon',
          },
        });
      });
    });

    test.each(['/cart'])('%s should NOT be cached', async (path) => {
      await expectMiss(`/tpci-ecommweb-api${path}`, {
        headers: {
          'x-store-scope': 'pokemon',
        },
      });
    });

    test('/search should be cached', async () => {
      await expectHit(
        '/tpci-ecommweb-api/search?q=pikachu&_br_uid_2=uid%3D2753335682822%3Av%3D12.0%3Ats%3D1614192939078%3Ahc%3D24&ref_url=&rows=30&start=0&url=stage.ui.pokemoncenter.com&fl=pid,title,brand,price,sale_price,promotions,thumb_image,sku_thumb_images,sku_swatch_images,sku_color_group,url,price_range,sale_price_range,description,best_seller,launch_date,sale_price,PRF,display_price,display_sale_price,currency,reporting_crumb,reporting_product_name,&sort=&search_type=keyword',
      );
    });

    test('/suggest should be cached', async () => {
      await expectHit(
        '/tpci-ecommweb-api/suggest?q=pika&_br_uid_2=uid%3D2753335682822%3Av%3D12.0%3Ats%3D1614192939078%3Ahc%3D25&ref_url=&url=https://stage.ui.pokemoncenter.com/search/pikachu',
      );
    });
  });

  describe('/site (brXM) should be cached', () => {
    test('/resourceapi/about', async () => {
      await expectHit('/site/resourceapi/about');
    });
  });

  describe('/products/images (S3) should be cached', () => {
    test.each([
      '/P3091/701-00027/P3091_701-00027_01_thumb',
      '/P3091/701-00027/P3091_701-00027_01',
      '/P0837/T710-01001/P0837_T710-01001_01',
    ])('%s.jpg', async (fp) => {
      await expectHit(`/products/images${fp}.jpg`);
    });
  });

  describe('static files should be cached', () => {
    test.each([
      '/robots.txt', //
      '/favicon.ico',
    ])('%s', async (fn) => {
      await expectHit(fn);
    });
  });
});
