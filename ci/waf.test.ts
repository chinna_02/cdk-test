import fetch from 'node-fetch';
const TIMEOUT = 30000;
jest.setTimeout(TIMEOUT);

const stage = process.env.STAGE;
const domain =
  stage === 'prod' || !stage
    ? `https://www.pokemoncenter.com`
    : `https://${stage}.ui.pokemoncenter.com`;

describe('WAF test for /health rule', () => {
  test('should allow the path /health', async () => {
    const response = await fetch(`${domain}/health`, { method: 'GET' });
    expect(response.status).not.toBe(403);
  });
});

describe('WAF test for /tpci-order-status rule', () => {
  test('should not allow /tpci-order-status from unknown IPs', async () => {
    const response = await fetch(`${domain}/tpci-order-status/abcd`, {
      method: 'GET',
      headers: {
        'x-forwarded-for': '90.37.204.215',
      },
    });
    expect(response.status).toBe(403);
  });
  test('should allow /tpci-order-status from whitelisted IPs', async () => {
    const response = await fetch(`${domain}/tpci-order-status/`, {
      method: 'POST',
      headers: {
        'x-forwarded-for': '54.68.194.206',
      },
    });
    expect(response.headers.get('x-amz-apigw-id')).not.toBeNull();
  });
});

describe.skip('WAF test for SQLi and XSS rules', () => {
  test('should not allow with query parameters ', async () => {
    const response = await fetch(`${domain}/funko?a=abc&query=select * from`, { method: 'GET' });
    expect(response.status).toBe(403);
  });
  test('should not allow with cookie containing sql injections', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: { Cookie: 'DROP everything' },
    });
    expect(response.status).toBe(403);
  });
  test('should not allow with request sbody', async () => {
    const response = await fetch(`${domain}/funko`, {
      method: 'POST',
      body: JSON.stringify({ a: 'abc' }),
    });
    expect(response.status).toBe(403);
  });
});

describe('WAF test for XSS rule', () => {
  test('Should not allow request body with xss snippets', async () => {
    const response = await fetch(`${domain}/funko`, {
      method: 'POST',
      body: '<script\x20type="text/javascript">javascript:alert(1);</script>',
    });
    expect(response.status).toBe(403);
  });
  test('Should not allow request query with xss snippets', async () => {
    const response = await fetch(
      `${domain}/funko?inject=<script\x20type="text/javascript">javascript:alert(1);</script>`,
      { method: 'GET' },
    );
    expect(response.status).toBe(403);
  });
  test('should not allow with cookie containing xss injections ', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: { Cookie: '<script\x20type="text/javascript">javascript:alert(1);</script>' },
    });
    expect(response.status).toBe(403);
  });
});

describe('WAF test for specific urls', () => {
  test('should allow /tpci-fluent-webhook', async () => {
    const response = await fetch(`${domain}/tpci-fluent-webhook/`, {
      method: 'GET',
    });
    expect(response.headers.get('x-amz-apigw-id')).not.toBeNull();
  });
  test('should allow /tpci-transaction-email-api/', async () => {
    const response = await fetch(`${domain}/tpci-transaction-email-api/`, {
      method: 'GET',
    });
    expect(response.headers.get('x-amz-apigw-id')).not.toBeNull();
  });
});

describe('WAF test for rate limit rule', () => {
  test('Should block more than 100 requests', async () => {
    const responses = await Promise.all(
      Array(105)
        .fill(0)
        .map(() => fetch(`${domain}/tpci-ecommweb-api/as`, { method: 'GET' })),
    );
    const statuses = responses.map((r) => r.status);
    expect(statuses).toContain(403);
  });
});

describe('WAF test for internal-ip-whitelist rule', () => {
  test('should allow with a valid IP', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: {
        'x-forwarded-for': '54.68.194.206',
      },
    });
    expect(response.status).not.toBe(403);
  });
  test('should not allow with an valid IP', async () => {
    const response = await fetch(domain, {
      method: 'POST',
      headers: {
        'x-forwarded-for': '52.37.204.215',
      },
    });
    expect(response.status).toBe(403);
  });
});

describe.skip('WAF test for en-gb rule', () => {
  test('Should block /en-gb ', async () => {
    const response = await fetch(`${domain}/en-gb`, { method: 'GET' });
    expect(response.status).toBe(403);
  });
  test('Should allow /en-gb ', async () => {
    const response = await fetch(`${domain}/en-gb`, {
      method: 'GET',
      headers: {
        'x-forwarded-for': '54.68.194.206',
      },
    });
    expect(response.status).not.toBe(403);
  });
});

describe.skip('WAF test for external-ip-whitelist rule', () => {
  test('should allow with a whitelisted IP', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: {
        'x-forwarded-for': '54.68.194.206',
      },
    });
    expect(response.status).not.toBe(403);
  });
  test('should not allow with a non whitelisted IP', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: {
        'x-forwarded-for': '76.101.87.75',
      },
    });
    expect(response.status).toBe(403);
  });
});

describe('Block x-http-method-override header', () => {
  test('should block requests with x-http-method-override header', async () => {
    const response = await fetch(domain, {
      method: 'GET',
      headers: {
        'x-http-method-override': 'POST',
      },
    });
    expect(response.status).toBe(403);
  });
});
