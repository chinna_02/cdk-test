/**
 * Ensure that all redirects in urls.json point to valid pages.
 */
import urls from '../lambda/uiOriginRequest/redirect/urls.json';
import fetch from 'node-fetch';

const concurrency = 20;

async function check(input: string) {
  let url = input.split('?')[0];
  if (url.startsWith('/')) {
    url = `https://www.pokemoncenter.com${url}`;
  }
  const res = await fetch(url).catch((err) => {
    throw new Error(`Network Error: ${err.message}`);
  });
  if (!res.ok) {
    throw new Error(`Check failed for ${url} ${res.status} ${res.statusText}`);
  }
  // console.log(`${url} ${res.status}`);
}

const checked: { [key: string]: boolean } = {};

async function main() {
  let batch = [];
  try {
    for (const url of Object.values(urls)) {
      if (!checked[url]) {
        batch.push(check(url));
        checked[url] = true;
      }
      if (batch.length > concurrency) {
        await Promise.all(batch);
        batch = [];
      }
    }
  } catch (err) {
    console.error(err.message);
    process.exit(1);
  }
}

main();
