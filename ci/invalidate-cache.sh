#!/bin/bash

set -o errexit
set -o pipefail

cf=$(aws cloudfront list-distributions | jq -r ".DistributionList.Items[] | select(.Aliases.Quantity > 0) | select(.Aliases.Items[] | select(. == \"${STAGE}.ui.pokemoncenter.com\")) | .Id")

id=$(aws cloudfront create-invalidation --distribution-id "$cf" --paths '/*' | jq -r '.Invalidation.Id')

while [[ $(aws cloudfront get-invalidation --distribution-id "$cf" --id "$id" | jq -r '.Invalidation.Status') != "Completed" ]]; do
  sleep 10
done
