#!/usr/bin/env ts-node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';

import { version, dependencies } from '../package.json';
import { ApiDomainStack } from '../lib/stack/api-domain';
import { SESBounceStack } from '../lib/stack/bounce';
import { VpcStack } from '../lib/stack/vpc';
import { ApiWafsStack } from '../lib/stack/api-wafs';
import CloudFrontStack from '../lib/stack/cloudfront';
import ServicesStack from '../lib/stack/services';
import { Stage } from '../lib/stack/tpci';
import { dev, prod } from '../lib/accounts';
import { PIMS3 } from '../lib/stack/pim-s3';
import { DAMS3 } from '../lib/stack/damroot-s3';
import { EKS } from '../lib/stack/eks';
import { PimImageSyncStack } from '../lib/stack/pim-image-sync';
import { S3SFTP } from '../lib/stack/s3sftp';
import { PIMEUS3 } from '../lib/stack/pim-eu-s3';
import { isFeatureOn } from '../lib/feature-toggle';
import { CloudFrontStage } from '../lib/cloudfront';

export const app = new cdk.App();

const tags = cdk.Tags.of(app);
tags.add('managed-by', 'cdk');
tags.add('stack-version', version);
tags.add('cdk-version', dependencies['aws-cdk'].replace(/[\^~]/g, ''));
tags.add('repository', 'tpci-aws-cdk');
tags.add('repository-owner', 'slalomtpci');
tags.add('organization', 'tpci');
tags.add('project', 'cela');
tags.add('cicd', 'true');

let region = process.env.CDK_DEFAULT_REGION;
const account = process.env.CDK_DEFAULT_ACCOUNT;

const stage = process.env.STAGE;
const usStages: Stage[] = ['dev', 'stage', 'hfe', 'ppd', 'prod'];
const devStages: Stage[] = ['dev', 'stage', 'hfe', 'stage-eu'];
const prodStages: Stage[] = ['prod', 'ppd', 'prod-eu', 'ppd-eu'];
const euStages: Stage[] = ['stage-eu', 'ppd-eu', 'prod-eu'];

let cloudFrontStages = usStages as CloudFrontStage[];
let stages: Stage[] = usStages.concat(euStages);

if (stage) {
  stages = [stage as Stage];

  if (cloudFrontStages.includes(stage as CloudFrontStage)) {
    cloudFrontStages = [stage as CloudFrontStage];
  } else {
    cloudFrontStages = [];
  }

  if (account === prod && devStages.includes(stage as Stage)) {
    throw new Error(`${stage} cannot be deployed to prod AWS account`);
  }

  if (account === dev && prodStages.includes(stage as Stage)) {
    throw new Error(`${stage} cannot be deployed to non-prod AWS account`);
  }
} else {
  const removeStages = (stages: Stage[], rm: Stage[]) => stages.filter((el) => !rm.includes(el));

  if (account !== dev) {
    stages = removeStages(stages, devStages);
    cloudFrontStages = removeStages(cloudFrontStages, devStages) as CloudFrontStage[];
  }

  if (account !== prod) {
    stages = removeStages(stages, prodStages);
    cloudFrontStages = removeStages(cloudFrontStages, prodStages) as CloudFrontStage[];
  }

  if (region !== 'us-west-2') {
    stages = removeStages(stages, usStages);
  }

  if (region !== 'eu-west-1') {
    stages = removeStages(stages, euStages);
  }
}

const recipients = ['a.giltus@pokemon.com'];

if (account === prod) {
  recipients.push('l.harris@pokemon.com', 's.nikkhah@pokemon.com');
}

// All stacks are derived from the TPCIStack
// which generates the stack names tpci-aws-cdk-${name}-${stage}

// One stack per region
// These stacks don't have a "stage" because there should only be one per AWS account
for (const stage of stages as Stage[]) {
  // A stack for each "stage"
  // EU Region
  if (stage.endsWith('-eu')) {
    region = 'eu-west-1';
  } else {
    region = 'us-west-2';
  }

  // Every stage, regions eu and us
  if (['us-west-2', 'eu-west-1'].includes(region)) {
    new ApiWafsStack(app, 'api-wafs', {
      stage,
      env: {
        account,
        region,
      },
    });

    // One per region
    if (['dev', 'stage-eu', 'ppd', 'ppd-eu'].includes(stage)) {
      new VpcStack(app, `pcenter-vpc`, {
        env: {
          account,
          region,
        },
      });
    }
  }

  // Dev us-west-2 ONLY
  if (['dev'].includes(stage) && region === 'us-west-2') {
    new S3SFTP(app, `s3-sftp`, {
      env: {
        account,
        region,
      },
    });
  }

  // One per region and us-west-2 ONLY
  if (['dev', 'ppd'].includes(stage) && region === 'us-west-2') {
    new SESBounceStack(app, 'ses-bounce', {
      recipients,
      env: {
        account,
        region,
      },
    });

    new PimImageSyncStack(app, `pim-image-sync`, {
      env: {
        account,
        region,
      },
    });
  }

  // Only deploy in Dev and Stage for US-West-2
  if (['dev', 'stage'].includes(stage) && region === 'us-west-2') {
    new EKS(app, 'eks-cluster', {
      stage,
      env: {
        account: dev,
        region,
      },
    });
  }

  if (['stage-eu', 'prod-eu'].includes(stage) && region === 'eu-west-1') {
    /* Dirty hack required as PIM S3 already deployed with a stage name of "stage"
       Not a stage name of "stage-eu". This would cause lots of issues. */
    const cleanStageStr = stage.replace('-eu', '');
    new PIMEUS3(app, 'pim-eu-s3', {
      stage: cleanStageStr as Stage,
      env: {
        account,
        region,
      },
    });
  }

  // us-west-2 ONLY
  if (region === 'us-west-2') {
    new ApiDomainStack(app, 'api-domain', {
      stage,
      env: {
        account,
        region,
      },
    });

    new PIMS3(app, 'pim-s3', {
      stage,
      env: {
        account,
        region,
      },
    });

    new DAMS3(app, 'damroot-s3', {
      stage,
      env: {
        account,
        region,
      },
    });
  }
}

// CloudFront Stacks
// us-east-1 ONLY
// For now only deploy cloudfront in the us-west-2 region

// But it should probably be moved to its own pipeline in us-east-1
if (region !== 'eu-west-1') {
  (async () => {
    for (const stage of cloudFrontStages) {
      const maintenanceMode = await isFeatureOn(
        `/${stage}/feature-toggle/tpci-aws-cdk/maintenance`,
        {
          region: 'us-east-1',
        },
      );

      const caMaintenanceMode = await isFeatureOn(
        `/${stage}/feature-toggle/tpci-aws-cdk/ca-maintenance`,
        {
          region: 'us-east-1',
        },
      );

      new CloudFrontStack(app, 'cloudfront', {
        stage,
        allSitesMaintanceMode: maintenanceMode,
        caSiteMaintanceMode: caMaintenanceMode,
        env: {
          account,
          region: 'us-east-1',
        },
      });

      new ServicesStack(app, 'services', {
        stage,
        env: {
          account,
          region: 'us-east-1',
        },
      });
    }
  })();
}

export default app;
