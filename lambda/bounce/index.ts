import { ScheduledEvent, Handler } from 'aws-lambda';
import SQS from 'aws-sdk/clients/sqs';
import SNS from 'aws-sdk/clients/sns';

const Charset = 'utf-8';

export const handler: Handler<ScheduledEvent> = async (event, context) => {
  const queueName = process.env.QUEUE;
  if (!queueName) {
    throw new Error('Environment variable QUEUE is not set');
  }

  const topicArn = process.env.TOPIC;
  if (!topicArn) {
    throw new Error('Environment variable TOPIC is not set');
  }

  const sqs = new SQS();
  const sns = new SNS();
  const QueueUrl = sqs.endpoint.href + event.account + '/' + queueName;

  const receipts: { ReceiptHandle: string }[] = [];
  const alerts = [];
  let res: SQS.ReceiveMessageResult;
  do {
    res = await sqs.receiveMessage({ QueueUrl }).promise();

    for (const msg of res.Messages || []) {
      const { Body } = msg;
      if (Body) {
        try {
          const notification = JSON.parse(Body);
          console.log('Notification:', notification);
          if (notification && notification.Message) {
            try {
              alerts.push(JSON.parse(notification.Message));
            } catch {
              alerts.push(notification.Message);
            }
          }
        } catch (error) {
          console.error('Failed to parse Body as JSON:', error, msg);
        }
      }
      const { ReceiptHandle } = msg;
      if (ReceiptHandle) {
        receipts.push({ ReceiptHandle });
      }
    }
  } while ((res?.Messages?.length ?? 0) > 0);

  if (alerts.length < 1) {
    return;
  }

  const accountName = (() => {
    switch (event.account) {
      case '053655145506':
        return `pokemoncenter-dev (${event.account})`;
      case '322789178911':
        return `pokemoncenter-prod (${event.account})`;
      default:
        return event.account;
    }
  })();

  await sns
    .publish({
      TopicArn: topicArn,
      Subject: 'SES Bounce/Complaint Alert',
      Message: `Total SES Bounces/Complaints: ${alerts.length}
Account: ${accountName}
Alert Lambda: ${context.functionName}
SQS Queue: ${queueName}

${'-'.repeat(80)}

${alerts.map((alert) => JSON.stringify(alert, null, 2)).join(`\n\n${'='.repeat(80)}\n\n`)}
`,
    })
    .promise();

  const promises = [];
  const size = 10; // deleteMessageBatch can only be done in chunks of 10
  let index = 0;
  while (index < receipts.length) {
    promises.push(
      sqs
        .deleteMessageBatch({
          Entries: receipts.slice(index, index + size).map(({ ReceiptHandle }, idx) => ({
            Id: `${idx + 1}`,
            ReceiptHandle,
          })),
          QueueUrl,
        })
        .promise(),
    );
    index += size;
  }

  await Promise.all(promises);
};
