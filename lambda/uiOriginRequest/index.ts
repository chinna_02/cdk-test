import { CloudFrontRequestEvent, CloudFrontRequest, CloudFrontResponse, Handler } from 'aws-lambda';
import * as redirect from './redirect';
import * as localize from './localize';
import { prod } from '../../lib/accounts';

export const handler: Handler<CloudFrontRequestEvent, CloudFrontRequest | CloudFrontResponse> = (
  event,
  context,
  callback,
) => {
  const cf = event.Records[0].cf;
  const stage = {
    EBIERFXQB8X99: 'dev',
    EA60MAV3RRPA3: 'stage',
    E30FS25EQONK9G: 'ppd',
    E3L61D6I4WJNSK: 'prod',
    E23T1VLARLNKAD: 'hfe',
  }[cf.config.distributionId];

  redirect.handler(event, context, callback);

  const request = localize.handler(cf.request);

  // tpci-ecommweb-lambda
  const euUiGatewayId = {
    stage: '51s77a6rk3',
    ppd: 'mjj1d00yg7',
    prod: '2eip5fznuf',
  }[stage ?? ''];

  const usUiGatewayId = {
    stage: 'a2b0a69f0b',
  }[stage ?? ''];

  const customOrigin = request.origin?.custom;

  if (
    euUiGatewayId &&
    customOrigin &&
    JSON.stringify(request.headers['Shopping-Region']) ===
      JSON.stringify([{ key: 'Shopping-Region', value: 'EUR' }])
  ) {
    request.origin = {
      custom: {
        ...customOrigin,
        domainName: `${euUiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
        path: `/${stage}-eu`,
      },
    };
    request.headers.host = [
      {
        key: 'Host',
        value: `${euUiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
      },
    ];
  }
  // We can remove the below logging after SiteSpect is implemented to production.
  console.log('Full event #################');
  console.log(JSON.stringify(event.Records[0]));

  callback(null, request);
};
