#!/usr/bin/env ts-node

import parse from 'csv-parse';
import fs from 'fs';
import { URLSearchParams } from 'url';

const unordered: { [key: string]: string } = {};
const ordered: { [key: string]: string } = {};

fs.readFile('urls.csv', (err, input) => {
  if (err) {
    throw err;
  }

  const parser = parse(input, (err, output) => {
    if (err) {
      throw err;
    }

    output.forEach((row: string[]) => {
      let k: string = row[0];
      const v: string = row[1] || '/';

      if (k.includes('?')) {
        console.log('Found query parameters:', { [k]: v });
        k = k.split('?')[0];
      }

      // It's faster to precompute the encoded and decoded forms during lookup

      let decoded = k;
      // Fully decode the url (up to 4 times)
      for (let decodeCount = 4; decodeCount > 0; decodeCount--) {
        const newDecoded = decodeURIComponent(decoded);
        if (newDecoded === decoded) {
          break;
        }
        decoded = newDecoded;
      }

      if (decoded in unordered && unordered[decoded] !== v) {
        console.error('Duplicate:', { url: decoded, dest1: unordered[k], dest2: v });
        throw new Error(`Duplicate: ${decoded}`);
      }

      if (decoded !== v) {
        // decoded === v then it would cause a redirect loop
        unordered[decoded] = v;
      }
    });

    // Collapse multi-redirect chains
    for (const oldUrl in unordered) {
      let newUrl = unordered[oldUrl];
      let loopCount = 0;
      while (newUrl) {
        if (newUrl in unordered) {
          unordered[oldUrl] = unordered[newUrl];
        }
        newUrl = unordered[newUrl];
        loopCount += 1;
        if (loopCount >= 4) {
          throw new Error(`Redirect loop found for ${oldUrl}`);
        }
      }
    }

    Object.keys(unordered)
      .sort()
      .forEach((key) => {
        ordered[key] = unordered[key];
      });

    fs.writeFileSync('../urls.json', JSON.stringify(ordered, null, 2) + '\n');
  });
});
