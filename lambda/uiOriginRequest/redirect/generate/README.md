This is a script to take a urls.csv from an Excel spreadsheet
and convert it to JSON.

Some cleanup that happens in the process:

- Remove query parameters
- URL decode
- URL encode (up to 3 times)
- Collapse multi-redirects
- Sort by keys

Input file is `urls.csv`. Output is `../urls.json`

On first usage run: `npm i`

To use run: `npm run start`
