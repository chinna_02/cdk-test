import parse from 'csv-parse';
import fs from 'fs';
import { redirectLocation, handler } from './index';
import {
  CloudFrontRequest,
  CloudFrontResponse,
  CloudFrontRequestEvent,
  Context,
  Callback,
} from 'aws-lambda';

describe('redirectLocation', () => {
  test('match /site', () => {
    expect(redirectLocation('/site')).toBe('/site/');
  });

  test('no match /site/', () => {
    expect(redirectLocation('/site/')).toBe(undefined);
  });

  test('no match /site/foobar', () => {
    expect(redirectLocation('/site/foobar')).toBe(undefined);
  });

  test('no match /foobar', () => {
    expect(redirectLocation('/foobar')).toBe(undefined);
  });

  test('old image url from urls.json', () => {
    expect(
      redirectLocation(
        '/wcsstore/PokemonCatalogAssetStore/images/catalog/products/P0883/710-01027/P0883_710-01027_01_full.jpg',
      ),
    ).toBe('/products/images/P0883/710-01027/P0883_710-01027_01_full.jpg');
  });

  test('old image url not in urls.json', () => {
    expect(
      redirectLocation(
        '/wcsstore/PokemonCatalogAssetStore/images/catalog/products/P0883/710-12345/P0883_710-12345_99_full.jpg',
      ),
    ).toBe('/products/images/P0883/710-12345/P0883_710-12345_99_full.jpg');
  });

  test('handle encoded URLS', () => {
    expect(
      redirectLocation('/an-afternoon-with-eevee-friends%3A-vaporeon-figure-by-funko-703-00548'),
    ).toBe('/product/703-00548/an-afternoon-with-eevee-and-friends-vaporeon-figure-by-funko');
  });

  test('handle double encoded URLS', () => {
    expect(redirectLocation('/arcanine-pok%25C3%25A9mon-petit-plush-keychain-701-00991')).toBe(
      '/category/keychains-clips-and-card-cases',
    );
  });

  test('handle triple encoded URLS', () => {
    expect(redirectLocation('/arcanine-pok%2525C3%2525A9mon-petit-plush-keychain-701-00991')).toBe(
      '/category/keychains-clips-and-card-cases',
    );
  });

  test('handle encoded URLS that has other special characters', () => {
    expect(
      redirectLocation(
        '/around-the-world-kung-fu-pikachu-pok%25C3%25A9-plush-(standard)---8-1-2-quot;-701-00016',
      ),
    ).toBe('/product/701-00016/kung-fu-pikachu-around-the-world-poke-plush-8-1-2-in');
  });

  test('handles all entries in urls.csv', (done) => {
    fs.readFile('./lambda/uiOriginRequest/redirect/generate/urls.csv', (err, input) => {
      if (err) {
        console.error(err);
        done(err);
        return;
      }

      const parser = parse(input, (err, output) => {
        if (err) {
          done(err);
          throw err;
        }

        output.forEach((row: string[]) => {
          const oldUrl: string = row[0];
          const newUrl: string = row[1];
          const actualUrl = redirectLocation(oldUrl);
          if (actualUrl !== newUrl) {
            console.log({ oldUrl, newUrl, actualUrl });
          }
          expect(actualUrl).toBe(newUrl);
        });
        done();
      });
    });
  });

  test.each([
    ['/collections/squishy-plush', '/category/squishy-plush'],
    ['/collections/first-partner-pokemon-plush', '/category/first-partner-pokemon-plush'],
    ['/collections/grass-type-first-partner-plush', '/category/grass-type-first-partner-plush'],
    ['/collections/galar-legendary-collection', '/category/galar-legendary-collection'],
    ['/collections/halloween-collection', '/category/halloween-collection'],
    ['/collections/detective-pikachu-collection', '/category/detective-pikachu-collection'],
    ['/collections/grass-type-pokemon-collection', '/category/grass-type-pokemon-collection'],
    ['/collections/graduation-pikachu-collection', '/category/graduation-pikachu-collection'],
    ['/collections/team-rocket-collection', '/category/team-rocket-collection'],
    ['/collections/relax-with-eevee-collection', '/category/relax-with-eevee-collection'],
  ])('%s redirect to %s', (oldUrl, newUrl) => {
    expect(redirectLocation(oldUrl)).toBe(newUrl);
  });
});

const expectRedirect = (
  status: string,
  location: string,
  done: jest.DoneCallback,
): Callback<CloudFrontRequest | CloudFrontResponse> => {
  const callback: Callback<CloudFrontRequest | CloudFrontResponse> = (_, res) => {
    try {
      if (res && 'status' in res) {
        expect(res.status).toBe(status);
        expect(res.headers.location[0].value).toBe(location);
      } else {
        done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
      }
      done();
    } catch (err) {
      done(err);
    }
  };
  return callback;
};

describe('handler', () => {
  const event = (request: Partial<CloudFrontRequest>): CloudFrontRequestEvent => ({
    Records: [
      {
        cf: {
          config: {
            distributionDomainName: 'd111111abcdef8.cloudfront.net',
            distributionId: 'EDFDVBD6EXAMPLE',
            eventType: 'origin-request',
            requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
          },
          request: {
            clientIp: '203.0.113.178',
            headers: {
              'x-forwarded-for': [
                {
                  key: 'X-Forwarded-For',
                  value: '203.0.113.178',
                },
              ],
              'user-agent': [
                {
                  key: 'User-Agent',
                  value: 'Amazon CloudFront',
                },
              ],
              via: [
                {
                  key: 'Via',
                  value: '2.0 2afae0d44e2540f472c0635ab62c232b.cloudfront.net (CloudFront)',
                },
              ],
              host: [
                {
                  key: 'Host',
                  value: 'example.org',
                },
              ],
              'cache-control': [
                {
                  key: 'Cache-Control',
                  value: 'no-cache, cf-no-cache',
                },
              ],
            },
            method: 'GET',
            origin: {
              custom: {
                customHeaders: {},
                domainName: 'example.org',
                keepaliveTimeout: 5,
                path: '',
                port: 443,
                protocol: 'https',
                readTimeout: 30,
                sslProtocols: ['TLSv1', 'TLSv1.1', 'TLSv1.2'],
              },
            },
            querystring: '',
            uri: '/',
            ...request,
          },
        },
      },
    ],
  });

  test('does not redirect homepage', (done) => {
    const callback: Callback<CloudFrontRequest | CloudFrontResponse> = () => {
      done('callback was called');
    };

    handler(event({}), {} as Context, callback);
    done();
  });

  test('preserves utm params when redirecting', (done) => {
    handler(
      event({
        uri: '/6-and-under',
        querystring: 'utm_source=test',
      }),
      {} as Context,
      expectRedirect('302', '/category/6-and-under?utm_source=test', done),
    );
  });

  test('ignores old query parameters when redirecting', (done) => {
    handler(
      event({
        uri: '/AjaxOrderItemDisplayView',
        querystring:
          'catalogId=3074457345616676718&langId=-1&langId=-1&storeId=715838034&storeId=715838034',
      }),
      {} as Context,
      expectRedirect('302', '/cart', done),
    );
  });

  test('ignores old query parameters but preserves utm params when redirecting', (done) => {
    handler(
      event({
        uri: '/AjaxOrderItemDisplayView',
        querystring:
          'catalogId=3074457345616676718&langId=-1&langId=-1&storeId=715838034&storeId=715838034&utm_campaign=test',
      }),
      {} as Context,
      expectRedirect('302', '/cart?utm_campaign=test', done),
    );
  });

  test('supports domain names in redirect', (done) => {
    handler(
      event({
        uri: '/REDEEM',
      }),
      {} as Context,
      expectRedirect(
        '302',
        'https://support.pokemoncenter.com/hc/en-us/articles/360001395843',
        done,
      ),
    );
  });

  test('merges redirect with utm params in new location', (done) => {
    handler(
      event({
        uri: '/YOU',
        querystring: 'utm_source=test',
      }),
      {} as Context,
      expectRedirect(
        '302',
        '/category/simply-pikachu?utm_source=test&utm_medium=referral&utm_campaign=you',
        done,
      ),
    );
  });
});
