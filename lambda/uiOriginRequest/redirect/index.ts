import { CloudFrontRequestEvent, CloudFrontRequest, CloudFrontResponse, Handler } from 'aws-lambda';
import urls from './urls.json';
import querystring from 'querystring';

export function redirectLocation(location: string): string | undefined {
  if (location === '/site') {
    return '/site/';
  }

  let decoded = location;
  try {
    for (let i = 0; i < 3; i++) {
      decoded = decodeURIComponent(decoded);
    }
  } catch {}

  const newLocation = (urls as { [key: string]: string })[decoded];
  if (newLocation) {
    return newLocation;
  }

  if (location.startsWith('/collections/')) {
    return location.replace('/collections/', '/category/');
  }

  // Catch all in case a product image is not listed in the urls.json
  // These tend to be valid product image urls that map directly from the old site to the new site.
  if (location.startsWith('/wcsstore/PokemonCatalogAssetStore/images/catalog/products/')) {
    return location.replace(
      '/wcsstore/PokemonCatalogAssetStore/images/catalog/products/',
      '/products/images/',
    );
  }

  return undefined;
}

export const handler: Handler<CloudFrontRequestEvent, CloudFrontRequest | CloudFrontResponse> = (
  event,
  context,
  callback,
) => {
  const request = event.Records[0].cf.request;

  const location = redirectLocation(request.uri);

  if (location) {
    const [redirectLocation, redirectQuerystring] = location.split('?');
    const redirectParams = querystring.parse(redirectQuerystring || '');

    // Pass-through 'utm_*' parameters
    const requestParams = querystring.parse(request.querystring);
    const utmParams = Object.keys(requestParams)
      .filter((key) => key.startsWith('utm_'))
      .reduce((obj, key) => {
        return {
          ...obj,
          [key]: requestParams[key],
        };
      }, {});

    const params = {
      ...redirectParams,
      ...utmParams,
    };

    const qs = Object.keys(params).length > 0 ? `?${querystring.stringify(params)}` : '';

    callback(null, {
      status: '302', // Use a Temporary redirect to reduce the risk of users' browsers keeping a bad redirect
      statusDescription: 'Found',
      headers: {
        location: [
          {
            key: 'Location',
            value: `${redirectLocation}${qs}`,
          },
        ],
      },
    });
  } else if (request.uri.startsWith('/wcsstore')) {
    // Anything from /wcsstore is from the old site.
    // Everything besides product images is no longer valid.
    callback(null, {
      status: '404',
      statusDescription: 'Not Found',
      headers: {},
    });
  }
};
