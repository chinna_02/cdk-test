/**
 * A bad entry in urls.json could break the site
 *   */
const urls = require('./urls.json') as { [key: string]: string }; // eslint-disable-line @typescript-eslint/no-var-requires

// Using `for (const urls in url)` rather than test.each()
// because with test.each() node runs out of memory

test(`url must not contain query parameters`, () => {
  for (const url in urls) {
    expect(url).not.toContain('?');
  }
});

test(`url should not contain %`, () => {
  for (const url in urls) {
    expect(url).not.toContain('%');
  }
});

test(`redirect destination must not return empty string`, () => {
  for (const url in urls) {
    const redirect = urls[url];
    expect(redirect).not.toBe('');
  }
});

test(`redirect destination should not be encoded`, () => {
  for (const url in urls) {
    const redirect = urls[url];
    expect(redirect).not.toContain('%');
  }
});

test('old url must not equal new url or else it will cause a redirect loop', () => {
  for (const url in urls) {
    expect(url).not.toBe(urls[url]);
  }
});

test('new urls not point to new urls or else it will cause a redirect loop', () => {
  for (const oldUrl in urls) {
    const newUrl = urls[oldUrl];
    if (newUrl in urls) {
      console.log({ oldUrl, newUrl, newUrlRedirect: urls[newUrl] });
    }
    expect(newUrl in urls).toBeFalsy();
  }
});
