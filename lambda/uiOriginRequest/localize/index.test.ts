import { handler, removeLocaleInURI } from './index';
import { CloudFrontRequest, CloudFrontRequestEvent } from 'aws-lambda';

describe('removeLocaleInURI', () => {
  test('Locale - /en-us/category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/en-us/category/figures-and-pins')).toBe(
      '/category/figures-and-pins',
    );
  });

  test('Locale - /en-ca/category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/en-ca/category/figures-and-pins')).toBe(
      '/category/figures-and-pins',
    );
  });

  test('Locale - /en-ca/category/en-ca123, return /category/en-ca123', () => {
    expect(removeLocaleInURI('/en-ca/category/en-ca123')).toBe('/category/en-ca123');
  });

  test('Locale - /en-ca, return /', () => {
    expect(removeLocaleInURI('/en-ca')).toBe('/');
  });

  test('Locale - /category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/category/figures-and-pins')).toBe('/category/figures-and-pins');
  });

  test('Locale - /en-us, must strip /en-us from the URI', () => {
    expect(removeLocaleInURI('/en-us')).toBe('/');
  });

  test('Locale - /en-us-xyz, return /en-us-xyz', () => {
    expect(removeLocaleInURI('/en-us-xyz')).toBe('/en-us-xyz');
  });

  test('Locale - /en-ca/site, return /site', () => {
    expect(removeLocaleInURI('/en-ca/site')).toBe('/site');
  });

  test('Locale - /en-caxyz, return /en-caxyz', () => {
    expect(removeLocaleInURI('/en-caxyz')).toBe('/en-caxyz');
  });

  test('Locale - /en-gb/en-gbxyz, return /en-gbxyz', () => {
    expect(removeLocaleInURI('/en-gb/en-gbxyz')).toBe('/en-gbxyz');
  });

  test('Locale - /EN-US, must strip /EN-US from the URI', () => {
    expect(removeLocaleInURI('/EN-US')).toBe('/');
  });

  test('Locale - /category/en-gb, must not strip /en-gb from the URI', () => {
    expect(removeLocaleInURI('/category/en-gb')).toBe('/category/en-gb');
  });
  test('Locale - /category/en-ca, must not strip /en-ca from the URI', () => {
    expect(removeLocaleInURI('/category/en-ca')).toBe('/category/en-ca');
  });
  test('Locale - /en-gb/category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/en-gb/category/figures-and-pins')).toBe(
      '/category/figures-and-pins',
    );
  });

  test('Locale - /en-gb/category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/en-gb/category/figures-and-pins')).toBe(
      '/category/figures-and-pins',
    );
  });

  test('Locale - /en-gb/category/en-gb123, return /category/en-gb123', () => {
    expect(removeLocaleInURI('/en-gb/category/en-gb123')).toBe('/category/en-gb123');
  });

  test('Locale - /en-gb, return /', () => {
    expect(removeLocaleInURI('/en-gb')).toBe('/');
  });

  test('Locale - /category/figures-and-pins, return /category/figures-and-pins', () => {
    expect(removeLocaleInURI('/category/figures-and-pins')).toBe('/category/figures-and-pins');
  });

  test('Locale - /en-gb, must strip /en-gb from the URI', () => {
    expect(removeLocaleInURI('/en-gb')).toBe('/');
  });

  test('Locale - /en-gb-xyz, return /en-gb-xyz', () => {
    expect(removeLocaleInURI('/en-gb-xyz')).toBe('/en-gb-xyz');
  });

  test('Locale - /en-gb/site, return /site', () => {
    expect(removeLocaleInURI('/en-gb/site')).toBe('/site');
  });

  test('Locale - /en-gbxyz, return /en-gbxyz', () => {
    expect(removeLocaleInURI('/en-gbxyz')).toBe('/en-gbxyz');
  });

  test('Locale - /en-gb/en-gbxyz, return /en-gbxyz', () => {
    expect(removeLocaleInURI('/en-gb/en-gbxyz')).toBe('/en-gbxyz');
  });

  test('Locale - /EN-GB, must strip /EN-GB from the URI', () => {
    expect(removeLocaleInURI('/EN-US')).toBe('/');
  });

  test('Locale - /category/en-gb, must not strip /en-gb from the URI', () => {
    expect(removeLocaleInURI('/category/en-gb')).toBe('/category/en-gb');
  });
  test('Locale - /category/en-gb, must not strip /en-gb from the URI', () => {
    expect(removeLocaleInURI('/category/en-gb')).toBe('/category/en-gb');
  });
});

describe('handleHeaders', () => {
  const event = (request: Partial<CloudFrontRequest>): CloudFrontRequestEvent => ({
    Records: [
      {
        cf: {
          config: {
            distributionDomainName: 'd111111abcdef8.cloudfront.net',
            distributionId: 'EDFDVBD6EXAMPLE',
            eventType: 'origin-request',
            requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
          },
          request: {
            clientIp: '203.0.113.178',
            headers: {
              'x-forwarded-for': [
                {
                  key: 'X-Forwarded-For',
                  value: '203.0.113.178',
                },
              ],
              'user-agent': [
                {
                  key: 'User-Agent',
                  value: 'Amazon CloudFront',
                },
              ],
              via: [
                {
                  key: 'Via',
                  value: '2.0 2afae0d44e2540f472c0635ab62c232b.cloudfront.net (CloudFront)',
                },
              ],
              host: [
                {
                  key: 'Host',
                  value: 'example.org',
                },
              ],
              'cache-control': [
                {
                  key: 'Cache-Control',
                  value: 'no-cache, cf-no-cache',
                },
              ],
            },
            method: 'GET',
            origin: {
              custom: {
                customHeaders: {},
                domainName: 'example.org',
                keepaliveTimeout: 5,
                path: '',
                port: 443,
                protocol: 'https',
                readTimeout: 30,
                sslProtocols: ['TLSv1', 'TLSv1.1', 'TLSv1.2'],
              },
            },
            querystring: '',
            uri: '/',
            ...request,
          },
        },
      },
    ],
  });

  test('Return CA request headers & stripped URI for /en-ca', (done) => {
    const returnValue = handler(
      event({
        uri: '/en-ca/category/figures-and-pins',
      }).Records[0].cf.request,
    );
    expect(returnValue.headers['Shopping-Region']).toStrictEqual([
      { key: 'Shopping-Region', value: 'CAD' },
    ]);
    expect(returnValue.headers['Accept-Language']).toStrictEqual([
      { key: 'Accept-Language', value: 'en-CA' },
    ]);
    expect(returnValue.uri).toStrictEqual('/category/figures-and-pins');
    done();
  });

  test('Return US request headers & stripped URI for /en-us', (done) => {
    const returnValue = handler(
      event({
        uri: '/en-us/category/figures-and-pins',
      }).Records[0].cf.request,
    );
    expect(returnValue.headers['Shopping-Region']).toStrictEqual([
      { key: 'Shopping-Region', value: 'USD' },
    ]);
    expect(returnValue.headers['Accept-Language']).toStrictEqual([
      { key: 'Accept-Language', value: 'en-US' },
    ]);
    expect(returnValue.uri).toStrictEqual('/category/figures-and-pins');
    done();
  });

  test('Retrun US request headers & unchanged URI for URI with no locale', (done) => {
    const returnValue = handler(
      event({
        uri: '/prod/category/figures-and-pins',
      }).Records[0].cf.request,
    );
    expect(returnValue.headers['Shopping-Region']).toStrictEqual([
      { key: 'Shopping-Region', value: 'USD' },
    ]);
    expect(returnValue.headers['Accept-Language']).toStrictEqual([
      { key: 'Accept-Language', value: 'en-US' },
    ]);
    expect(returnValue.uri).toStrictEqual('/prod/category/figures-and-pins');
    done();
  });
});
