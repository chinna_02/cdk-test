import { CloudFrontRequest } from 'aws-lambda';

//function to strip /en-us or /en-ca or /en-gb from the URI if uri starts with /prod/en-us(ca)(gb)
export function removeLocaleInURI(uri: string): string {
  const regex = /(^\/en-(us|ca|gb))(?=\/|$)/i;
  uri = uri.replace(regex, '');
  return uri || '/';
}

//function to add locale headers based on the URI
export function handler(request: CloudFrontRequest): CloudFrontRequest {
  let shopRegion = 'USD';
  let language = 'en-US';

  // for Canada URI set appropriate header values
  if (request.uri.toLowerCase().includes('/en-ca')) {
    shopRegion = 'CAD';
    language = 'en-CA';
  }

  // for UK URI set appropriate header values
  if (request.uri.toLowerCase().includes('/en-gb')) {
    shopRegion = 'EUR';
    language = 'en-GB';
  }

  // add locale headers to request
  request.headers['Shopping-Region'] = [{ key: 'Shopping-Region', value: shopRegion }];
  request.headers['Accept-Language'] = [{ key: 'Accept-Language', value: language }];

  request.uri = removeLocaleInURI(request.uri);

  return request;
}
