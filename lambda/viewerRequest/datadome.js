/**
 * DataDome Lambda@Edge module.
 *
 * @version 1.8
 * @author DataDome (https://datadome.co)
 */

'use strict';

/***************************/
/* DataDome default config */
/***************************/

// License key (DATADOME_LICENSE_KEY).
var DATADOME_LICENSE_KEY = 'aiyBz0iGXG7ZJm3';

// API connection timeout in milliseconds (DATADOME_TIMEOUT).
var DATADOME_TIMEOUT = 150;

// Maximum number of open sockets per lambda function
var DATADOME_MAX_SOCKETS = 100;

// URIRegex (DATADOME_URI_REGEX) and URIRegexExclusion (DATADOME_URI_REGEX_EXCLUSION)
// are regex used to match URI.
//
// The logic is:
//   Does URI match with URIRegexExclusion if present?
//     if yes stop
//       if no Does URI match with URIRegex if present?
//       if no stop
//       if yes, send to API
//
// Example with a URIRegexExclusion:
//   DATADOME_URI_REGEX_EXCLUSION /\.(js|css|jpg|jpeg|png|ico|gif|tiff|svg|woff|woff2|ttf|eot|mp4|otf)$/
//
// Default behavior excludes static assets.
// To disable a regex, set null value.
var DATADOME_URI_REGEX = null;
var DATADOME_URI_REGEX_EXCLUSION =
  /\.(js|css|jpg|jpeg|png|ico|gif|tiff|svg|woff|woff2|ttf|eot|mp4|otf)$/i;

/*********************/
/* DataDome const    */
/*********************/
const datadomeHost = 'api-lambda.datadome.co';
const datadomePath = '/validate-request/';
const datadomeContentType = 'application/x-www-form-urlencoded';
const datadomeUserAgent = 'DataDome';
const datadomeModuleName = 'Lambda@Edge';
const datadomeModuleVersion = '1.8';
const datadomeRequestPort = 0;

/******************************/
/* Requirements and main app. */
/******************************/
const http = require('http');
const querystring = require('querystring');

const keepAliveAgent = new http.Agent({
  keepAlive: true,
  maxSockets: DATADOME_MAX_SOCKETS,
});

exports.handler = (event, context, callback) => {
  /********************/
  /* DataDome process */
  /********************/

  const request = event.Records[0].cf.request;
  const domain = request.headers.host[0].value;
  const isStage = domain.includes('stage.');
  const isDev = domain.includes('dev.');
  const isHfe = domain.includes('hfe.');
  const isNonProd = isStage || isDev || isHfe;

  if (isNonProd) {
    callback(null, request);
    return;
  }
  try {
    /////////////////////////
    // Test exclusion regex
    if (DATADOME_URI_REGEX_EXCLUSION !== null) {
      if (DATADOME_URI_REGEX_EXCLUSION.test(request.uri) !== false) {
        callback(null, request);

        return;
      }
    }

    if (
      request.clientIp === '35.163.145.243' ||
      request.clientIp === '34.212.113.224' ||
      request.clientIp === '34.217.189.165' ||
      request.clientIp === '35.164.215.23' ||
      request.clientIp === '52.39.74.213' ||
      request.clientIp === '54.68.194.206'
    ) {
      callback(null, request);
      return;
    }
    /////////////////////////
    // Test inclusion regex
    if (DATADOME_URI_REGEX !== null) {
      if (DATADOME_URI_REGEX.test(request.uri) === false) {
        callback(null, request);

        return;
      }
    }

    ////////////////////////
    // Builds request data
    var clientIdAndCookiesLength = getClientIdAndCookiesLength(request.headers);

    var requestData = {
      Key: DATADOME_LICENSE_KEY,
      RequestModuleName: datadomeModuleName,
      ModuleVersion: datadomeModuleVersion,
      ServerName: context.invokedFunctionArn,
      APIConnectionState: 'new',
      IP: request.clientIp,
      Port: datadomeRequestPort,
      TimeRequest: getCurrentMicroTime(),
      Protocol: getRequestProtocol(request),
      Method: request.method,
      ServerHostname: getHeader(request.headers, 'host'),
      Request: request.uri,
      HeadersList: getHeadersList(request.headers),
      Host: getHeader(request.headers, 'host'),
      UserAgent: getHeader(request.headers, 'user-agent'),
      Referer: getHeader(request.headers, 'referer'),
      Accept: getHeader(request.headers, 'accept'),
      AcceptEncoding: getHeader(request.headers, 'accept-encoding'),
      AcceptLanguage: getHeader(request.headers, 'accept-language'),
      AcceptCharset: getHeader(request.headers, 'accept-charset'),
      Origin: getHeader(request.headers, 'origin'),
      XForwaredForIP: getHeader(request.headers, 'x-forwarded-for'),
      'X-Requested-With': getHeader(request.headers, 'x-requested-with'),
      Connection: getHeader(request.headers, 'connection'),
      Pragma: getHeader(request.headers, 'pragma'),
      CacheControl: getHeader(request.headers, 'cache-control'),
      CookiesLen: clientIdAndCookiesLength.cookiesLength,
      AuthorizationLen: getAuthorizationLength(request.headers),
      PostParamLen: getHeader(request.headers, 'content-length'),
      ClientID: clientIdAndCookiesLength.clientId,
    };

    /////////////////////////////////////
    // Prepares request to DataDome API
    let req = http
      .request(
        {
          agent: keepAliveAgent,
          host: datadomeHost,
          path: datadomePath,
          method: 'POST',
          headers: {
            'Content-Type': datadomeContentType,
            'User-Agent': datadomeUserAgent,
          },
          timeout: DATADOME_TIMEOUT,
        },
        (datadomeResponse) => {
          var bodyData = '';

          datadomeResponse
            .on('error', (e) => {
              console.log('Error at requesting DataDome API. Bypass DataDome. Error trace: ', e);
              console.log('Full DataDome response: ', datadomeResponse);
              callback(null, request);
            })
            .on('data', (chunk) => {
              bodyData += chunk;
            })
            .on('end', () => {
              if (!datadomeResponse.statusCode || !datadomeResponse.headers) {
                console.log(
                  'Fails to get status code and response headers from DataDome API response. Bypass DataDome. Full DataDome response: ',
                  datadomeResponse,
                );
                callback(null, request);

                return;
              }

              let datadomeHeader = datadomeResponse.headers['x-datadomeresponse'] || null;
              if (datadomeHeader === null) {
                console.log(
                  'Header "x-datadomeresponse" not found in DataDome API response. Bypass DataDome. Full DataDome response: ',
                  datadomeResponse,
                );
                callback(null, request);

                return;
              }
              if (datadomeResponse.statusCode != datadomeHeader) {
                console.log(
                  'Status code of DataDome API response does not match the status code returned in DataDome "x-datadomeresponse" header. Bypass DataDome. Status code: ' +
                    datadomeResponse.statusCode +
                    ' - "x-datadomeresponse" header:' +
                    datadomeHeader,
                );
                callback(null, request);

                return;
              }

              switch (datadomeResponse.statusCode) {
                case 200:
                  let updatedRequest = addDataDomeRequestHeaders(datadomeResponse.headers, request);
                  if (updatedRequest === null) {
                    callback(null, request);

                    return;
                  }

                  callback(null, updatedRequest);

                case 401:
                case 403:
                  // Builds response to send
                  let response = {
                    status: '403',
                    statusDescription: 'HTTP Forbidden',
                    body: bodyData,
                  };

                  response = addDataDomeHeaders(datadomeResponse.headers, response);
                  if (response === null) {
                    console.log(
                      'Failed to add DataDome headers to the response. Bypass DataDome. Headers: ',
                      datadomeResponse.headers,
                    );
                    callback(null, request);

                    return;
                  }

                  callback(null, response);
                  return;

                case 400:
                  callback(null, request);
                  return;

                case 301:
                case 302:
                  let redirectResponse = redirectRequest(datadomeResponse);
                  if (redirectResponse === null) {
                    console.log(
                      'An error occured while building the redirection. If the error remains, please contact us at support@datadome.co. Full DataDome response: ',
                      datadomeResponse,
                    );
                    callback(null, request);

                    return;
                  }

                  callback(null, redirectResponse);

                  return;

                default:
                  console.log(
                    datadomeResponse.statusCode +
                      ' response from DataDome API - An unexpected error occured from DataDome API. If the error remains, please contact us at support@datadome.co. Full response: ',
                    datadomeResponse,
                  );
                  callback(null, request);

                  return;
              }
            });
        },
      )
      .on('error', (e) => {
        console.log('Request to DataDome API failed. Bypass DataDome. Error trace: ', e);
        callback(null, request);
      });

    req.write(querystring.stringify(requestData));

    req.end();
  } catch (e) {
    console.log(
      'An error occured in the processing of the request. If the error remains, please contact us at support@datadome.co. Error trace: ',
      e,
    );
    callback(null, request);
  }
};

/*******************/
/* Tools           */
/*******************/

/**
 * Gets current microtimestamp.
 * @returns {number} The current microtimestamp.
 */
function getCurrentMicroTime() {
  const currentDate = new Date();

  return currentDate.getTime() * 1000;
}

/**
 * Gets list of headers in single string.
 * @param {Object} headers The list of headers from request.
 * @returns {string} The list of headers, concatened.
 */
function getHeadersList(headers) {
  var headersList = '';
  for (var headerKey in headers) {
    if (headersList !== '') {
      headersList += ',';
    }

    headersList += headerKey;
  }

  return headersList;
}

/**
 * Gets a specific header.
 * @param {Object} headers The list of headers from request.
 * @param {string} headerName The name of the header to get.
 * @returns {string} The value of the header.
 */
function getHeader(headers, headerName) {
  if (headers[headerName] && headers[headerName][0].value) {
    return headers[headerName][0].value;
  }

  return null;
}

/**
 * Gets DataDome client ID from cookies and length of all cookies combined.
 * @param {Object} headers The list of headers from request.
 * @returns {Object} An object containing the client ID and the length of values from cookie header (all cookie values combined).
 */
function getClientIdAndCookiesLength(headers) {
  var clientId = null;
  var cookiesLength = null;

  if (headers.cookie) {
    let cookiesLength = 0;
    headers.cookie.forEach(function (cookie) {
      cookiesLength += cookie.value.length;

      if (cookie.key.toLowerCase() == 'cookie') {
        let cookiesList = cookie.value.split(';');
        for (let cookieCount = 0; cookieCount < cookiesList.length; cookieCount++) {
          let cookieString = cookiesList[cookieCount].trim();
          let cookieParts = cookieString.split('=');
          if (cookieParts[0] === 'datadome') {
            clientId = cookieString.substring(cookieParts[0].length + 1);
          }
        }
      }
    });
  }

  return {
    clientId: clientId,
    cookiesLength: cookiesLength,
  };
}

/**
 * Gets length of Authorization header.
 * @param {Object} headers The list of headers from request.
 * @returns {number} The length of Authorization header.
 */
function getAuthorizationLength(headers) {
  var authorization = getHeader(headers, 'authorization');
  if (authorization === null) {
    return null;
  }

  return authorization.length;
}

/**
 * Gets the request protocol.
 * @param {Object} request The request.
 * @returns {string} The used protocol.
 */
function getRequestProtocol(request) {
  if (request.connection && request.connection.encrypted) {
    return 'https';
  }

  return 'http';
}

/**
 * Adds headers based on x-datadome-headers.
 * @param {Object} datadomeResponseHeaders The list of headers from the DataDome response.
 * @param {Object} response The response to enrich.
 * @returns {Object} The response with DataDome response headers.
 */
function addDataDomeHeaders(datadomeResponseHeaders, response) {
  let datadomeHeadersStr = datadomeResponseHeaders['x-datadome-headers'] || null;
  if (datadomeHeadersStr === null) {
    return null;
  }

  if (!response.headers) {
    response.headers = {};
  }

  let datadomeHeaders = datadomeHeadersStr.split(' ');
  for (let datadomeHeaderKey in datadomeHeaders) {
    let datadomeHeaderName = datadomeHeaders[datadomeHeaderKey].toLowerCase();
    let datadomeHeaderValue = datadomeResponseHeaders[datadomeHeaderName] || null;
    if (datadomeHeaderValue === null) {
      return null;
    }

    if (datadomeHeaderName == 'Set-Cookie') {
      if (!(datadomeHeaderName in response.headers)) {
        response.headers[datadomeHeaderName] = [];
      }

      response.headers[datadomeHeaderName] = response.headers[datadomeHeaderName].concat([
        {
          key: datadomeHeaderName,
          value: datadomeHeaderValue,
        },
      ]);
    } else {
      response.headers[datadomeHeaderName] = [
        {
          key: datadomeHeaderName,
          value: datadomeHeaderValue,
        },
      ];
    }
  }

  return response;
}

/**
 * Adds headers based on x-datadome-request-headers.
 * @param {Object} datadomeResponseHeaders The list of headers from the DataDome response.
 * @param {Object} request The request to enrich.
 * @returns {Object} The request with DataDome request headers.
 */
function addDataDomeRequestHeaders(datadomeResponseHeaders, request) {
  let datadomeHeadersStr = datadomeResponseHeaders['x-datadome-request-headers'] || null;
  if (datadomeHeadersStr === null) {
    return null;
  }

  if (!request.headers) {
    request.headers = {};
  }

  let requestHeadersLog = '';
  let datadomeHeaders = datadomeHeadersStr.split(' ');
  for (let datadomeHeaderKey in datadomeHeaders) {
    let datadomeHeaderName = datadomeHeaders[datadomeHeaderKey].toLowerCase();
    let datadomeHeaderValue = datadomeResponseHeaders[datadomeHeaderName] || null;
    if (datadomeHeaderValue === null) {
      return null;
    }

    if (requestHeadersLog !== '') {
      requestHeadersLog += ' - ';
    }

    request.headers[datadomeHeaderName] = [
      {
        key: datadomeHeaderName,
        value: datadomeHeaderValue,
      },
    ];
  }

  return request;
}

/**
 * Gets the redirect response, based on Location header.
 * @param {Object} response The original response.
 * @returns {Object} The redirect response.
 */
function redirectRequest(response) {
  if (!response.headers) {
    return null;
  }

  if (!response.headers['location']) {
    return null;
  }

  var datadomeHeaders = addDataDomeHeaders(response);
  if (datadomeHeaders === null) {
    return null;
  }

  const redirectResponse = {
    status: response.statusCode,
    statusDescription: response.statusDescription,
    headers: datadomeHeaders,
  };

  return redirectResponse;
}
