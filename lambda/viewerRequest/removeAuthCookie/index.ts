/**
 * This is for removing the auth cookie from the request for cached routes.
 */

import { CloudFrontRequestEvent } from 'aws-lambda';

export const handler = (event: CloudFrontRequestEvent): CloudFrontRequestEvent => {
  const locales = ['en-us', 'en-ca', 'en-gb'];
  const cachedRoutes = [
    '', // Home page
    'product', // Product pages
  ];
  const request = event.Records[0].cf.request;
  const headers = request.headers;
  const pathSegments = request.uri.split('/');

  let route = pathSegments.length > 1 ? pathSegments[1].toLowerCase() : '';

  if (locales.includes(route)) {
    // Path includes locale, get the next part of URI
    route = pathSegments.length > 2 ? pathSegments[2].toLowerCase() : '';
  }

  if (cachedRoutes.includes(route) && headers.cookie) {
    // Remove auth cookie from header
    const cookieHeader = headers.cookie[0].value;
    const cookieList = cookieHeader.split(/;\ /);
    let wasCookieRemoved = false;

    for (let m = 0; m < cookieList.length; m++) {
      if (cookieList[m].startsWith('auth=') || cookieList[m].startsWith('correlationId=')) {
        cookieList.splice(m, 1);
        wasCookieRemoved = true;
        m--; // Next loop is same m, to counteract array splice above
      }
    }

    if (wasCookieRemoved) {
      request.headers.cookie[0].value = cookieList.join('; ');
    }
  }

  return event;
};
