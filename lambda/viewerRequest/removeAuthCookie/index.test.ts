import { handler } from './index';
import { CloudFrontRequest, CloudFrontRequestEvent } from 'aws-lambda';

const cookie1 = 'cookie1=value1';
const cookie2 = 'cookie2=value2';
const authCookie = 'auth={"access_token":"abc123","role":"GUEST"}';
const correlationId = 'correlationId=abc123';
const cookiesWithAuth = `${cookie1}; ${authCookie}; ${correlationId}; ${cookie2}`;
const cookiesNoAuth = `${cookie1}; ${cookie2}`;

describe('removeAuthCookie handler', () => {
  const event = (request: Partial<CloudFrontRequest>): CloudFrontRequestEvent => ({
    Records: [
      {
        cf: {
          config: {
            distributionDomainName: 'd111111abcdef8.cloudfront.net',
            distributionId: 'EDFDVBD6EXAMPLE',
            eventType: 'viewer-request',
            requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
          },
          request: {
            clientIp: '203.0.113.178',
            headers: {
              host: [
                {
                  key: 'Host',
                  value: 'www.pokemoncenter.com',
                },
              ],
              'user-agent': [
                {
                  key: 'User-Agent',
                  value: 'curl/7.66.0',
                },
              ],
              accept: [
                {
                  key: 'accept',
                  value: '*/*',
                },
              ],
            },
            method: 'GET',
            uri: '/',
            querystring: '',
            ...request,
          },
        },
      },
    ],
  });

  test.each([
    '',
    '/',
    '/en-ca',
    '/en-ca/',
    '/product/123-45678/test-product',
    '/en-ca/product/123-45678/test-product',
  ])('remove auth and correlationId cookies on cached route %s', (uri) => {
    const returnValue = handler(
      event({
        headers: {
          cookie: [
            {
              key: 'Cookie',
              value: cookiesWithAuth,
            },
          ],
        },
        uri,
      }),
    );

    expect(returnValue.Records[0].cf.request.headers.cookie[0].value).toStrictEqual(cookiesNoAuth);
  });

  test.each(['/account', '/en-ca/account', '/checkout/payment', '/en-ca/checkout/payment'])(
    'do not remove auth or correlationId cookies on non-cached route %s',
    (uri) => {
      const returnValue = handler(
        event({
          headers: {
            cookie: [
              {
                key: 'Cookie',
                value: cookiesWithAuth,
              },
            ],
          },
          uri,
        }),
      );

      expect(returnValue.Records[0].cf.request.headers.cookie[0].value).toStrictEqual(
        cookiesWithAuth,
      );
    },
  );

  test.each([
    `${authCookie}; ${cookie1}; ${cookie2}`,
    `${cookie1}; ${authCookie}; ${cookie2}`,
    `${cookie1}; ${cookie2}; ${authCookie}`,
    `${correlationId}; ${cookie1}; ${cookie2}`,
    `${cookie1}; ${correlationId}; ${cookie2}`,
    `${cookie1}; ${cookie2}; ${correlationId}`,
    `${authCookie}; ${correlationId}; ${cookie1}; ${cookie2}`,
    `${cookie1}; ${authCookie}; ${correlationId}; ${cookie2}`,
    `${correlationId}; ${cookie1}; ${cookie2}; ${authCookie}`,
    `${cookie1}; ${cookie2}; ${correlationId}; ${authCookie}`,
  ])('remove auth cookie from cookie string %s', (cookie) => {
    const returnValue = handler(
      event({
        headers: {
          cookie: [
            {
              key: 'Cookie',
              value: cookie,
            },
          ],
        },
      }),
    );

    expect(returnValue.Records[0].cf.request.headers.cookie[0].value).toStrictEqual(
      `${cookie1}; ${cookie2}`,
    );
  });

  test('remove auth cookie when no other cookies are present', () => {
    const returnValue = handler(
      event({
        headers: {
          cookie: [
            {
              key: 'Cookie',
              value: authCookie,
            },
          ],
        },
      }),
    );

    expect(returnValue.Records[0].cf.request.headers.cookie[0].value).toStrictEqual('');
  });
});
