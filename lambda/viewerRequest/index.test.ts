import { handler } from './index';
import { removeEnUsFromPath } from './domainRedirect';
import {
  CloudFrontRequest,
  CloudFrontResponse,
  CloudFrontRequestEvent,
  Context,
  Callback,
} from 'aws-lambda';
import datadome from './datadome';

const expectRedirect = (
  status: string,
  location: string,
  done: jest.DoneCallback,
): Callback<CloudFrontRequest | CloudFrontResponse> => {
  const callback: Callback<CloudFrontRequest | CloudFrontResponse> = (_, res) => {
    try {
      if (res && 'status' in res) {
        expect(res.status).toBe(status);
        expect(res.headers.location[0].value).toBe(location);
      } else {
        done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
      }
      done();
    } catch (err) {
      done(err);
    }
  };
  return callback;
};

const checkQuerystringForTimestamp = (
  exists: boolean,
): Callback<CloudFrontRequest | CloudFrontResponse> => {
  const callback: Callback<CloudFrontRequest | CloudFrontResponse> = (_, res) => {
    if (exists) {
      if (res && 'querystring' in res) {
        expect(res?.querystring?.includes('ts=')).toBeTruthy();
      }
    } else {
      if (res && 'querystring' in res) {
        expect(res?.querystring?.includes('ts=')).toBeFalsy();
      }
    }
  };
  return callback;
};

const event = (request: Partial<CloudFrontRequest>): CloudFrontRequestEvent => ({
  Records: [
    {
      cf: {
        config: {
          distributionDomainName: 'd111111abcdef8.cloudfront.net',
          distributionId: 'EDFDVBD6EXAMPLE',
          eventType: 'viewer-request',
          requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
        },
        request: {
          clientIp: '203.0.113.178',
          headers: {
            host: [
              {
                key: 'Host',
                value: 'www.pokemoncenter.com',
              },
            ],
            'user-agent': [
              {
                key: 'User-Agent',
                value: 'curl/7.66.0',
              },
            ],
            accept: [
              {
                key: 'accept',
                value: '*/*',
              },
            ],
          },
          method: 'GET',
          uri: '/',
          querystring: '',
          ...request,
        },
      },
    },
  ],
});

describe('handler', () => {
  test('does not redirect homepage', (done) => {
    const callback: Callback<CloudFrontRequest | CloudFrontResponse> = (_, res) => {
      try {
        if (res && 'status' in res) {
          expect(res.status).toBe('403'); // 403 because DataDome doesn't work in a unit test
        } else {
          done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
        }
        done();
      } catch (err) {
        done(err);
      }
    };

    handler(event({}), {} as Context, callback);
  });

  test('redirects pokemoncenter.com', (done) => {
    jest.spyOn(datadome, 'handler').mockImplementation(() => {
      // do nothing. This is a mock out for data dome
    });
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.com',
            },
          ],
        },
        uri: '',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/', done),
    );
  });

  test('redirects prod.ui.pokemoncenter.com', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'prod.ui.pokemoncenter.com',
            },
          ],
        },
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/', done),
    );
  });

  test('preserves querystring when redirecting for domain name', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.com',
            },
          ],
        },
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/?foo=bar', done),
    );
  });

  test('redirects prod.ui.pokemoncenter.com/tpci-ecommweb-api/', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'prod.ui.pokemoncenter.com',
            },
          ],
        },
        uri: '/tpci-ecommweb-api/search',
        querystring:
          'q=S0101-0000-0000&_br_uid_2=&ref_url=&rows=30&start=0&url=dev.ui.pokemoncenter.com&fl=pid,title,brand,price,sale_price,promotions,thumb_image,sku_thumb_images,sku_swatch_images,sku_color_group,url,price_range,sale_price_range,description,best_seller,launch_date,sale_price,PRF&sort=&search_type=category',
      }),
      {} as Context,
      expectRedirect(
        '301',
        'https://www.pokemoncenter.com/tpci-ecommweb-api/search?q=S0101-0000-0000&_br_uid_2=&ref_url=&rows=30&start=0&url=dev.ui.pokemoncenter.com&fl=pid,title,brand,price,sale_price,promotions,thumb_image,sku_thumb_images,sku_swatch_images,sku_color_group,url,price_range,sale_price_range,description,best_seller,launch_date,sale_price,PRF&sort=&search_type=category',
        done,
      ),
    );
  });

  test('preserves uri and querystring when redirecting for domain name', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.com',
            },
          ],
        },
        uri: '/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/foo/bar?foo=bar', done),
    );
  });
  test('Truncate /en-us from the uri - www.pokemoncenter.com', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'www.pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/', done),
    );
  });

  test('Truncate /en-us from the uri and redirect with uri and querystring - www.pokemoncenter.com', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'www.pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/foo/bar?foo=bar', done),
    );
  });
  test('Truncate /en-us from the uri and redirect with the rest of the url - uri and querystring', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/foo/bar?foo=bar', done),
    );
  });
  test('Truncate /en-us from the uri and redirect with the rest of the url - querystring', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/?foo=bar', done),
    );
  });

  test('Truncate /en-us from the uri and redirect with the rest of the url (*.ui.pokemoncenter.com) - uri and querystring', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://dev.ui.pokemoncenter.com/foo/bar?foo=bar', done),
    );
  });

  test('Truncate /en-us from the uri and redirect with the rest of the url (*.ui.pokemoncenter.com) - querystring', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
        },
        uri: '/en-us',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://dev.ui.pokemoncenter.com/?foo=bar', done),
    );
  });
  test('Remove /en-us, from the path if uri prepends /en-us', () => {
    expect(removeEnUsFromPath('/en-us')).toStrictEqual('/');
    expect(removeEnUsFromPath('')).toStrictEqual('/');
    expect(removeEnUsFromPath('/en-us/foo')).toStrictEqual('/foo');
    expect(removeEnUsFromPath('/en-us/foo/en-us')).toStrictEqual('/foo/en-us');
  });
  test('redirects pokemoncenter.ca', (done) => {
    jest.spyOn(datadome, 'handler').mockImplementation(() => {
      // do nothing. This is a mock out for data dome
    });
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.ca',
            },
          ],
        },
        uri: '',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/en-ca/', done),
    );
  });

  test('redirects dev.ui.pokemoncenter.ca', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.ca',
            },
          ],
        },
      }),
      {} as Context,
      expectRedirect('301', 'https://dev.ui.pokemoncenter.com/en-ca/', done),
    );
  });

  test('redirects stage.ui.pokemoncenter.ca', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'stage.ui.pokemoncenter.ca',
            },
          ],
        },
      }),
      {} as Context,
      expectRedirect('301', 'https://stage.ui.pokemoncenter.com/en-ca/', done),
    );
  });

  test('redirects ppd.ui.pokemoncenter.ca', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'ppd.ui.pokemoncenter.ca',
            },
          ],
        },
      }),
      {} as Context,
      expectRedirect('301', 'https://ppd.ui.pokemoncenter.com/en-ca/', done),
    );
  });

  test('preserves querystring when redirecting for CA domain name', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.ca',
            },
          ],
        },
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/en-ca/?foo=bar', done),
    );
  });

  test('preserves uri and querystring when redirecting for CA domain name', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'pokemoncenter.ca',
            },
          ],
        },
        uri: '/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://www.pokemoncenter.com/en-ca/foo/bar?foo=bar', done),
    );
  });

  test('preserves uri and querystring when redirecting for *.ui.pokemoncenter.ca', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.ca',
            },
          ],
        },
        uri: '/foo/bar',
        querystring: 'foo=bar',
      }),
      {} as Context,
      expectRedirect('301', 'https://dev.ui.pokemoncenter.com/en-ca/foo/bar?foo=bar', done),
    );
  });

  test('redirects for predefined urls', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
        },
        uri: '/red',
      }),
      {} as Context,
      expectRedirect('301', 'https://dev.ui.pokemoncenter.com/category/nendoroid', done),
    );
  });

  test('redirects for predefined urls for absolute paths', (done) => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
        },
        uri: '/redeem',
      }),
      {} as Context,
      expectRedirect(
        '301',
        'https://support.pokemoncenter.com/hc/en-us/articles/360001395843-How-do-I-redeem-a-promo-code-',
        done,
      ),
    );
  });
});

describe('timestamp query string parameter', () => {
  test('does not add ts if referer is not from Bloomreach', () => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
          referer: [
            {
              key: 'Referer',
              value: 'https://test.domain/',
            },
          ],
        },
        uri: '/about-our-plush',
      }),
      {} as Context,
      checkQuerystringForTimestamp(false),
    );
  });

  test('appends ts to existing querystring if referer is from Bloomreach', () => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
          referer: [
            {
              key: 'Referer',
              value: 'https://dev.tpci1.bloomreach.cloud/',
            },
          ],
        },
        querystring: 'key=value',
        uri: '/about-our-plush',
      }),
      {} as Context,
      checkQuerystringForTimestamp(true),
    );
  });

  test('does not append ts to existing querystring if referer is not from Bloomreach', () => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
          referer: [
            {
              key: 'Referer',
              value: 'https://dev-tpci1.mydomain.io/',
            },
          ],
        },
        querystring: 'key=value',
        uri: '/about-our-plush',
      }),
      {} as Context,
      checkQuerystringForTimestamp(false),
    );
  });

  test('adds ts querystring for bloomreach.cloud domain', () => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
          referer: [
            {
              key: 'Referer',
              value: 'https://dev.tpci1.bloomreach.cloud/',
            },
          ],
        },
        uri: '/about-our-plush',
      }),
      {} as Context,
      checkQuerystringForTimestamp(true),
    );
  });

  test('adds ts querystring for cms.pokemoncenter.com subdomain', () => {
    handler(
      event({
        headers: {
          host: [
            {
              key: 'Host',
              value: 'dev.ui.pokemoncenter.com',
            },
          ],
          referer: [
            {
              key: 'Referer',
              value: 'https://dev.cms.pokemoncenter.com/',
            },
          ],
        },
        uri: '/about-our-plush',
      }),
      {} as Context,
      checkQuerystringForTimestamp(true),
    );
  });
});
