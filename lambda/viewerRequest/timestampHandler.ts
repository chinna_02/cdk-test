/**
 * Adds a timestamp "ts" querystring parameter to break caching when front-end is running within the Bloomreach CMS
 */
import url = require('url');
import { CloudFrontRequestEvent } from 'aws-lambda';

const cmsDomainList = ['bloomreach.cloud', 'cms.pokemoncenter.com'];

export const handler = (event: CloudFrontRequestEvent): CloudFrontRequestEvent => {
  const request = event.Records[0].cf.request;

  if (request.headers['referer']) {
    const refererUrl = request.headers['referer'][0]['value'];
    const { hostname } = new url.URL(refererUrl);

    const domainMatches = cmsDomainList.filter((element) => hostname.includes(element));
    if (domainMatches.length > 0) {
      const now = Date.now();

      if (request.querystring) {
        request.querystring += `&ts=${now}`;
      } else {
        request.querystring = `ts=${now}`;
      }
    }
  }

  return event;
};
