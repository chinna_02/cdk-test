import { CloudFrontRequestEvent, CloudFrontRequest, CloudFrontResponse, Handler } from 'aws-lambda';
import datadome from './datadome';
import * as domainRedirect from './domainRedirect';
import * as timestamp from './timestampHandler';
import * as removeAuthCookie from './removeAuthCookie';

export const handler: Handler<CloudFrontRequestEvent, CloudFrontRequest | CloudFrontResponse> = (
  event,
  context,
  callback,
) => {
  const cf = event.Records[0].cf;
  if (cf.request.headers['x-forwarded-host'] && cf.request.headers['x-forwarded-server']) {
    delete cf.request.headers['x-forwarded-host'];
    delete cf.request.headers['x-forwarded-server'];
  }

  try {
    event = timestamp.handler(event);
    event = removeAuthCookie.handler(event);
    domainRedirect.handler(event, context, callback);
  } catch (err) {
    // Don't let a bug in domainRedirect prevent datadome from running.
    console.error(err);
  }

  datadome.handler(event, context, callback);
};
