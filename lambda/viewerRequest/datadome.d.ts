import { CloudFrontRequestEvent, CloudFrontRequest, CloudFrontResponse, Handler } from 'aws-lambda';
declare const handler: Handler<CloudFrontRequestEvent, CloudFrontRequest | CloudFrontResponse>;

declare module 'datadome' {
  export = handler;
}
