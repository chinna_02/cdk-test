import { CloudFrontResponseEvent, CloudFrontResponse, Handler } from 'aws-lambda';

export const handler: Handler<CloudFrontResponseEvent, CloudFrontResponse> = (
  event,
  context,
  callback,
) => {
  const domain = event.Records[0].cf.request.headers.host[0].value;
  const response = event.Records[0].cf.response;
  const headers = response.headers;

  if (response.status != '200') {
    // Note: We can't completely create a new set of headers even for a redirect
    // because Cloudfront fails painfully if you set (or clear) a "read-only" header.
    // For Origin Response triggers, that includes leaving "Transfer-Encoding" alone.
    // https://github.com/awsdocs/amazon-cloudfront-developer-guide/blob/master/doc_source/lambda-requirements-limits.md#read-only-headers

    headers['location'] = [
      {
        key: 'Location',
        value: `https://${domain}/maintenance/index.html`,
      },
    ];
    response.status = '307';
    response.statusDescription = 'Temporary Redirect';

    callback(null, response);
  }
};
