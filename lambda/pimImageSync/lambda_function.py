import json
import boto3
import datetime
import csv
import os

s3 = boto3.client('s3')
s3_client = boto3.client('s3control', region_name='us-east-1')

def create_manifest(message_dict, source_bucket_name):
        with open( '/tmp/manifests.csv', 'w' ) as local_manifest:
            writer = csv.writer(local_manifest)
            for path in message_dict[ 'paths' ]:
                source_path = 'DAMRoot' + '/'+ path
                print( 'Image path: ' + source_path )
                writer.writerow( [source_bucket_name, source_path] )          

def upload_manifest(inventory_bucket_name, manifest_prefix, file_prefix):      
        with open( '/tmp/manifests.csv', 'rb' ) as manifest:   
            s3.upload_fileobj( manifest, inventory_bucket_name , manifest_prefix + '/' + file_prefix +'.csv')

def get_latest_manifest(inventory_bucket_name, manifest_prefix):
    get_last_modified = lambda obj: int(obj['LastModified'].strftime('%s'))
    list_objects = s3.list_objects_v2(Bucket=inventory_bucket_name, Prefix=manifest_prefix)
    content = list_objects['Contents']  
    latest_manifest = [content['Key'] for content in sorted(content, key=get_last_modified)][-1]
    return latest_manifest

def create_s3batch(aws_account_id, destination_bucket_arn, inventory_bucket_arn, manifest_arn, manifest_etag, role_arn):
     response = s3_client.create_job(
        AccountId = aws_account_id,
        ConfirmationRequired = False,
        Operation = {
            'S3PutObjectCopy': {
                'TargetResource': destination_bucket_arn
            }
        },
        Report = {
            'Bucket': inventory_bucket_arn,
            'Format': 'Report_CSV_20180820',
            'Enabled': True,
            'Prefix': 'reports',
            'ReportScope': 'AllTasks'
        },  
    
        Manifest = {
            'Spec': {
                'Format': 'S3BatchOperations_CSV_20180820',
                'Fields': [ 'Bucket', 'Key' ]    
            },
            'Location': {
                'ObjectArn': manifest_arn,
                'ETag': manifest_etag
            }
        },
        Description = 'Pim Image Sync Batch operation',
        Priority = 100,
        RoleArn = role_arn,
        Tags = [
             {
                'Key': 'string',
                'Value': 'string'
            },
        ]
    )


def lambda_handler(event, context):
    message = event['Records'][0]['Sns']['Message']
    message_dict = json.loads(message)
    
    aws_account_id = boto3.client('sts').get_caller_identity().get('Account')
    role_arn = os.environ.get('S3BATCH_ROLE')
    source_bucket_name = os.environ.get('SOURCE_BUCKET_NAME') 
    destination_bucket_name = os.environ.get('DESTINATION_BUCKET_NAME')
    inventory_bucket_name = os.environ.get('INVENTORY_BUCKET_NAME')
    destination_bucket_arn = 'arn:aws:s3:::' + destination_bucket_name
    inventory_bucket_arn = 'arn:aws:s3:::' + inventory_bucket_name
    
    now = datetime.datetime.now()
    current_time = now.strftime("%H%M%S")
    s3_key = now.strftime("%Y-%m-%d ")
    manifest_prefix= 'manifests/' + s3_key.strip()
    file_prefix = 'S3BatchOperations_CSV_' + current_time

    create_manifest(message_dict, source_bucket_name)
    upload_manifest(inventory_bucket_name, manifest_prefix, file_prefix)

    manifest_file = get_latest_manifest(inventory_bucket_name, manifest_prefix)
    manifest_arn = inventory_bucket_arn + '/' + manifest_file
    manifest_attributes = s3.get_object(Bucket=inventory_bucket_name, Key=manifest_file)

    print('Manifest file ARN is: ' + manifest_arn)
    manifest_etag = manifest_attributes.get('ETag')
    print('Manifest file ETag is:' + manifest_etag ) 

    create_s3batch(aws_account_id, destination_bucket_arn, inventory_bucket_arn, manifest_arn, manifest_etag, role_arn)