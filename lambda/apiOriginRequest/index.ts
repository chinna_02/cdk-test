import { CloudFrontRequestEvent, CloudFrontRequest, CloudFrontResponse, Handler } from 'aws-lambda';

export const removeUriPrefix = (location: string): string => {
  return location
    .replace(/^\/tpci-fluent-webhook\//, '/')
    .replace(/^\/tpci-transaction-email-api\//, '/')
    .replace(/^\/tpci-order-status\//, '/')
    .replace(/^\/tpci-ecommweb-api\//, '/')
    .replace(/^\/tpci-inventory-sync\//, '/');
};
export const euApiGatewayIds: { [key: string]: string } = {
  stage: '1qlx4l21a3',
  ppd: 't9kucs1d04',
  prod: 'ia9pokyo96',
};

export const handler: Handler<CloudFrontRequestEvent, CloudFrontRequest | CloudFrontResponse> = (
  event,
  _context,
  callback,
) => {
  const { cf } = event.Records[0];

  const stage = {
    EBIERFXQB8X99: 'dev',
    EA60MAV3RRPA3: 'stage',
    E30FS25EQONK9G: 'ppd',
    E3L61D6I4WJNSK: 'prod',
    E23T1VLARLNKAD: 'hfe',
  }[cf.config.distributionId];

  const euApiGatewayId = euApiGatewayIds[stage ?? ''];

  const request = cf.request;
  const customOrigin = request.origin?.custom;

  if (
    euApiGatewayId &&
    customOrigin &&
    request.headers['x-store-scope'] &&
    request.headers['x-store-scope'].some(({ value }) =>
      [
        'pokemon-uk',
        // Other EU storefronts can be added below
      ].includes(value),
    )
  ) {
    request.origin = {
      custom: {
        ...customOrigin,
        domainName: `${euApiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
        path: `/${stage}-eu`,
      },
    };
    request.headers.host = [
      {
        key: 'Host',
        value: `${euApiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
      },
    ];
  }

  if (customOrigin && !customOrigin.domainName.startsWith('customdomain')) {
    request.uri = removeUriPrefix(request.uri);
  }

  callback(null, request);
};
