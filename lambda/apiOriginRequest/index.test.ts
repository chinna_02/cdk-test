import { euApiGatewayIds, handler, removeUriPrefix } from './index';
import {
  CloudFrontRequest,
  CloudFrontResponse,
  CloudFrontRequestEvent,
  Context,
  Callback,
  CloudFrontOrigin,
  CloudFrontCustomOrigin,
  CloudFrontEvent,
} from 'aws-lambda';
import { config } from 'process';

describe('removeUriPrefix', () => {
  test('remove /tpci-fluent-webhook', () => {
    expect(removeUriPrefix('/tpci-fluent-webhook/abc')).toBe('/abc');
  });
  test('remove /tpci-order-status', () => {
    expect(removeUriPrefix('/tpci-order-status/hij')).toBe('/hij');
  });
  test('remove /tpci-transaction-email-api', () => {
    expect(removeUriPrefix('/tpci-transaction-email-api/xyz')).toBe('/xyz');
  });
  test('remove /tpci-ecommweb-api', () => {
    expect(removeUriPrefix('/tpci-ecommweb-api/foo')).toBe('/foo');
  });
  test('remove /tpci-ecommweb-api with querystring untouched', () => {
    expect(removeUriPrefix('/tpci-ecommweb-api/bar?xyz=1')).toBe('/bar?xyz=1');
  });
  test('unmatched is untouched', () => {
    expect(removeUriPrefix('/product/701-06560/gigantamax-pikachu-poke-plush-31-in')).toBe(
      '/product/701-06560/gigantamax-pikachu-poke-plush-31-in',
    );
  });
});

const makeEvent = (
  config: Partial<CloudFrontEvent['config']> = {},
  request: Partial<CloudFrontRequest> = {},
  custom: Partial<CloudFrontOrigin['custom']> = {},
): CloudFrontRequestEvent => ({
  Records: [
    {
      cf: {
        config: {
          distributionDomainName: 'd1wufn1l2yxm2i.cloudfront.net',
          distributionId: 'EBIERFXQB8X99',
          eventType: 'origin-request',
          requestId: 'BA2BCZkvJctwe01D1xGvrjcW_nxpk8BVp4ftWLzrh4ZEzBuVYKz3MQ==',
          ...config,
        },
        request: {
          clientIp: '6.7.8.9',
          headers: {
            'x-forwarded-for': [{ key: 'X-Forwarded-For', value: '6.7.8.9' }],
            'user-agent': [{ key: 'User-Agent', value: 'Amazon CloudFront' }],
            via: [
              {
                key: 'Via',
                value: '1.1 574d7c6467f1dfa45947cfb5d366e244.cloudfront.net (CloudFront)',
              },
            ],
            'sec-ch-ua': [{ key: 'sec-ch-ua', value: '' }],
            'sec-ch-ua-mobile': [{ key: 'sec-ch-ua-mobile', value: '?0' }],
            'x-store-scope': [{ key: 'x-store-scope', value: 'pokemon' }],
            'sec-fetch-site': [{ key: 'sec-fetch-site', value: 'same-origin' }],
            'sec-fetch-mode': [{ key: 'sec-fetch-mode', value: 'cors' }],
            'sec-fetch-dest': [{ key: 'sec-fetch-dest', value: 'empty' }],
            'accept-encoding': [{ key: 'Accept-Encoding', value: 'gzip' }],
            host: [{ key: 'Host', value: 'customdomain.dev.api.pokemoncenter.com' }],
          },
          method: 'GET',
          origin: {
            custom: {
              customHeaders: {
                'x-api-key': [
                  { key: 'x-api-key', value: '09RArg3n1032n3h45mm132x123rJ746O41oiHV8L' },
                ],
              },
              domainName: 'customdomain.dev.api.pokemoncenter.com',
              keepaliveTimeout: 5,
              path: '',
              port: 443,
              protocol: 'https',
              readTimeout: 30,
              sslProtocols: ['TLSv1.2'],
              ...custom,
            },
          },
          querystring: '',
          uri: '/tpci-ecommweb-api/health',
          ...request,
        },
      },
    },
  ],
});

/* a common abstract to test handler() in all the subsequent tests */

describe('handler', () => {
  const cloudfrontIds = {
    dev: 'EBIERFXQB8X99',
    stage: 'EA60MAV3RRPA3',
    ppd: 'E30FS25EQONK9G',
    prod: 'E3L61D6I4WJNSK',
    hfe: 'E23T1VLARLNKAD',
  };

  describe('tpci-ecommweb-api', () => {
    for (const [stage, distributionId] of Object.entries(cloudfrontIds)) {
      test(`customdomain.${stage}.api.pokemoncenter.com should untouched`, () => {
        const event = makeEvent();
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        event.Records[0].cf.request.origin!.custom!.domainName = `customdomain.${stage}.api.pokemoncenter.com`;
        handler(event, {} as Context, (_, res) => {
          const req = res as CloudFrontRequest;
          expect(req.origin?.custom?.domainName).toBe(
            `customdomain.${stage}.api.pokemoncenter.com`,
          );
          expect(req.uri).toBe('/tpci-ecommweb-api/health');
        });
      });

      test(`customdomain.${stage}.api.pokemoncenter.com should untouched with pokemon-uk`, () => {
        const event = makeEvent();
        event.Records[0].cf.request.headers['x-store-scope'][0] = {
          key: 'x-store-scope',
          value: 'pokemon-uk',
        };
        // eslint-disable-next-line @typescript-eslint/no-non-null-assertion
        event.Records[0].cf.request.origin!.custom!.domainName = `customdomain.${stage}.api.pokemoncenter.com`;
        handler(event, {} as Context, (_, res) => {
          const req = res as CloudFrontRequest;
          expect(req.origin?.custom?.domainName).toBe(
            `customdomain.${stage}.api.pokemoncenter.com`,
          );
          expect(req.uri).toBe('/tpci-ecommweb-api/health');
        });
      });

      test(`api gateways should have only have prefix stripped for ${stage} US`, () => {
        const domainName = 'c609eitot6.execute-api.us-west-2.amazonaws.com';
        const event = makeEvent({ distributionId }, {}, { domainName, path: `/${stage}` });
        handler(event, {} as Context, (_, res) => {
          const req = res as CloudFrontRequest;
          expect(req.origin?.custom?.domainName).toBe(domainName);
          expect(req.uri).toBe('/health');
        });
      });
    }

    for (const stage of ['dev', 'hfe']) {
      test(`${stage} should not go to EU`, () => {
        const domainName = '2pgyrh4ao1.execute-api.us-west-2.amazonaws.com';
        const event = makeEvent(
          { distributionId: cloudfrontIds[stage as keyof typeof cloudfrontIds] },
          {},
          { domainName, path: `/dev` },
        );
        const request = event.Records[0].cf.request;
        request.headers['x-store-scope'][0] = {
          key: 'x-store-scope',
          value: 'pokemon-uk',
        };
        request.uri = '/tpci-ecommweb-api/search';
        handler(event, {} as Context, (_, res) => {
          const req = res as CloudFrontRequest;
          expect(req.origin?.custom?.domainName).toBe(domainName);
          expect(req.uri).toBe('/search');
        });
      });
    }

    // TODO - Reenble when EU is ready
    /* for (const stage of ['stage' /* TODO 'ppd', 'prod' ]) {
      test(`rewrite api gateways to EU for ${stage}`, () => {
        const event = makeEvent(
          {
            distributionId: cloudfrontIds[stage as keyof typeof cloudfrontIds],
          },
          {},
          {
            domainName: 'c609eitot6.execute-api.us-west-2.amazonaws.com',
            path: '/stage',
          },
        );
        const request = event.Records[0].cf.request;
        request.headers['x-store-scope'][0] = {
          key: 'x-store-scope',
          value: 'pokemon-uk',
        };
        request.uri = '/tpci-ecommweb-api/search';
        const euApiGatewayId = euApiGatewayIds[stage];
        handler(event, {} as Context, (_, res) => {
          const req = res as CloudFrontRequest;
          expect(req.origin?.custom?.domainName).toBe(
            `${euApiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
          ),
            expect(req.uri).toBe('/search');
        });
      });
    }*/
  });
});

const expectForwarded = (
  location: string,
  done: jest.DoneCallback,
): Callback<CloudFrontRequest | CloudFrontResponse> => {
  const callback: Callback<CloudFrontRequest | CloudFrontResponse> = (_, res) => {
    try {
      if (res && 'uri' in res) {
        expect(res.uri).toBe(location);
      } else {
        done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
      }
      done();
    } catch (err) {
      done(err);
    }
  };
  return callback;
};

describe('strip URI prefix in handler', () => {
  const event = (request: Partial<CloudFrontRequest>): CloudFrontRequestEvent => ({
    Records: [
      {
        cf: {
          config: {
            distributionDomainName: 'd111111abcdef8.cloudfront.net',
            distributionId: 'EDFDVBD6EXAMPLE',
            eventType: 'origin-request',
            requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
          },
          request: {
            clientIp: '203.0.113.178',
            headers: {
              'x-forwarded-for': [
                {
                  key: 'X-Forwarded-For',
                  value: '203.0.113.178',
                },
              ],
              'user-agent': [
                {
                  key: 'User-Agent',
                  value: 'Amazon CloudFront',
                },
              ],
              via: [
                {
                  key: 'Via',
                  value: '2.0 2afae0d44e2540f472c0635ab62c232b.cloudfront.net (CloudFront)',
                },
              ],
              host: [
                {
                  key: 'Host',
                  value: 'example.org',
                },
              ],
              'cache-control': [
                {
                  key: 'Cache-Control',
                  value: 'no-cache, cf-no-cache',
                },
              ],
            },
            method: 'GET',
            origin: {
              custom: {
                customHeaders: {},
                domainName: 'example.org',
                keepaliveTimeout: 5,
                path: '',
                port: 443,
                protocol: 'https',
                readTimeout: 30,
                sslProtocols: ['TLSv1', 'TLSv1.1', 'TLSv1.2'],
              },
            },
            querystring: '',
            uri: '/',
            ...request,
          },
        },
      },
    ],
  });

  test('Return URI prefix /tpci-fluent-webhook/ stripped ', (done) => {
    const returnValue = handler(
      event({
        uri: '/tpci-fluent-webhook/abc',
      }),
      {} as Context,
      expectForwarded('/abc', done),
    );
  });
  test('Return URI prefix /tpci-order-status/ stripped ', (done) => {
    const returnValue = handler(
      event({
        uri: '/tpci-order-status/abc',
      }),
      {} as Context,
      expectForwarded('/abc', done),
    );
  });
  test('Return URI prefix /tpci-transaction-email-api/ stripped ', (done) => {
    const returnValue = handler(
      event({
        uri: '/tpci-transaction-email-api/abc',
      }),
      {} as Context,
      expectForwarded('/abc', done),
    );
  });
});
