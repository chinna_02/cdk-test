import { handler, checkURI } from './index';
import {
  CloudFrontRequest,
  CloudFrontResponse,
  CloudFrontResponseEvent,
  Context,
  Callback,
} from 'aws-lambda';

describe('checkURI', () => {
  test('URI - /, return false', () => {
    expect(checkURI('/')).toBe(false);
  });
  test('URI - /static, return false', () => {
    expect(checkURI('/static')).toBe(false);
  });
  test('URI - /tpci-ecommweb-api/order, return true', () => {
    expect(checkURI('/tpci-ecommweb-api/order')).toBe(true);
  });

  test('URI - /en-ca/tpci-ecommweb-api/order, return true', () => {
    expect(checkURI('/en-ca/tpci-ecommweb-api/order')).toBe(true);
  });

  test('URI - /tpci-ecommweb-api/profile, return true', () => {
    expect(checkURI('/tpci-ecommweb-api/profile')).toBe(true);
  });

  test('URI - /en-ca/tpci-ecommweb-api/profile, return true', () => {
    expect(checkURI('/en-ca/tpci-ecommweb-api/profile')).toBe(true);
  });

  test('URI - /order, return true', () => {
    expect(checkURI('/order')).toBe(true);
  });

  test('URI - /en-ca/order, return true', () => {
    expect(checkURI('/en-ca/order')).toBe(true);
  });

  test('URI - /account, return true', () => {
    expect(checkURI('/account')).toBe(true);
  });

  test('URI - /en-ca/account, return true', () => {
    expect(checkURI('/en-ca/account')).toBe(true);
  });

  test('URI - /cart, return true', () => {
    expect(checkURI('/cart')).toBe(true);
  });

  test('URI - /en-ca/cart, return true', () => {
    expect(checkURI('/en-ca/cart')).toBe(true);
  });

  test('URI - /checkout, return true', () => {
    expect(checkURI('/checkout')).toBe(true);
  });

  test('URI - /en-ca/checkout, return true', () => {
    expect(checkURI('/en-ca/checkout')).toBe(true);
  });

  test('URI - /returns, return true', () => {
    expect(checkURI('/returns')).toBe(true);
  });

  test('URI - /en-ca/returns, return true', () => {
    expect(checkURI('/en-ca/returns')).toBe(true);
  });
  test('URI - /forgot-password, return true', () => {
    expect(checkURI('/forgot-password')).toBe(true);
  });

  test('URI - /en-ca/forgot-password, return true', () => {
    expect(checkURI('/en-ca/forgot-password')).toBe(true);
  });
});

//check cache header value
const expectNoCache = (done: jest.DoneCallback): Callback<CloudFrontResponse> => {
  const callback: Callback<CloudFrontResponse> = (_, res) => {
    try {
      if (res && 'status' in res) {
        expect(res.headers['cache-control']).toStrictEqual([
          { key: 'Cache-Control', value: 'no-store' },
        ]);
      } else {
        done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
      }
      done();
    } catch (err) {
      done(err);
    }
  };
  return callback;
};

//check for deleted cache header
const expectUndefinedCacheHeader = (done: jest.DoneCallback): Callback<CloudFrontResponse> => {
  const callback: Callback<CloudFrontResponse> = (_, res) => {
    try {
      if (res && 'status' in res) {
        expect(res.headers['cache-control']).toBeUndefined;
      } else {
        done(`Expected a response: ${JSON.stringify(res, null, 2)}`);
      }
      done();
    } catch (err) {
      done(err);
    }
  };
  return callback;
};

describe('handler', () => {
  const event1 = (request: Partial<CloudFrontRequest>): CloudFrontResponseEvent => ({
    Records: [
      {
        cf: {
          config: {
            distributionDomainName: 'd111111abcdef8.cloudfront.net',
            distributionId: 'EDFDVBD6EXAMPLE',
            eventType: 'viewer-response',
            requestId: '4TyzHTaYWb1GX1qTfsHhEqV6HUDd_BzoBZnwfnvQc_1oF26ClkoUSEQ==',
          },
          request: {
            clientIp: '203.0.113.178',
            headers: {
              host: [
                {
                  key: 'Host',
                  value: 'd111111abcdef8.cloudfront.net',
                },
              ],
              'user-agent': [
                {
                  key: 'User-Agent',
                  value: 'curl/7.66.0',
                },
              ],
              accept: [
                {
                  key: 'accept',
                  value: '*/*',
                },
              ],
            },
            method: 'GET',
            querystring: '',
            uri: '/',
            ...request,
          },
          response: {
            headers: {
              'access-control-allow-credentials': [
                {
                  key: 'Access-Control-Allow-Credentials',
                  value: 'true',
                },
              ],
              'access-control-allow-origin': [
                {
                  key: 'Access-Control-Allow-Origin',
                  value: '*',
                },
              ],
              date: [
                {
                  key: 'Date',
                  value: 'Mon, 13 Jan 2020 20:14:56 GMT',
                },
              ],
              'referrer-policy': [
                {
                  key: 'Referrer-Policy',
                  value: 'no-referrer-when-downgrade',
                },
              ],
              server: [
                {
                  key: 'Server',
                  value: 'ExampleCustomOriginServer',
                },
              ],
              'x-content-type-options': [
                {
                  key: 'X-Content-Type-Options',
                  value: 'nosniff',
                },
              ],
              'x-frame-options': [
                {
                  key: 'X-Frame-Options',
                  value: 'DENY',
                },
              ],
              'x-xss-protection': [
                {
                  key: 'X-XSS-Protection',
                  value: '1; mode=block',
                },
              ],
              age: [
                {
                  key: 'Age',
                  value: '2402',
                },
              ],
              'content-type': [
                {
                  key: 'Content-Type',
                  value: 'text/html; charset=utf-8',
                },
              ],
              'content-length': [
                {
                  key: 'Content-Length',
                  value: '9593',
                },
              ],
            },
            status: '200',
            statusDescription: 'OK',
          },
        },
      },
    ],
  });

  test('for path=/, cache-control:undefined', (done) => {
    handler(
      event1({
        uri: '/',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });

  test('for path /cart, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/cart',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/cart, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/cart',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /order, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/order',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/order, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/order',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /account, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/account',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/account, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/account',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /checkout, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/checkout',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/checkout, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/checkout',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /returns, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/returns',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/returns, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/returns',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /forgot-password, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/forgot-password',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/forgot-password, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/forgot-password',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /tpci-ecommweb-api/order, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/tpci-ecommweb-api/order',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/tpci-ecommweb-api/order, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/tpci-ecommweb-api/order',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /tpci-ecommweb-api/profile, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/tpci-ecommweb-api/profile',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /en-ca/tpci-ecommweb-api/profile, cache-control:no-store', (done) => {
    handler(
      event1({
        uri: '/en-ca/tpci-ecommweb-api/profile',
      }),
      {} as Context,
      expectNoCache(done),
    );
  });

  test('for path /tpci-ecommweb-api/search, cache-control header:undefined', (done) => {
    handler(
      event1({
        uri: '/tpci-ecommweb-api/search',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });
  test('for path /en-ca/tpci-ecommweb-api/search, cache-control header:undefined', (done) => {
    handler(
      event1({
        uri: '/tpci-ecommweb-api/search',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });
  test('for path /tpci-ecommweb-api/suggest, cache-control header:undefined', (done) => {
    handler(
      event1({
        uri: '/tpci-ecommweb-api/suggest',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });
  test('for path /en-ca/tpci-ecommweb-api/suggest, cache-control header:undefined', (done) => {
    handler(
      event1({
        uri: '/en-ca/tpci-ecommweb-api/suggest',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });
  test('for path /static, cache-control header:undefined', (done) => {
    handler(
      event1({
        uri: '/static',
      }),
      {} as Context,
      expectUndefinedCacheHeader(done),
    );
  });
});
