import { ScheduledEvent, Handler } from 'aws-lambda';
import CF from 'aws-sdk/clients/cloudfront';

export const handler: Handler<ScheduledEvent> = async (event, context) => {
  const DistributionId = process.env.CLOUDFRONT;
  if (!DistributionId) {
    throw new Error('Environment variable CLOUDFRONT is not set');
  }

  const cf = new CF();
  const resp = await cf
    .createInvalidation({
      DistributionId,
      InvalidationBatch: {
        CallerReference: Date.now().toString(),
        Paths: {
          Items: ['/*'],
          Quantity: 1,
        },
      },
    })
    .promise();

  console.log(resp);
};
