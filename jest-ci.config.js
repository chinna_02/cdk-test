module.exports = {
  "roots": [
    "<rootDir>/ci"
  ],
  testMatch: ['**/*.test.ts'],
  "transform": {
    "^.+\\.tsx?$": "ts-jest"
  },
  modulePathIgnorePatterns: ['<rootDir>/dist'],
}
