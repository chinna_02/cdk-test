"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const cloudfront_1 = __importDefault(require("aws-sdk/clients/cloudfront"));
const handler = async (event, context) => {
    const DistributionId = process.env.CLOUDFRONT;
    if (!DistributionId) {
        throw new Error('Environment variable CLOUDFRONT is not set');
    }
    const cf = new cloudfront_1.default();
    const resp = await cf
        .createInvalidation({
        DistributionId,
        InvalidationBatch: {
            CallerReference: Date.now().toString(),
            Paths: {
                Items: ['/*'],
                Quantity: 1,
            },
        },
    })
        .promise();
    console.log(resp);
};
exports.handler = handler;
