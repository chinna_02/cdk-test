"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
//function to strip /en-us or /en-ca from the URI
function checkURI(uri) {
    const regex = /(\/tpci-ecommweb-api\/(profile|orders?))|(\/)(returns|forgot-password|account|checkout|cart|orders?)(?=\/|$)/i;
    return regex.test(uri);
}
exports.checkURI = checkURI;
exports.handler = (event, context, callback) => {
    const requestUri = event.Records[0].cf.request.uri;
    const response = event.Records[0].cf.response;
    const headers = response.headers;
    if (checkURI(requestUri)) {
        headers['cache-control'] = [{ key: 'Cache-Control', value: 'no-store' }];
    }
    else if (requestUri.startsWith('/tpci-ecommweb-api/search') ||
        requestUri.startsWith('/tpci-ecommweb-api/suggest')) {
        delete headers['cache-control'];
    }
    headers['strict-transport-security'] = [
        { key: 'Strict-Transport-Security', value: 'max-age=63072000; includeSubdomains; preload' },
    ];
    headers['content-security-policy-report-only'] = [
        {
            key: 'Content-Security-Policy-Report-Only',
            value: "default-src 'self';" +
                "img-src 'self' data: https://p.yotpo.com https://www.google-analytics.com www.google-analytics.com https://stats.g.doubleclick.net https://yotpo-editor-production.s3.amazonaws.com https://p.brsrvr.com https://bat.bing.com https://www.google.com https://www.google.fi https://f.monetate.net https://www.googletagmanager.com https://cdn-yotpo-images-production.yotpo.com https://t.paypal.com https://fonts.gstatic.com https://bam.nr-data.net https://*.facebook.com https://cdn.honey.io https://scontent.flhr3-2.fna.fbcdn.net https://www.google.co.uk https://www.google.cl https://www.google.com.mx https://www.google.co.in https://www.google.com.au https://www.google.ca https://www.google.es https://www.google.co.jp https://www.google.de https://www.google.com.ph https://www.gstatic.com https://lh3.googleusercontent.com https://ssl.gstatic.com/analytics-suite https://ssl.gstatic.com https://marketer.monetate.net https://graph.facebook.com https://6022696.global.siteimproveanalytics.io https://scontent-lax3-2.xx.fbcdn.net https://gateway.zscalerone.net https://*.google.com https://platform-lookaside.fbsbx.com https://dozi-staging.zoominfo.com;" +
                "script-src 'self' data: https://siteimproveanalytics.com https://www.google-analytics.com https://ssl.google-analytics.com https://stats.g.doubleclick.net https://www.googletagmanager.com https://js-agent.newrelic.com https://bam.nr-data.net bat.bing.com cdn.amplitude.com https://dd.pokemoncenter.com https://staticw2.yotpo.com *.paypal.com *.paypalobjects.com https://se.monetate.net https://f.monetate.net https://cdns.brsrvr.com https://connect.facebook.net https://flex.cybersource.com https://testflex.cybersource.com https://s3.eu-central-1.amazonaws.com https://m71.prod2016.com 'unsafe-eval' 'unsafe-inline';" +
                "script-src-elem 'self' https://siteimproveanalytics.com https://cdns.brsrvr.com https://js-agent.newrelic.com https://testflex.cybersource.com https://www.googletagmanager.com https://se.monetate.net https://f.monetate.net https://bam.nr-data.net https://gc.kis.v2.scr.kaspersky-labs.com https://www.google-analytics.com  https://staticw2.yotpo.com https://bat.bing.com https://cdn.amplitude.com https://www.paypal.com https://fonts.googleapis.com https://dd.pokemoncenter.com https://fonts.googleapis.com https://optimize.google.com https://www.paypalobjects.com https://tagmanager.google.com https://marketer.monetate.net https://me.kis.v2.scr.kaspersky-labs.com https://flex.cybersource.com  https://www.googletagmanager.com https://cdn.ghostaio.com https://www.webrtc-experiment.com https://s3.eu-central-1.amazonaws.com https://m71.prod2016.com  https://connect.facebook.net https://dd.pokemoncenter.com https://www.webrtc-experiment.com https://widget.bugreporting.co https://rialto-gms.s3.amazonaws.com 'unsafe-inline';" +
                "connect-src 'self' https://stats.g.doubleclick.net https://dd.pokemoncenter.com https://www.google-analytics.com https://bam.nr-data.net api.amplitude.com https://bat.bing.com https://staticw2.yotpo.com https://api.yotpo.com  https://www.sandbox.paypal.com https://www.paypal.com https://staticw2.yotpo.com https://www.paypalobjects.com https://engine.monetate.net https://subwayblaze.com https://h7s9xishng.execute-api.us-east-1.amazonaws.com https://api.trongrid.io https://server.bugreporting.co https://www.googletagmanager.com https://dasfelynsaterr.webcam https://mc.yandex.ru https://luxins.net https://sun.tronex.io https://1637314617.rsc.cdn77.org https://zamant.ru https://gjtrack.ucweb.com https://track.uc.cn https://freakarcade.com https://steganos-api.ciuvo.com;" +
                "style-src-elem 'self' https://fonts.googleapis.com https://staticw2.yotpo.com https://optimize.google.com https://marketer.monetate.net https://tagmanager.google.com  https://www.googletagmanager.com https://cdn.ghostaio.com https://www.webrtc-experiment.com https://gc.kis.v2.scr.kaspersky-labs.com https://me.kis.v2.scr.kaspersky-labs.com https://adblockers.opera-mini.net https://pwm-image.trendmicro.com https://widget.bugreporting.co/web/app.css https://cdnjs.cloudflare.com https://romeo.liveclicker.com https://fast.fonts.net https://maxcdn.bootstrapcdn.com https://kickassapp.com 'unsafe-inline';" +
                "style-src 'self' https://staticw2.yotpo.com/ https://fonts.googleapis.com https://translate.googleapis.com https://use.fontawesome.com https://gateway.zscalerone.net https://staticw2.yotpo.com https://app.distill.io 'unsafe-inline';" +
                "font-src 'self' data: https://staticw2.yotpo.com http://fonts.gstatic.com https://marketer.monetate.net https://*.fontawesome.com https://cdn.getspeechify.com https://static3.avast.com https://www.slant.co https://appdown.pstatic.net https://maxcdn.bootstrapcdn.com https://fonts.googleapis.com https://www.clearplay.com https://use.typekit.net https://assets.quadpay.com http://themes.googleusercontent.com https://svcs.tql.com;" +
                "frame-src 'self' https://testflex.cybersource.com https://www.sandbox.paypal.com https://www.paypalobjects.com https://flex.cybersource.com https://www.paypal.com https://www.youtube.com https://optimize.google.com geo.captha-delivery.com  https://h7s9xishng.execute-api.us-east-1.amazonaws.com https://stats.g.doubleclick.net https://pwm-image.trendmicro.com https://s.cmptch.com https://service.securesrv12.com https://secure.mycouponsmartmac.com https://geo.captcha-delivery.com https://youtube.com https://mozbar.moz.com  https://acestream.me https://noop.style https://div.show https://gateway.zscalertwo.net https://www.googletagmanager.com https://utp.ucweb.com https://xen-media.com https://freakarcade.com https://captcha.gecirtnotification.com https://logopedtoys.ru https://russianteleweek.ru https://useast2-www.securly.com https://utp.ucweb.com https://xen-media.com https://blipznchitzcom-a.akamaihd.net https://s.cmptch.com https://bpb.opendns.com https://www.getclyp.com https://www.googletagmanager.com 	https://www.securly.com https://app.distill.io https://d31b6i309j1ui2.cloudfront.net https://partners.cmptch.com ttps://26431-occ0-flex.cybersource.com;" +
                "frame-ancestors 'self' https://dev-tpci1.onehippo.io https://stage-tpci1.onehippo.io https://ppd-tpci1.onehippo.io https://prod-tpci1.onehippo.io;" +
                "object-src 'self';" +
                'report-uri https://cspreport.pokemoncenter.com',
        },
    ];
    headers['x-content-type-options'] = [{ key: 'X-Content-Type-Options', value: 'nosniff' }];
    headers['x-xss-protection'] = [{ key: 'X-XSS-Protection', value: '1; mode=block' }];
    headers['referrer-policy'] = [{ key: 'Referrer-Policy', value: 'same-origin' }];
    callback(null, response);
};
