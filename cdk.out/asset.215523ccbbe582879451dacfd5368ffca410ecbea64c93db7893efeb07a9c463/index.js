"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const handler = (event, _context, callback) => {
    const { cf } = event.Records[0];
    // Only en-GB S3 buckets are "redirected" to currently.
    // en-US buckets should be the default S3 origin that is received from Cloudfront
    const localeS3 = {
        dev: {
            'en-US': 'np-tpci-ecommweb-lambda-dev.s3.us-west-2.amazonaws.com',
            'en-CA': 'np-tpci-ecommweb-lambda-dev.s3.us-west-2.amazonaws.com',
        },
        stage: {
            'en-US': 'np-tpci-ecommweb-lambda-dev.s3.us-west-2.amazonaws.com',
            'en-CA': 'np-tpci-ecommweb-lambda-dev.s3.us-west-2.amazonaws.com',
            'en-GB': 'np-tpci-ecommweb-lambda-dev-eu-west-1.s3.eu-west-1.amazonaws.com', //stage-eu
        },
        ppd: {
            'en-US': 'prod-tpci-ecommweb-lambda-ppd.s3.us-west-2.amazonaws.com',
            'en-CA': 'prod-tpci-ecommweb-lambda-ppd.s3.us-west-2.amazonaws.com',
            'en-GB': 'prod-tpci-ecommweb-lambda-ppd-eu-west-1.s3.eu-west-1.amazonaws.com', //ppd-eu
        },
        prod: {
            'en-US': 'prod-tpci-ecommweb-lambda-ppd.s3.us-west-2.amazonaws.com',
            'en-CA': 'prod-tpci-ecommweb-lambda-ppd.s3.us-west-2.amazonaws.com',
            'en-GB': 'prod-tpci-ecommweb-lambda-ppd-eu-west-1.s3.eu-west-1.amazonaws.com', //prod-eu
        },
        hfe: {
            'en-US': 'np-tpci-ecommweb-lambda-hfe.s3.us-west-2.amazonaws.com',
            'en-CA': 'np-tpci-ecommweb-lambda-hfe.s3.us-west-2.amazonaws.com',
        },
    };
    const stage = {
        E1PIODSAU9WHCA: 'dev',
        EA60MAV3RRPA3: 'stage',
        E30FS25EQONK9G: 'ppd',
        E3L61D6I4WJNSK: 'prod',
        E23T1VLARLNKAD: 'hfe',
    }[cf.config.distributionId];
    const request = cf.request;
    const headers = request.headers;
    if (headers['referer'] &&
        request.origin &&
        request.origin.s3 &&
        JSON.stringify(headers['referer']).includes('/en-gb')) {
        request.headers['host'] = [{ key: 'host', value: localeS3[String(stage)]['en-GB'] }];
        request.origin.s3.domainName = localeS3[String(stage)]['en-GB'];
        request.origin.s3.region = 'eu-west-1';
    }
    else if (headers.cookie && request.origin && request.origin.s3) {
        for (let i = 0; i < headers.cookie.length; i++) {
            if (headers.cookie[i].value.indexOf('NEXT_LOCALE=en-GB') >= 0) {
                request.headers['host'] = [{ key: 'host', value: localeS3[String(stage)]['en-GB'] }];
                request.origin.s3.domainName = localeS3[String(stage)]['en-GB'];
                request.origin.s3.region = 'eu-west-1';
                break;
            }
        }
    }
    callback(null, request);
};
exports.handler = handler;
