"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const datadome_1 = __importDefault(require("./datadome"));
const domainRedirect = __importStar(require("./domainRedirect"));
exports.handler = (event, context, callback) => {
    try {
        domainRedirect.handler(event, context, callback);
    }
    catch (err) {
        // Don't let a bug in domainRedirect prevent datadome from running.
        console.error(err);
    }
    datadome_1.default.handler(event, context, callback);
};
