"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const datadome_1 = __importDefault(require("./datadome"));
const domainRedirect = __importStar(require("./domainRedirect"));
const timestamp = __importStar(require("./timestampHandler"));
const removeAuthCookie = __importStar(require("./removeAuthCookie"));
const handler = (event, context, callback) => {
    try {
        event = timestamp.handler(event);
        event = removeAuthCookie.handler(event);
        domainRedirect.handler(event, context, callback);
    }
    catch (err) {
        // Don't let a bug in domainRedirect prevent datadome from running.
        console.error(err);
    }
    datadome_1.default.handler(event, context, callback);
};
exports.handler = handler;
