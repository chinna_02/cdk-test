"use strict";
/**
 * This is for handling redirects based on domain names.
 *
 * This functionality must be in a "Viewer Request" lambda
 * because the "Origin Request" lambda cannot see the domain name.
 */
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = exports.removeEnUsFromPath = void 0;
const redirectUrls_json_1 = __importDefault(require("./redirectUrls.json"));
const removeEnUsFromPath = (uri) => {
    return (uri.startsWith('/en-us') ? uri.replace('/en-us', '') : uri) || '/';
};
exports.removeEnUsFromPath = removeEnUsFromPath;
const handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    const domain = request.headers.host[0].value;
    const pathStartsWithEnUs = request.uri.startsWith('/en-us');
    const redirectConfig = redirectUrls_json_1.default[request.uri];
    if (redirectConfig) {
        const uri = redirectConfig.path;
        return callback(null, {
            status: '301',
            statusDescription: 'Moved Permanently',
            headers: {
                location: [
                    {
                        key: 'Location',
                        value: redirectConfig.type === 'relative' ? `https://${domain}${uri}` : uri,
                    },
                ],
                'access-control-allow-credentials': [
                    {
                        key: 'Access-Control-Allow-Credentials',
                        value: 'true',
                    },
                ],
                'access-control-allow-methods': [
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET, OPTIONS, HEAD, PUT, POST',
                    },
                ],
            },
        });
    }
    if (['pokemoncenter.com', 'beta.pokemoncenter.com', 'prod.ui.pokemoncenter.com'].includes(domain)) {
        let uri = exports.removeEnUsFromPath(request.uri);
        if (request.querystring) {
            uri += '?' + request.querystring;
        }
        // Redirect to canonical domain
        return callback(null, {
            status: '301',
            statusDescription: 'Moved Permanently',
            headers: {
                location: [
                    {
                        key: 'Location',
                        value: `https://www.pokemoncenter.com${uri}`,
                    },
                ],
                'access-control-allow-credentials': [
                    {
                        key: 'Access-Control-Allow-Credentials',
                        value: 'true',
                    },
                ],
                'access-control-allow-methods': [
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET, OPTIONS, HEAD, PUT, POST',
                    },
                ],
            },
        });
    }
    if ([
        'stage.ui.pokemoncenter.com',
        'dev.ui.pokemoncenter.com',
        'ppd.ui.pokemoncenter.com',
        'www.pokemoncenter.com',
    ].includes(domain) &&
        pathStartsWithEnUs) {
        let uri = exports.removeEnUsFromPath(request.uri);
        if (request.querystring) {
            uri += '?' + request.querystring;
        }
        // Redirect to canonical domain
        return callback(null, {
            status: '301',
            statusDescription: 'Moved Permanently',
            headers: {
                location: [
                    {
                        key: 'Location',
                        value: `https://${domain}${uri}`,
                    },
                ],
                'access-control-allow-credentials': [
                    {
                        key: 'Access-Control-Allow-Credentials',
                        value: 'true',
                    },
                ],
                'access-control-allow-methods': [
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET, OPTIONS, HEAD, PUT, POST',
                    },
                ],
            },
        });
    }
    if ([
        'pokemoncenter.ca',
        'www.pokemoncenter.ca',
        'ppd.ui.pokemoncenter.ca',
        'stage.ui.pokemoncenter.ca',
        'dev.ui.pokemoncenter.ca',
    ].includes(domain)) {
        let redirectDomain = domain.replace('.ca', '.com');
        redirectDomain =
            redirectDomain === 'pokemoncenter.com' ? 'www.pokemoncenter.com' : redirectDomain;
        let uri = request.uri ? request.uri : '/';
        uri = '/en-ca' + uri;
        if (request.querystring) {
            uri += '?' + request.querystring;
        }
        // Redirect to canonical domain
        return callback(null, {
            status: '301',
            statusDescription: 'Moved Permanently',
            headers: {
                location: [
                    {
                        key: 'Location',
                        value: `https://${redirectDomain}${uri}`,
                    },
                ],
                'access-control-allow-origin': [
                    {
                        key: 'Access-Control-Allow-Origin',
                        value: `https://${redirectDomain}${uri}`,
                    },
                ],
                'access-control-allow-credentials': [
                    {
                        key: 'Access-Control-Allow-Credentials',
                        value: 'true',
                    },
                ],
                'access-control-allow-methods': [
                    {
                        key: 'Access-Control-Allow-Methods',
                        value: 'GET, OPTIONS, HEAD, PUT, POST',
                    },
                ],
            },
        });
    }
};
exports.handler = handler;
