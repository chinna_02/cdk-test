"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = exports.removeLocaleInURI = void 0;
//function to strip /en-us or /en-ca from the URI if uri starts with /prod/en-us(ca)
function removeLocaleInURI(uri) {
    const regex = /(^\/en-(us|ca))(?=\/|$)/i;
    uri = uri.replace(regex, '');
    return uri || '/';
}
exports.removeLocaleInURI = removeLocaleInURI;
//function to add locale headers based on the URI
function handler(request) {
    let shopRegion = 'USD';
    let language = 'en-US';
    // for Canada URI set appropriate header values
    if (request.uri.toLowerCase().includes('/en-ca')) {
        shopRegion = 'CAD';
        language = 'en-CA';
    }
    // add locale headers to request
    request.headers['Shopping-Region'] = [{ key: 'Shopping-Region', value: shopRegion }];
    request.headers['Accept-Language'] = [{ key: 'Accept-Language', value: language }];
    request.uri = removeLocaleInURI(request.uri);
    return request;
}
exports.handler = handler;
