"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const sqs_1 = __importDefault(require("aws-sdk/clients/sqs"));
const sns_1 = __importDefault(require("aws-sdk/clients/sns"));
const Charset = 'utf-8';
const handler = async (event, context) => {
    var _a, _b;
    const queueName = process.env.QUEUE;
    if (!queueName) {
        throw new Error('Environment variable QUEUE is not set');
    }
    const topicArn = process.env.TOPIC;
    if (!topicArn) {
        throw new Error('Environment variable TOPIC is not set');
    }
    const sqs = new sqs_1.default();
    const sns = new sns_1.default();
    const QueueUrl = sqs.endpoint.href + event.account + '/' + queueName;
    const receipts = [];
    const alerts = [];
    let res;
    do {
        res = await sqs.receiveMessage({ QueueUrl }).promise();
        for (const msg of res.Messages || []) {
            const { Body } = msg;
            if (Body) {
                try {
                    const notification = JSON.parse(Body);
                    console.log('Notification:', notification);
                    if (notification && notification.Message) {
                        try {
                            alerts.push(JSON.parse(notification.Message));
                        }
                        catch {
                            alerts.push(notification.Message);
                        }
                    }
                }
                catch (error) {
                    console.error('Failed to parse Body as JSON:', error, msg);
                }
            }
            const { ReceiptHandle } = msg;
            if (ReceiptHandle) {
                receipts.push({ ReceiptHandle });
            }
        }
    } while (((_b = (_a = res === null || res === void 0 ? void 0 : res.Messages) === null || _a === void 0 ? void 0 : _a.length) !== null && _b !== void 0 ? _b : 0) > 0);
    if (alerts.length < 1) {
        return;
    }
    const accountName = (() => {
        switch (event.account) {
            case '053655145506':
                return `pokemoncenter-dev (${event.account})`;
            case '322789178911':
                return `pokemoncenter-prod (${event.account})`;
            default:
                return event.account;
        }
    })();
    await sns
        .publish({
        TopicArn: topicArn,
        Subject: 'SES Bounce/Complaint Alert',
        Message: `Total SES Bounces/Complaints: ${alerts.length}
Account: ${accountName}
Alert Lambda: ${context.functionName}
SQS Queue: ${queueName}

${'-'.repeat(80)}

${alerts.map(alert => JSON.stringify(alert, null, 2)).join(`\n\n${'='.repeat(80)}\n\n`)}
`,
    })
        .promise();
    const promises = [];
    const size = 10; // deleteMessageBatch can only be done in chunks of 10
    let index = 0;
    while (index < receipts.length) {
        promises.push(sqs
            .deleteMessageBatch({
            Entries: receipts.slice(index, index + size).map(({ ReceiptHandle }, idx) => ({
                Id: `${idx + 1}`,
                ReceiptHandle,
            })),
            QueueUrl,
        })
            .promise());
        index += size;
    }
    await Promise.all(promises);
};
exports.handler = handler;
