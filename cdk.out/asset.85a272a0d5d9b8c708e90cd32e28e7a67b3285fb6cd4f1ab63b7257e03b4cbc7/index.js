"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const redirect = __importStar(require("./redirect"));
const localize = __importStar(require("./localize"));
const handler = (event, context, callback) => {
    var _a;
    const cf = event.Records[0].cf;
    const stage = {
        E1PIODSAU9WHCA: 'dev',
        EA60MAV3RRPA3: 'stage',
        E30FS25EQONK9G: 'ppd',
        E3L61D6I4WJNSK: 'prod',
        E23T1VLARLNKAD: 'hfe',
    }[cf.config.distributionId];
    redirect.handler(event, context, callback);
    const request = localize.handler(cf.request);
    // tpci-ecommweb-lambda
    const euUiGatewayId = {
    //stage: '51s77a6rk3', // stage-eu
    // TODO ppd: '', // ppd-eu
    // TODO prod: '', // prod-eu
    }[stage !== null && stage !== void 0 ? stage : ''];
    const customOrigin = (_a = request.origin) === null || _a === void 0 ? void 0 : _a.custom;
    if (euUiGatewayId &&
        customOrigin &&
        JSON.stringify(request.headers['Shopping-Region']) ===
            JSON.stringify([{ key: 'Shopping-Region', value: 'EUR' }])) {
        request.origin = {
            custom: {
                ...customOrigin,
                domainName: `${euUiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
                path: `/${stage}-eu`,
            },
        };
        request.headers.host = [
            {
                key: 'Host',
                value: `${euUiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
            },
        ];
    }
    callback(null, request);
};
exports.handler = handler;
