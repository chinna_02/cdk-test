"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = exports.euApiGatewayIds = exports.removeUriPrefix = void 0;
const removeUriPrefix = (location) => {
    return location
        .replace(/^\/tpci-fluent-webhook\//, '/')
        .replace(/^\/tpci-transaction-email-api\//, '/')
        .replace(/^\/tpci-order-status\//, '/')
        .replace(/^\/tpci-ecommweb-api\//, '/')
        .replace(/^\/tpci-inventory-sync\//, '/');
};
exports.removeUriPrefix = removeUriPrefix;
exports.euApiGatewayIds = {
//stage: '1qlx4l21a3', // stage-eu
// TODO ppd: '', // ppd-eu
// TODO prod: '', // prod-eu
};
const handler = (event, _context, callback) => {
    var _a;
    const { cf } = event.Records[0];
    const stage = {
        E1PIODSAU9WHCA: 'dev',
        EA60MAV3RRPA3: 'stage',
        E30FS25EQONK9G: 'ppd',
        E3L61D6I4WJNSK: 'prod',
        E23T1VLARLNKAD: 'hfe',
    }[cf.config.distributionId];
    const euApiGatewayId = exports.euApiGatewayIds[stage !== null && stage !== void 0 ? stage : ''];
    const request = cf.request;
    const customOrigin = (_a = request.origin) === null || _a === void 0 ? void 0 : _a.custom;
    if (euApiGatewayId &&
        customOrigin &&
        request.headers['x-store-scope'] &&
        request.headers['x-store-scope'].some(({ value }) => [
            'pokemon-uk',
            // Other EU storefronts can be added below
        ].includes(value))) {
        request.origin = {
            custom: {
                ...customOrigin,
                domainName: `${euApiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
                path: `/${stage}-eu`,
            },
        };
        request.headers.host = [
            {
                key: 'Host',
                value: `${euApiGatewayId}.execute-api.eu-west-1.amazonaws.com`,
            },
        ];
    }
    if (customOrigin && !customOrigin.domainName.startsWith('customdomain')) {
        request.uri = exports.removeUriPrefix(request.uri);
    }
    callback(null, request);
};
exports.handler = handler;
