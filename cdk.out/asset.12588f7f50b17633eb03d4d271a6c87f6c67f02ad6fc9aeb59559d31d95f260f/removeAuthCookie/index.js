"use strict";
/**
 * This is for removing the auth cookie from the request for cached routes.
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
const handler = (event) => {
    const locales = ['en-us', 'en-ca'];
    const cachedRoutes = [
        '',
        'product', // Product pages
    ];
    const request = event.Records[0].cf.request;
    const headers = request.headers;
    const pathSegments = request.uri.split('/');
    let route = pathSegments.length > 1 ? pathSegments[1].toLowerCase() : '';
    if (locales.includes(route)) {
        // Path includes locale, get the next part of URI
        route = pathSegments.length > 2 ? pathSegments[2].toLowerCase() : '';
    }
    if (cachedRoutes.includes(route) && headers.cookie) {
        // Remove auth cookie from header
        const cookieHeader = headers.cookie[0].value;
        const cookieList = cookieHeader.split(/;\ /);
        let wasCookieRemoved = false;
        for (let m = 0; m < cookieList.length; m++) {
            if (cookieList[m].startsWith('auth=') || cookieList[m].startsWith('correlationId=')) {
                cookieList.splice(m, 1);
                wasCookieRemoved = true;
                m--; // Next loop is same m, to counteract array splice above
            }
        }
        if (wasCookieRemoved) {
            request.headers.cookie[0].value = cookieList.join('; ');
        }
    }
    return event;
};
exports.handler = handler;
