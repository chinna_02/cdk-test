"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const redirect = __importStar(require("./redirect"));
const localize = __importStar(require("./localize"));
exports.handler = (event, context, callback) => {
    redirect.handler(event, context, callback);
    callback(null, localize.handler(event.Records[0].cf.request));
};
