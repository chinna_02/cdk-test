"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const urls_json_1 = __importDefault(require("./urls.json"));
const querystring_1 = __importDefault(require("querystring"));
function redirectLocation(location) {
    if (location === '/site') {
        return '/site/';
    }
    let decoded = location;
    try {
        for (let i = 0; i < 3; i++) {
            decoded = decodeURIComponent(decoded);
        }
    }
    catch { }
    const newLocation = urls_json_1.default[decoded];
    if (newLocation) {
        return newLocation;
    }
    if (location.startsWith('/collections/')) {
        return location.replace('/collections/', '/category/');
    }
    // Catch all in case a product image is not listed in the urls.json
    // These tend to be valid product image urls that map directly from the old site to the new site.
    if (location.startsWith('/wcsstore/PokemonCatalogAssetStore/images/catalog/products/')) {
        return location.replace('/wcsstore/PokemonCatalogAssetStore/images/catalog/products/', '/products/images/');
    }
    return undefined;
}
exports.redirectLocation = redirectLocation;
exports.handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    const location = redirectLocation(request.uri);
    if (location) {
        const [redirectLocation, redirectQuerystring] = location.split('?');
        const redirectParams = querystring_1.default.parse(redirectQuerystring || '');
        // Pass-through 'utm_*' parameters
        const requestParams = querystring_1.default.parse(request.querystring);
        const utmParams = Object.keys(requestParams)
            .filter(key => key.startsWith('utm_'))
            .reduce((obj, key) => {
            return {
                ...obj,
                [key]: requestParams[key],
            };
        }, {});
        const params = {
            ...redirectParams,
            ...utmParams,
        };
        const qs = Object.keys(params).length > 0 ? `?${querystring_1.default.stringify(params)}` : '';
        callback(null, {
            status: '302',
            statusDescription: 'Found',
            headers: {
                location: [
                    {
                        key: 'Location',
                        value: `${redirectLocation}${qs}`,
                    },
                ],
            },
        });
    }
    else if (request.uri.startsWith('/wcsstore')) {
        // Anything from /wcsstore is from the old site.
        // Everything besides product images is no longer valid.
        callback(null, {
            status: '404',
            statusDescription: 'Not Found',
            headers: {},
        });
    }
};
