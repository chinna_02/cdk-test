"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = exports.removeLambdaPrefix = void 0;
const removeLambdaPrefix = (location) => {
    return location
        .replace(/^\/tpci-fluent-webhook\//, '/')
        .replace(/^\/tpci-transaction-email-api\//, '/')
        .replace(/^\/tpci-order-status\//, '/')
        .replace(/^\/tpci-inventory-sync\//, '/');
};
exports.removeLambdaPrefix = removeLambdaPrefix;
const handler = (event, context, callback) => {
    const request = event.Records[0].cf.request;
    request.uri = exports.removeLambdaPrefix(request.uri);
    callback(null, request);
};
exports.handler = handler;
