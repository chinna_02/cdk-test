"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.handler = void 0;
/**
 * Adds a timestamp "ts" querystring parameter to break caching when front-end is running within the Bloomreach CMS
 */
const url = require("url");
const cmsDomainList = ['bloomreach.cloud', 'cms.pokemoncenter.com'];
const handler = (event) => {
    const request = event.Records[0].cf.request;
    if (request.headers['referer']) {
        const refererUrl = request.headers['referer'][0]['value'];
        const { hostname } = new url.URL(refererUrl);
        const domainMatches = cmsDomainList.filter((element) => hostname.includes(element));
        if (domainMatches.length > 0) {
            const now = Date.now();
            if (request.querystring) {
                request.querystring += `&ts=${now}`;
            }
            else {
                request.querystring = `ts=${now}`;
            }
        }
    }
    return event;
};
exports.handler = handler;
