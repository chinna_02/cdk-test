# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 2.3.13

- patch: re-enable cache.test

## 2.3.12

- patch: Disable stage com from datadome

## 2.3.11

- patch: update DEV distribution ID

## 2.3.10

- patch: whitelist wss://*.zineone.com

## 2.3.9

- patch: Remove /en-gb block WAF rule

## 2.3.8

- patch: add additional csp header url

## 2.3.7

- patch: sitespect and datadome changes

## 2.3.6

- patch: Remove unneeded pokemoncenter.co.uk redirect

## 2.3.5

- patch: Update EU bucket policies

## 2.3.4

- patch: Update IP Whitelists. Add new PenTest IPs. Add new IngramMicro Prod IP.

## 2.3.3

- patch: update policy statement

## 2.3.2

- patch: Move Imperva waf rule to before rate limit. All traffic will come through limited number of Imperva IPs and throttle quickly

## 2.3.1

- patch: Add EU Prod VPC IPs
- patch: Send Host header to S3 for maintenance mode to prevent seeing S3 url

## 2.3.0

- minor: Reconfigure waf for Imperva compatibility.

## 2.2.2

- patch: fix whitelist url
- patch: whitelist csp urls

## 2.2.1

- patch: Add Imperva IPs to the WAF rules

## 2.2.0

- minor: Add S3 Origin Request Lambda@Edge to redirect ecommweb-lambda S3 orign to EU S3 Origin
- patch: Change prod SSL cert to be new Digicert
- patch: Skip cache tests for now with the addition of Imperva

## 2.1.2

- patch: add new acm and update CF aliases
- patch: route .com/en-gb stage traffic to eu-west-1

## 2.1.1

- patch: localize .co.uk traffic to .com/en-gb

## 2.1.0

- minor: Add /en-ca maintenace page capability.

## 2.0.9

- patch: Add additional CSP urls

## 2.0.8

- patch: Remove Ingram Endpoint allow. Only allow Ingram IPs.

## 2.0.7

- patch: pokemon-center.co.uk pre-launch redirect

## 2.0.6

- patch: update pim image source path

## 2.0.5

- patch: Add IngramMicro new Static IPs for dev account access

## 2.0.4

- patch: Deploy PIM EU S3 bucket to Prod

## 2.0.3

- patch: s3 bucket for AX PIM integration

## 2.0.2

- patch: sitespect viewer request configuration

## 2.0.1

- patch: Make s3SFTP stack only deploy in dev us-west-2

## 2.0.0

- major: Changes for EU compatibility
- minor: Add EU support
- minor: Change deployment script to now allow a single cdk command to do all deployments
- minor: Rewrite of Lambdas@Edge
- patch: whitelist sitespect cidrs

## 1.21.3

- patch: fix api gateway lambda integration errors
- patch: fix elk stack failures
- patch: move eks stack to aws account level
- patch: move the account level deployment logic to class itself
- patch: remove stage name from eks stack
- patch: rename eks stack
- patch: semver for removing stage prefix from eks stack

## 1.21.2

- patch: stack for s3sftp

## 1.21.1

- patch: whitelist MABL Ips 

## 1.21.0

- minor: Open Ingram Micro endpoints in Order-Status and Inventory-Sync to external traffic in Dev and Stage with no IP allowlist.
- patch: Remove Ingram Micro IP Whitelist. IM IPs are far to dynamic to Whitelist.

## 1.20.7

- patch: Eks cluster should be created only in non-prod

## 1.20.6

- patch: whitelist ingram micro ips

## 1.20.5

- patch: Add XPO VPN IP to order-status whitelist for Stage env

## 1.20.4

- patch: whitelist new Flexe CIDRs

## 1.20.3

- patch: Add Inventory-Sync APIGW IDs for ppd and prod

## 1.20.2

- patch: whitelist new Flexe IP

## 1.20.1

- patch: disable redirect tests temporarily
- patch: enable back redirect test cases
- patch: fix connectivitty test failure
- patch: fix connectivitty test failure in dev
- patch: fix connectivitty test failure in dev
- patch: fix feature toggles
- patch: remove tpci-inventory-sync behavior in ppd and prod as the origin api doesn't exists

## 1.20.0

- minor: pim s3 bucket and topic for EU

## 1.19.0

- minor: Add ACL creation for inventory-sync
- minor: Update the waf rules
- minor: test cases for WAF rules
- minor: update waf rules to check IP in the header
- patch: Bump bitbucket pipeline image version. Shellcheck fixes.
- patch: Temporarily skip 3 CI tests that are misconfigured for a while. Preventing deployments.
- patch: Update Lambda Runtime version to 14
- patch: add block-x-http-method-override rule in cloudfront WAF

## 1.18.4

- patch: Enforce formatting

## 1.18.3

- patch: disable ci:redirect-destinations
- patch: remove http report-url header
- patch: remove tag step from BitBucket Pipeline

## 1.18.2

- patch: Added condition on SG

## 1.18.1

- patch: services: update order-status api gateway

## 1.18.0

- minor: Lambda function for pim image sync batch operation

## 1.17.1

- patch: Apply ELB security-groups rules

## 1.17.0

- minor: cloudfront: add maintenance page [CELA-1378]

## 1.16.3

- patch: services: update cdk.context.json
- patch: vpc: add k8s tags to subnets

## 1.16.2

- patch: update services acm certificate

## 1.16.1

- patch: cloudfront: move flexe ips to oms-plugins WAF rule

## 1.16.0

- minor: cloudfront: use EdgeLamda
- patch: cloudfront: allow internal users to access /en-gb [COMUK-870]

## 1.15.2

- patch: whitelist flexe ips

## 1.15.1

- patch: cloudfront: block /en/gb in PROD

## 1.15.0

- minor: Update to cdk 1.101
- minor: services: create an originConfig abstraction
- patch: Add EdgeLambda and use it in tpci-aws-cdk-services
- patch: simplify servicesOriginRequest implementation

## 1.14.1

- patch: fix deploy.sh

## 1.14.0

- minor: refactor resource naming
- patch: modify IPSets name 

## 1.13.1

- patch: remove the stage check in pim image lambda

## 1.13.0

- minor: add lambda and sns topic for pim comca-1445

## 1.12.2

- patch: fix IPSet already exists error in cloudfront waf order-status

## 1.12.1

- patch: fix IPSet already exists error in services cloudfront

## 1.12.0

- minor: Separate domain for Services: services.pokemoncenter.com cela-1379

## 1.11.3

- patch: Updated brXM endpoint

## 1.11.2

- patch: cloudfront: cache /site/* based on Origin header

## 1.11.1

- patch: cloudfront: retain edge lambdas instead of deleting them

## 1.11.0

- minor: Add en-GB header

## 1.10.2

- patch: Remove unused cookies from tpci-ecommweb-lambda cookie whitelist

## 1.10.1

- patch: Remove correlationId cookie for cached routes

## 1.10.0

- minor: whitelist Flexe Ips

## 1.9.1

- patch: Enable cache-control response from FE lambdas

## 1.9.0

- minor: whitelist netsparker IPs

## 1.8.0

- minor: Add removeAuthCookie to viewerRequest, which removes the auth cookie from requests to cachable pages for increased cache hits

## 1.7.1

- patch: Added hitcounter for requests from Bloomreach CMS

## 1.7.0

- minor: Creating EKS cluster stack

## 1.6.0

- minor: cloudfront: whitelist narvar IPs [CELA-1818]

## 1.5.0

- minor: cloudfront: change .ca domain redirect to permanent [CELA-1699]

## 1.4.11

- patch: cloudfront: expire waf logs after 7 days

## 1.4.10

- patch: fix s3 bucket action

## 1.4.9

- patch: Iam role to access sumologic

## 1.4.8

- patch: Add https://h.online-metrix.net to response headers for device fingerprint

## 1.4.7

- patch: Do not require approval in buildspec

## 1.4.6

- patch: remove /galar redirect

## 1.4.5

- patch: fix caching

## 1.4.4

- patch: fix cache tests

## 1.4.3

- patch: install npm@7.5.6 and dont cache node_modules
- patch: update packages and rebuild package-lock.json

## 1.4.2

- patch: use npm ci during build and connectivity tests

## 1.4.1

- patch: Fix deprecation warning regarding Tag.add()

## 1.4.0

- minor: Creating KinesisFirehose Delivery Stream for WAF

## 1.3.0

- minor: update packages

## 1.2.3

- patch: cache brXM for one hour, comca-1361

## 1.2.2

- patch: cloudfront: cache tpci-ecommweb-api/product/status/* based on header

## 1.2.1

- patch: cloudfront/waf: whitelist Bitbucket Pipeline IPs in stage
- patch: vpc: remove bitbucket bastion

## 1.2.0

- minor: add an ec2 bastion to allow bitbucket to access stage

## 1.1.15

- patch: fix cdk version

## 1.1.14

- patch: redirect: fix broken redirect

## 1.1.13

- patch: Update CDK Packages

## 1.1.12

- patch: cloudfront: add siteimproveanalytics.com to CSP

## 1.1.11

- patch: cloudfront: replace js.datadome.co with dd.pokemoncenter.com in CSP

## 1.1.10

- patch: whitelist violated domains

## 1.1.9

- patch: ci/cache: Increase jest.setTimeout() to 10 seconds

## 1.1.8

- patch: Rate limit all API requests in the WAF cela-1365

## 1.1.7

- patch: cloudfront: cache /tpci-ecommweb-api/product based on header

## 1.1.6

- patch: npm: add jest-cli

## 1.1.5

- patch: cloudfront: add tests for caching

## 1.1.4

- patch: lambda@edge: add missing space in CSP

## 1.1.3

- patch: Whitelist Violated CSP Domain Urls

## 1.1.2

- patch: cloudfront: support /tpci-ecommweb-api/product/status/*

## 1.1.1

- patch: cloudfront: whitelist x-store-scope header

## 1.1.0

- minor: cloudfront: Enable caching for /tpci-ecommweb-api/product/*

## 1.0.12

- patch: Whitelist NEXT_LOCALE cookie

## 1.0.11

- patch: api-wafs: rm old api key

## 1.0.10

- patch: fix the stage connectivity test failure

## 1.0.9

- patch: Whitelist Violated CSP Domain Urls

## 1.0.8

- patch: disable datadome for non-prod, cela-1098

## 1.0.7

- patch: enable caching for ecommweb-api

## 1.0.6

- patch:  Revert Whitelisted CSP violation domain

## 1.0.5

- patch: BrXM: Making changes on page editor not being displayed immediately

## 1.0.4

- patch: Whitelist Violated Domain Urls

## 1.0.3

- patch: Whitelist pc-locale cookie cela-1081

## 1.0.2

- patch: change cache behaviors for ecommweb-api and  default route- comca-1152

## 1.0.1

- patch: Whitelist Violated Domains Urls

## 1.0.0

- major: point test.pokemoncenter.com to stage CloudFront distribution

## 0.42.15

- patch: fix script-src-elem directive quotes

## 0.42.14

- patch: cela-955 exclue non-prod all NAT IPs

## 0.42.13

- patch: Whitelisted Violated Domain Urls

## 0.42.12

- patch: cela-955 exclue non-prod NAT IPs

## 0.42.11

- patch: Whitelisted Violated Domains Under Report-only Header

## 0.42.10

- patch: cela-955 reduce calls to DataDome

## 0.42.9

- patch: redirect requests cela-918

## 0.42.8

- patch: canada site launch comca-1137

## 0.42.7

- patch: add content-security-policy-report-only header cela-930

## 0.42.6

- patch: API WAF failure AWS support Case ID 7518157421

## 0.42.5

- patch: add all envs in the origin request locale check

## 0.42.4

- patch: en-us paths are not being routed properly ecomca-1040

## 0.42.3

- patch: enable vpn restriction for /en-ca

## 0.42.2

- patch: removed exception for ppd

## 0.42.1

- patch: remove /en-ca vpn restriction for testing in prod

## 0.42.0

- minor: Creating s3 bucket for DAM

## 0.41.4

- patch: vanity redirect for www.pokemoncenter.com/en-us comca-1071

## 0.41.3

- patch: revert cache for /site/* in cela-117

## 0.41.2

- patch: add .ca domains in .com acm cert

## 0.41.1

- patch: allow epcc queue to subscribe the topic comca-899

## 0.41.0

- minor: Adding HTTPS Headers

## 0.40.4

- patch: cloudfront: use api-wafs key

## 0.40.3

- patch: whitelist ip-addr for pen-test

## 0.40.2

- patch: fix for dev connectivity test failure 

## 0.40.1

- patch: US vanity redirect

## 0.40.0

- minor: Added Https headers.

## 0.39.0

- minor: Canada vanity redirects

## 0.38.2

- patch: cela-117 - cache policies for cfn routes

## 0.38.1

- patch: cela-897,cela-887 standardize pim topic name and set proper policy

## 0.38.0

- minor: whitelist SiteImprove Ips

## 0.37.1

- patch: add SiteSpect header

## 0.37.0

- minor: cloudfront: forward SiteSpect header to ecommweb-lambda

## 0.36.2

- patch: add all sitespect Ips

## 0.36.1

- patch: adding waf rule for sitespect

## 0.36.0

- minor: cloudfront: remove cloudfront error page

## 0.35.1

- patch: cloudfront: revert sitespec whitelist from WAF

## 0.35.0

- minor: Only convert 403 -> 404 in prod

## 0.34.0

- minor: whitelist SiteSpect IP address

## 0.33.0

- minor: Update WAF to block external traffic to en-ca

## 0.32.0

- minor: Update WAF to allow internal traffic to en-ca

## 0.31.0

- minor: Release feature xyz to production

## 0.30.0

- minor: Add PIM S3 to app file

## 0.29.0

- minor: COMCA-29-Configure-S3-for-PIM

## 0.28.1

- patch: Update cloudfront stack to use hfe buckets and oai

## 0.28.0

- minor: Move cache-control from CloudFront to Response Lambda and Frontend

## 0.27.1

- patch: Adding HFE checks for cdk execution - API GW

## 0.27.0

- minor: Re-enable en-ca in stage

## 0.26.0

- minor: Route requests for \en-ca* to 404 (Page not found) until Canada is launched (except in dev env)

## 0.25.0

- minor: COMCA-404 Fix for Promotion Build DataDome test error

## 0.24.1

- patch: cloudfront: fix lambda version generation

## 0.24.0

- minor: COMCA-78-Add headers for locale

## 0.23.1

- patch: Rename CloudFront lambdas

## 0.23.0

- minor: CloudFront: cache /tpci-ecommweb-api/(search|suggest)

## 0.22.0

- minor: new redirect for graduation sku
- minor: updated to avoid double redirect
- minor: updated to urls.csv

## 0.21.3

- patch: redirect: handle URIError when decoding

## 0.21.2

- patch: cloudfront: favicon.ico moved to root of uiBucket

## 0.21.1

- patch: cloudfront: fix lambda version

## 0.21.0

- minor: cloudfront: clean up of emergency production changes

## 0.20.0

- minor: add a.giltus ftp user to production

## 0.19.10

- patch: redirect: /collections/* -> /category/*

## 0.19.9

- patch: redirect: triple decode every request

## 0.19.8

- patch: cloudfront: add correlationId cookie

## 0.19.7

- patch: cloudfront: fix sitemap

## 0.19.6

- patch: Fix connectivity buildspec and redirect test

## 0.19.5

- patch: update redirect urls and add tests for urls.json

## 0.19.4

- patch: cloudfront: robots.txt moved to root of uiBucket

## 0.19.3

- patch: Disable caching

## 0.19.2

- patch: remove allow-ecommweb feature toggle

## 0.19.1

- patch: cloudfront: add no-cache to /checkout and /tpci-ecommweb-api/order

## 0.19.0

- minor: add api-wafs stack

## 0.18.0

- minor: cloudfront: add headers lambda

## 0.17.2

- patch: cloudfront: improve caching by whitelisting cookies

## 0.17.1

- patch: cloudfront: redirect to www

## 0.17.0

- minor: cloudfront: enable caching

## 0.16.2

- patch: cloudfront: remove header-size from WAF

## 0.16.1

- patch: Add Liz, Sheela, and Alphonse to prod ses notifications

## 0.16.0

- minor: cloudfront: proxy /site/* to bloomreach

## 0.15.11

- patch: cloudfront: use decodeURIComponent in redirect to handle colon in URLs

## 0.15.10

- patch: cloudfront: serve apple-touch-icon*.png out of s3

## 0.15.9

- patch: cloudfront: reponsed to /wcsstore with 404

## 0.15.8

- patch: cloudfront: add catch all for old product image urls

## 0.15.7

- patch: Adding version for redirect updates

## 0.15.6

- patch: cloudfront: move /health check to beginning of WAF

## 0.15.5

- patch: White list TPCI deviceConnect

## 0.15.4

- patch: Update README to include WAF and pcenter vpc
- patch: Update feature toggle check to use true/false instead of on/off

## 0.15.3

- patch: Use valueFromLookup so that parameter value can be evaulated as plain string

## 0.15.2

- patch: Catch exception when feature toggle not found

## 0.15.1

- patch: Add feature toggle allow-ecommweb
- patch: cloudfront: open up prod

## 0.15.0

- minor: cloudfront: add initial redirect urls and script a to generate them

## 0.14.0

- minor: remove oms-plugins stack

## 0.13.4

- patch: Add rm -rf node_modules command for buildspec.yml necessary for stage build

## 0.13.3

- patch: cloudfront: break up IP whitelists

## 0.13.2

- patch: Files for deployment

## 0.13.1

- patch: cloudfront: move ratelimit below IP whitelist

## 0.13.0

- minor: cloudfront: add rate limit

## 0.12.0

- minor: cloudfront: switch to WAFv2
- patch: update to aws-cdk 1.27

## 0.11.0

- minor: cloudfront: add [www.]pokemoncenter.com

## 0.10.0

- minor: oms-plugins: add prod

## 0.9.7

- patch: cloudfront: add favicon

## 0.9.6

- patch: cloudfront: add DataDome IP to whitelist

## 0.9.5

- patch: fix cdk-version tag

## 0.9.4

- patch: Add version tag

## 0.9.3

- patch: cloudfront: turn on compression

## 0.9.2

- patch: cloudfront: allow /tpci-transaction-email-api and reorganize rules
- patch: cloudfront: fix WAF for tpci-order-status
- patch: cloudfront: increase max cookie size because BR's cookies are long

## 0.9.1

- patch: Whitelist BugCrowd IP

## 0.9.0

- minor: Move API behind CloudFront

## 0.8.4

- patch: cloudfront: allow shared OAI to access sitemaps for both stages.

## 0.8.3

- patch: cloudfront: sitemaps moved to /${stage}/

## 0.8.2

- patch: Add IPs for InsightAppSec DAST Scanner

## 0.8.1

- patch: cloudfront: add sitemap

## 0.8.0

- minor: Add custom domain for APIs
- patch: combine dev and prod apps
- patch: update packages

## 0.7.3

- patch: Whitelist brSM bay area office ips

## 0.7.2

- patch: Add support for ips subnets

## 0.7.1

- patch: wafs: whitelist more IPs for order-status

## 0.7.0

- minor: wafs: add order-status

## 0.6.1

- patch: wafs: remove old ACLs and cloudfront key

## 0.6.0

- minor: cloudfront: add security rules to WAF
- patch: wafs: only check that x-api-key query parameter is not empty

## 0.5.4

- patch: cloudfront: use new api key

## 0.5.3

- patch: Remove Bloomreach IPs for old us-east-1 instances
- patch: Temporarily whitelist Bloomreach Amsterdam Office IP for Testing

## 0.5.2

- patch: cloudfront: restrict static bucket access

## 0.5.1

- patch: base: fix stage+account check
- patch: wafs: allow anyone to call /health on fluent-webhook

## 0.5.0

- minor: wafs: add fluent webhook

## 0.4.1

- patch: more cleanup of stage checks

## 0.4.0

- minor: Add SES Bounce Stack

## 0.3.1

- patch: Add new Bloomreach IPs for us-west-2

## 0.3.0

- minor: Add DataDome to CloudFront

## 0.2.1

- patch: Serve robots.txt from cloudfront

## 0.2.0

- minor: serve static files from CloudFront

## 0.1.0

- minor: Updates for deploying to us-west-2 in prod account
